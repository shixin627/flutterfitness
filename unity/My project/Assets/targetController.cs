using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class TargetController : MonoBehaviour
{
    [SerializeField]
    // 原點Origin(肩膀)
    private Vector3 Origin;
    public GameObject leftShoulder;
    public GameObject leftElbow;
    public GameObject leftWrist;

    // 人形的臂長
    private float model_upperArm_length;
    private float model_lowerArm_length;
    // 左手臂 Sensors 的初始單位向量(基於 Sensor frame)
    private Vector3 V0_ELBOW = new Vector3(1, 0, 0); //(0, 1, 0)  //(1, 0, 0)
    private Vector3 V0_WRIST = new Vector3(0, -1, 0); //(0, -1, 0) //(0, 1, 0)

    // Elbow
    private Vector3 V0_elbow;
    public Vector3 Vt_elbow;
    private Vector3 originalGe = Vector3.down;
    public Vector3 unit_Ve;
    private Vector3 global_vector_elbow;

    // Wrist
    private Vector3 V0_wrist;
    private Vector3 Vt_wrist;
    private Vector3 originalGw = Vector3.down;
    private Vector3 unit_Vw;
    private Vector3 global_vector_wrist;

    // Quaternions from sensor fusion algorithm
    private Quaternion myElbowQuaternion;
    private Quaternion myWristQuaternion;

    void Start()
    {
        leftShoulder = GameObject.Find("mixamorig:LeftArm");
        leftElbow = GameObject.Find("mixamorig:LeftForeArm");
        leftWrist = GameObject.Find("mixamorig:LeftHand");

        Origin = leftShoulder.transform.position;
        
        model_upperArm_length = Vector3.Distance(leftShoulder.transform.position, leftElbow.transform.position);
        model_lowerArm_length = Vector3.Distance(leftElbow.transform.position, leftWrist.transform.position);

        V0_elbow = V0_ELBOW;
        V0_wrist = V0_WRIST;

        myElbowQuaternion = new Quaternion();
        myWristQuaternion = new Quaternion();

        Vt_elbow = myElbowQuaternion * V0_elbow;
        Vt_wrist = myWristQuaternion * V0_wrist;

        global_vector_elbow = new Vector3(0, -model_upperArm_length, 0);
        global_vector_wrist = new Vector3(0, -model_lowerArm_length, 0); // 先前是model_upperArm_length忘了改正
    }

    // Update is called once per frame
    void Update()
    {
        dataProcess();

        gameObject.transform.position = Origin + global_vector_elbow + global_vector_wrist;
    }

    private void dataProcess() // data is a list of two quaternions at left arm
    {
        myElbowQuaternion = GameObject.Find("FreeCube").GetComponent<Rotate>().myElbowQuaternion;
        myWristQuaternion = GameObject.Find("FreeCube").GetComponent<Rotate>().myWristQuaternion;

        // Rotate the orientation of the object
        gameObject.transform.rotation = myWristQuaternion;
        // Convert sensor frame to global frame
        Vt_elbow = myElbowQuaternion * V0_elbow;
        Vt_wrist = myWristQuaternion * V0_wrist;

        // Global frame to Unity frame vector
        originalGe = new Vector3(Vt_elbow.x, Vt_elbow.z, Vt_elbow.y);
        originalGw = new Vector3(Vt_wrist.x, Vt_wrist.z, Vt_wrist.y);
        // Align the real direction of sensor to the original direction in Unity,
        // and we want to send this unit vector back to Flutter.
        float correctedAngle = GameObject.Find("FreeCube").GetComponent<Rotate>().correctedAngle;
        unit_Ve = Quaternion.AngleAxis(correctedAngle, Vector3.up) * originalGe;
        unit_Vw = Quaternion.AngleAxis(correctedAngle, Vector3.up) * originalGw;

        // Scale arms unit vector to size in Unity Frame(could be updated with fixed frame: 60Hz)
        global_vector_elbow = Vector3.Scale(unit_Ve, new Vector3(model_upperArm_length, model_upperArm_length, model_upperArm_length));
        global_vector_wrist = Vector3.Scale(unit_Vw, new Vector3(model_lowerArm_length, model_lowerArm_length, model_lowerArm_length));
    }
}
