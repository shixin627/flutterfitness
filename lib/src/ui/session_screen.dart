import 'package:flutter/cupertino.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter/material.dart';
import '/src/data/constant.dart';
import '../models/ExerciseData_model.dart';
import '../models/TaskChart.dart';
import '../services/navigation_service.dart';
import '../widgets/material.dart';

import '../service_locator.dart';

class SessionScreen extends StatefulWidget {
  final DataSet dataSet;
  const SessionScreen({Key? key, required this.dataSet}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return SessionScreenState(dataSet);
  }
}

class SessionScreenState extends State<SessionScreen> {
  final DataSet dataSet;
  SessionScreenState(this.dataSet);

  final NavigationService _navigationService = locator<NavigationService>();
  TaskData task = TaskData();
  int _actualChart = 0;
  final List<String> _chartDropdownItems = ['總做功量', '平均功率', '總訓練量'];

  @override
  void initState() {
    super.initState();
    task.index = _chartDropdownItems[_actualChart];
    SessionBar.fetchSession(dataSet, task);
  }

  @override
  Widget build(BuildContext context) {
    final category = CATEGORIES[dataSet.itemID][0];
    final item = ITEM_NAME[dataSet.itemID];
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: const TabBar(
            tabs: [
              const Tab(text: '表格'),
              Tab(text: '長條圖'),
            ],
          ),
          title: Text(category + ' x ' + item),
          automaticallyImplyLeading: true,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () => _navigationService.goBack(),
          ),
        ),
        body: ScreenBackground(
          child: TabBarView(
            children: [
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: StaggeredGrid.count(
                  crossAxisCount: 2,
                  crossAxisSpacing: 12.0,
                  mainAxisSpacing: 12.0,
                  children: <Widget>[
                    StaggeredGridTile.count(
                      crossAxisCellCount: 2,
                      mainAxisCellCount: 2,
                      child: DashboardElement(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: DataTable(
                                columns: const [
                                  DataColumn(
                                    label: Text('重量'),
                                  ),
                                  DataColumn(
                                    label: Text('次數'),
                                  ),
                                  DataColumn(
                                    label: Text('訓練量'),
                                  ),
                                  DataColumn(
                                    label: Text('做功量'),
                                  ),
                                  DataColumn(
                                    label: Text('平均功率'),
                                  ),
                                  DataColumn(
                                    label: Text('最大爆發力'),
                                  ),
                                ],
                                rows: dataSet.dataset.map((theSet) {
                                  return DataRow(cells: [
                                    DataCell(
                                      Text('${theSet.weight} KG'),
                                      onTap: () {},
                                    ),
                                    DataCell(
                                      Text('${theSet.repetitions}'),
                                      onTap: () {},
                                    ),
                                    DataCell(
                                      Text('${theSet.trainingVolume} KG'),
                                      onTap: () {},
                                    ),
                                    DataCell(
                                      Text(
                                        '${theSet.joule}焦耳',
                                      ),
                                      onTap: () {},
                                    ),
                                    DataCell(
                                      Text(
                                        '${theSet.powerAve}W',
                                      ),
                                      onTap: () {},
                                    ),
                                    DataCell(
                                      Text(
                                        '${theSet.maxPower}W',
                                      ),
                                      onTap: () {},
                                    ),
                                  ]);
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    StaggeredGridTile.count(
                      crossAxisCellCount: 2,
                      mainAxisCellCount: 2,
                      child: Visibility(
                        visible: (dataSet.interRest != null),
                        child: DashboardElement(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: DataTable(
                                  columns: const [
                                    DataColumn(
                                      label: Text('組間休息時間'),
                                    ),
                                  ],
                                  rows: dataSet.interRest.map((rest) {
                                    return DataRow(cells: [
                                      DataCell(
                                        Text('休息 $rest 秒'),
                                        onTap: () {},
                                      ),
                                    ]);
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                  // staggeredTiles: [
                  //   StaggeredTile.extent(2, 200.0),
                  //   StaggeredTile.extent(2, 200.0),
                  // ],
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: StaggeredGrid.count(
                  crossAxisCount: 2,
                  crossAxisSpacing: 12.0,
                  mainAxisSpacing: 12.0,
                  children: <Widget>[
                    StaggeredGridTile.count(
                      crossAxisCellCount: 2,
                      mainAxisCellCount: 4,
                      child: DashboardElement(
                        child: Container(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child: Column(children: <Widget>[
                              DropdownButton(
                                isDense: true,
                                value: task.index,
                                onChanged: (value) {
                                  var txt = value as String;
                                  setState(() {
                                    task.index = txt;
                                    _actualChart =
                                        _chartDropdownItems.indexOf(txt);
                                  });
                                },
                                items: _chartDropdownItems.map((String title) {
                                  return DropdownMenuItem(
                                    value: title,
                                    child: Text(title,
                                        style: const TextStyle(
                                            color: Colors.blue,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14.0)),
                                  );
                                }).toList(),
                              ),
                              Expanded(
                                child: SessionBar(task, animate: true),
                              ),
                            ]),
                          ),
                        ),
                      ),
                    ),
                  ],
                  // staggeredTiles: [
                  //   StaggeredTile.extent(2, 450.0),
                  // ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
