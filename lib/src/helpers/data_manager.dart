import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class DataManager {
  /// 刪除位於路徑[path]的檔案
  static Future<void> deleteFile(String path) async {
    File(path).delete();
  }

  /// 檢查路徑[path]是否存在資料夾，如果沒有就創建一個
  static Future<void> checkDir(String path) async {
    await Directory(path).exists().then((isExisted) {
      if (!isExisted) {
        Directory(path).create(recursive: true);
      }
    });
  }

  /// 在App的資料夾底下創建一個包含項目名稱[item]與使用者編號[id]的路徑
  static Future<Directory> getTargetDir(String item, String id) async {
    return await getExternalStorageDirectory().then((directory) {
      final path = "${directory!.path}/$item/$id";
      return checkDir(path).then((_) {
        return Directory(path);
      });
    });
  }

  /// 從資料夾中讀取所有檔案
  static Future<List<File>> getFilesFromDir(String item, String id) async {
    return await getTargetDir(item, id).then((dir) {
      return dir.listSync().map((f) {
        return File(f.path);
      }).toList();
    });
  }

  /// 寫入資料至新建立的檔案中
  static Future<File?> writeCSVFile(
      List<String> data, String item, String uid, String fileName,
      {Function? success, Function? fail}) async {
    if (data.isEmpty) {
      if (fail != null) fail();
    }
    String head =
        "systemclock,ax,ay,az,gx,gy,gz,qwu,qxu,qyu,qzu,qwl,qxl,qyl,qzl,xu,yu,zu,xl,yl,zl\n";
    String body = data.join('\n');
    List<int> bytes =
        utf8.encode(head + body); // 將字串編碼為 UTF-8形式的位元組
    return getTargetDir(item, uid).then((dir) {
      return File('${dir.path}/$fileName').writeAsBytes(bytes); // 在路徑底下寫入新檔案
    });
  }
}
