import 'package:flutter/cupertino.dart';
import 'package:flutter_blue/flutter_blue.dart';

class FitnessItemModel extends ChangeNotifier {
  bool _isAvailable = false;
  bool get isAvailable => _isAvailable;
  set isAvailable(bool value) {
    if (value != _isAvailable) {
      _isAvailable = value;
      notifyListeners();
    }
  }

  // 已搜索到的目標設備
  BluetoothDevice? _target;
  BluetoothDevice? get target => _target;
  set target(BluetoothDevice? device) {
    _target = device;
    notifyListeners();
  }

  // 重置連線狀態
  void resetBLE() {
    isAvailable = false;
    target = null;
    notifyListeners();
  }
}
