import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'services/navigation_service.dart';
import 'service_locator.dart';
import 'widgets/theme.dart';
import 'router.dart' as router;
import 'models/route_paths.dart' as routes;

class SmartFitnessApp extends StatefulWidget {
  const SmartFitnessApp({Key? key}) : super(key: key);

  @override
  SmartFitnessAppState createState() => SmartFitnessAppState();
}

class SmartFitnessAppState extends State<SmartFitnessApp> {
  Future<void> _setupLocator() async {
    setupNavLocator();
    await setupLocator();
  }

  @override
  void initState() {
    super.initState();
    _setupLocator();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Smart Fitness App',
      theme: defaultTargetPlatform == TargetPlatform.iOS ? null : androidTheme,
      home: Column(
        children: <Widget>[
          Expanded(
            child: Navigator(
              key: locator<NavigationService>().navigatorKey,
              onGenerateRoute: router.generateRoute, // 產生路由清單
            ),
          ),

          // PageView(
          //   children: [
          //     FitnessMenuScreen(),
          //     DatabaseManagement(),
          //     HistoryStatistic(),
          //     ProfilePage(),
          //     JudgeAuth(),
          //   ],
          // ),

          BottomNav(navCallback: (String namedRoute) {
            debugPrint("Navigating to $namedRoute");
            locator<NavigationService>()
                .navigatorKey
                .currentState!
                .pushReplacementNamed(namedRoute);
          }),
        ],
      ),
    );
  }
}

class BottomNav extends StatefulWidget {
  final String initialRoute;
  final ValueChanged<String> navCallback;

  const BottomNav({
    Key? key,
    this.initialRoute: routes.HomeRoute,
    required this.navCallback,
  }) : super(key: key);

  @override
  _BottomNavState createState() => _BottomNavState();
}

// 底部指標欄
class _BottomNavState extends State<BottomNav> {
  String? _currentRoute;

  @override
  void initState() {
    super.initState();
    _currentRoute = widget.initialRoute;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      elevation: 12.0,
      child: Container(
        height: 56.0,
        width: MediaQuery.of(context).size.width,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildButton(routes.HomeRoute, Icons.home, "主選單"),
            _buildButton(routes.DBManagement, Icons.cloud, "雲端"),
            _buildButton(routes.StatisticRoute, Icons.show_chart, "資料統計"),
            _buildButton(routes.ProfileRoute, Icons.person, "檔案"),
            _buildButton(routes.FreeWeightRoute, Icons.developer_board, "自由重量"),
          ],
        ),
      ),
    );
  }

  Widget _buildButton(String namedRoute, IconData data, String tooltip) {
    final theme = Theme.of(context);
    return Flexible(
      flex: 1,
      child: Tooltip(
        message: tooltip,
        child: InkWell(
          onTap: () => onButtonTap(namedRoute),
          child: Center(
            child: Icon(
              data,
              size: 32.0,
              color: _currentRoute == namedRoute
                  ? theme.colorScheme.secondary
                  : theme.disabledColor,
            ),
          ),
        ),
      ),
    );
  }

  void onButtonTap(String namedRoute) {
    setState(() {
      _currentRoute = namedRoute;
    });
    widget.navCallback(_currentRoute!);
  }
}
