import 'package:flutter/material.dart';
import '/src/data/constant.dart';
import '/src/models/freeweight_item.dart';
import '/src/models/internel_state.dart';
import '/src/providers/data_monitor.dart';
import '/src/service_locator.dart';
import 'package:provider/provider.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';
import '../providers/bluetooth_notifier.dart';
import '../providers/artificial_intelligent.dart';
import '../providers/unity_provider.dart';

class MyUnityView extends StatefulWidget {
  const MyUnityView({Key? key}) : super(key: key);

  @override
  _MyUnityViewState createState() => _MyUnityViewState();
}

class _MyUnityViewState extends State<MyUnityView>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  // UnityWidget? unity;

  // @override
  // void initState() {
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final _width = MediaQuery.of(context).size.width;

    return Card(
      margin: const EdgeInsets.all(5),
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: SizedBox(
        width: double.infinity,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
                if (states.contains(MaterialState.pressed)) {
                  return Theme.of(context).colorScheme.primary.withOpacity(0.5);
                }
                return const Color(0x30777777);
              },
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              Text(
                'Motion Tracking',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color(0xFF00C8F4),
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
            ],
          ),
          onPressed: () {
            debugPrint("opening unity view...");
            Navigator.of(context).pushNamed(
              '/simple',
            );
          },
        ),
      ),
    );

    return Container(
      color: const Color(0x99FFFFFF),
      child: Selector<UnityNotifier, bool>(
        selector: (_, prov) => prov.isTriggered,
        builder: (context, isTriggered, child) => isTriggered
            ? Card(
                margin: const EdgeInsets.all(5),
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Stack(
                  children: <Widget>[
                    Visibility(
                      visible: isTriggered,
                      child: UnityWidget(
                        onUnityCreated:
                            Provider.of<UnityNotifier>(context, listen: false)
                                .onUnityCreated,
                        onUnityMessage:
                            Provider.of<UnityNotifier>(context, listen: false)
                                .onUnityMessage,
                        onUnitySceneLoaded:
                            Provider.of<UnityNotifier>(context, listen: false)
                                .onUnitySceneLoaded,
                        fullscreen: false,
                      ),
                    ),
                    Positioned(
                      bottom: 10,
                      right: _width - 280,
                      left: 20,
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        elevation: 10,
                        child: Row(children: [
                          IconButton(
                              onPressed: () {
                                Provider.of<UnityNotifier>(context,
                                        listen: false)
                                    .resetOrigin("reset");
                                // Provider.of<UnityNotifier>(context,
                                //         listen: false)
                                //     .resetOrigin("zero");
                              },
                              icon: const Icon(Icons.compass_calibration)),
                          PopupMenuButton<Item>(
                            onSelected: _select,
                            itemBuilder: (BuildContext context) {
                              return DUMBBELL_ITEMs.skip(0).map((Item item) {
                                return PopupMenuItem<Item>(
                                  value: item,
                                  child: Text(item.title),
                                );
                              }).toList();
                            },
                          ),
                          Selector<DataMonitor, String>(
                              selector: (_, provider) =>
                                  provider.freeWeightItem,
                              builder: (_, item, __) {
                                return Text(item);
                              }),
                          const SizedBox(width: 15),
                          Selector<BTNotifier, BluetoothCharacteristic?>(
                              selector: (_, notifier) =>
                                  notifier.margCharacteristic,
                              builder: (_, c, __) {
                                if (c == null) {
                                  return const Icon(
                                      Icons.fitness_center_outlined);
                                } else {
                                  return Selector<BTNotifier, bool>(
                                      selector: (_, notifier) => notifier
                                          .margCharacteristic!.isNotifying,
                                      builder: (_, isNotifying, __) {
                                        if (isNotifying) {
                                          return IconButton(
                                              icon: const Icon(
                                                  Icons.fitness_center),
                                              color: Colors.yellowAccent,
                                              onPressed: () =>
                                                  locator<BTNotifier>()
                                                      .unsubscribe("MARG"));
                                        } else {
                                          return IconButton(
                                              icon: const Icon(
                                                  Icons.fitness_center),
                                              onPressed: () {
                                                debugPrint(
                                                    "listenCharacteristic");
                                                locator<BTNotifier>()
                                                    .listenCharacteristic(
                                                        "MARG");
                                              });
                                        }
                                      });
                                }
                              }),
                        ]),
                      ),
                    ),
                  ],
                ),
              )
            : Card(
                margin: const EdgeInsets.all(5),
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.resolveWith<Color>(
                        (Set<MaterialState> states) {
                          if (states.contains(MaterialState.pressed)) {
                            return Theme.of(context)
                                .colorScheme
                                .primary
                                .withOpacity(0.5);
                          }
                          return const Color(0x30777777);
                        },
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const <Widget>[
                        Text(
                          'Motion Tracking',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color(0xFF00C8F4),
                              fontWeight: FontWeight.bold,
                              fontSize: 30),
                        ),
                      ],
                    ),
                    onPressed: () {
                      debugPrint("opening unity view...");
                      Provider.of<UnityNotifier>(context, listen: false)
                          .isTriggered = true;
                    },
                  ),
                ),
              ),
      ),
    );
  }

  // 選擇想要做的運動項目
  void _select(Item choice) {
    final selectedIndex = DUMBBELL_ITEMs.indexOf(choice);
    Provider.of<DataMonitor>(context, listen: false).itemIndex = selectedIndex;
    Provider.of<ArtificialIntelligent>(context, listen: false).internalState =
        (selectedIndex != 0)
            ? InternalState(initLabel: selectedIndex)
            : null; // create a new instance.
  }
}
