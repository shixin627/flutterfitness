import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';
import 'package:provider/provider.dart';

import '../data/constant.dart';
import '../models/freeweight_item.dart';
import '../models/internel_state.dart';
import '../providers/artificial_intelligent.dart';
import '../providers/bluetooth_notifier.dart';
import '../providers/data_monitor.dart';
import '../providers/unity_provider.dart';
import '../service_locator.dart';

class SimpleScreen extends StatefulWidget {
  const SimpleScreen({Key? key}) : super(key: key);

  @override
  _SimpleScreenState createState() => _SimpleScreenState();
}

class _SimpleScreenState extends State<SimpleScreen> {
  static final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>();

  double _sliderValue = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    Provider.of<UnityNotifier>(context, listen: false)
        .unityWidgetController!
        .dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text('Simple Screen'),
      ),
      body: Card(
          margin: const EdgeInsets.all(8),
          clipBehavior: Clip.antiAlias,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          child: Stack(
            children: [
              UnityWidget(
                onUnityCreated:
                    Provider.of<UnityNotifier>(context, listen: false)
                        .onUnityCreated,
                onUnityMessage:
                    Provider.of<UnityNotifier>(context, listen: false)
                        .onUnityMessage,
                onUnitySceneLoaded:
                    Provider.of<UnityNotifier>(context, listen: false)
                        .onUnitySceneLoaded,
                fullscreen: false,
              ),
              Positioned(
                bottom: 10,
                right: MediaQuery.of(context).size.width - 280,
                left: 20,
                child: Card(
                  clipBehavior: Clip.antiAlias,
                  elevation: 10,
                  child: Row(children: [
                    IconButton(
                        onPressed: () {
                          Provider.of<UnityNotifier>(context, listen: false)
                              .resetOrigin("reset");
                          // Provider.of<UnityNotifier>(context,
                          //         listen: false)
                          //     .resetOrigin("zero");
                        },
                        icon: const Icon(Icons.compass_calibration)),
                    PopupMenuButton<Item>(
                      onSelected: _select,
                      itemBuilder: (BuildContext context) {
                        return DUMBBELL_ITEMs.skip(0).map((Item item) {
                          return PopupMenuItem<Item>(
                            value: item,
                            child: Text(item.title),
                          );
                        }).toList();
                      },
                    ),
                    Selector<DataMonitor, String>(
                        selector: (_, provider) => provider.freeWeightItem,
                        builder: (_, item, __) {
                          return Text(item);
                        }),
                    const SizedBox(width: 15),
                    Selector<BTNotifier, BluetoothCharacteristic?>(
                        selector: (_, notifier) => notifier.margCharacteristic,
                        builder: (_, c, __) {
                          if (c == null) {
                            return const Icon(Icons.fitness_center_outlined);
                          } else {
                            return Selector<BTNotifier, bool>(
                                selector: (_, notifier) =>
                                    notifier.margCharacteristic!.isNotifying,
                                builder: (_, isNotifying, __) {
                                  if (isNotifying) {
                                    return IconButton(
                                        icon: const Icon(Icons.fitness_center),
                                        color: Colors.yellowAccent,
                                        onPressed: () => locator<BTNotifier>()
                                            .unsubscribe("MARG"));
                                  } else {
                                    return IconButton(
                                        icon: const Icon(Icons.fitness_center),
                                        onPressed: () {
                                          debugPrint("listenCharacteristic");
                                          locator<BTNotifier>()
                                              .listenCharacteristic("MARG");
                                        });
                                  }
                                });
                          }
                        }),
                  ]),
                ),
              ),
            ],
          )),
    );
  }

  // 選擇想要做的運動項目
  void _select(Item choice) {
    final selectedIndex = DUMBBELL_ITEMs.indexOf(choice);
    Provider.of<DataMonitor>(context, listen: false).itemIndex = selectedIndex;
    Provider.of<ArtificialIntelligent>(context, listen: false).internalState =
        (selectedIndex != 0)
            ? InternalState(initLabel: selectedIndex)
            : null; // create a new instance.
  }
}
