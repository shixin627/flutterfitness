abstract class ExerciseEvent {} //作為觸發個個運動事件的自訂義抽象物件

class StartEvent extends ExerciseEvent {
  final int sets; /// 第[sets]組
  final int weight; // 當組初始負重(KG)
  final int? restBetweenSets; // 上一組和本組間的休息時間(s)
  StartEvent(this.sets, this.weight, {this.restBetweenSets});
}

class RepeatEvent extends ExerciseEvent {
  final double accVsLengthIntergration; // 加速度對位移的積分(a^2 * m^2)
  final int duration; // 反覆期間(s)
  final int repetitions; // 反覆次數(次)
  RepeatEvent(this.accVsLengthIntergration, this.duration, this.repetitions);
}

class MovementEvent extends ExerciseEvent {
  final double data; //當前位置(cm)
  MovementEvent(this.data);
}

class RestEvent extends ExerciseEvent {}
