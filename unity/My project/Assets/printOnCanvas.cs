﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class printOnCanvas : MonoBehaviour
{
    Text txt;
    private GameObject thePlayer;
    private Rotate rotateClass;

    // Use this for initialization
    void Awake()
    {
        thePlayer = GameObject.Find("FreeCube");
        rotateClass = thePlayer.GetComponent<Rotate>();
    }

    void Start()
    {
        txt = gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        string quat_of_elbow = "Quaternion of elbow: " + rotateClass.myElbowQuaternion.ToString("0.000") + "\n";
        string quat_of_wrist = "Quaternion of wrist: " + rotateClass.myWristQuaternion.ToString("0.000") + "\n";

        string global_vector_of_upperArm = "Global vector of upperArm: " + rotateClass.Vt_elbow.ToString("0.000") + "\n";
        string global_vector_of_lowerArm = "Global vector of lowerArm: " + rotateClass.Vt_wrist.ToString("0.000") + "\n";

        string unity_vector_of_upperArm = "Unity vector of upperArm: " + rotateClass.unit_Ve.ToString("0.000") + "\n";
        string unity_vector_of_lowerArm = "Unity vector of lowerArm: " + rotateClass.unit_Vw.ToString("0.000");
        
        txt.text = quat_of_elbow + quat_of_wrist + global_vector_of_upperArm + global_vector_of_lowerArm + unity_vector_of_upperArm + unity_vector_of_lowerArm;
    }
}
