// 與nRF52840連線, 控制其RGB
import 'dart:async';
import 'dart:convert' show utf8;

import 'package:control_pad/control_pad.dart';
import 'package:control_pad/models/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

const String SERVICE_UUID = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
const String CHARACTERISTIC_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";
const String TARGET_DEVICE_ID = "EA:23:E1:E5:68:6F";

class JoyPad extends StatefulWidget {
  @override
  _JoyPadState createState() => _JoyPadState();
}

class _JoyPadState extends State<JoyPad> {
  FlutterBlue flutterBlue = FlutterBlue.instance;
  StreamSubscription<ScanResult>? scanSubScription;

  BluetoothDevice? targetDevice;
  BluetoothCharacteristic? targetCharacteristic;

  String connectionText = "";

  @override
  void initState() {
    super.initState();
    startScan();
  }

  startScan() {
    setState(() {
      connectionText = "Start Scanning";
    });

    scanSubScription = flutterBlue.scan().listen((scanResult) {
      if (scanResult.device.id.toString() == TARGET_DEVICE_ID) {
        print('DEVICE found');
        stopScan();
        setState(() {
          connectionText = "Found Target Device";
        });

        targetDevice = scanResult.device;
        connectToDevice();
      }
    }, onDone: () => stopScan());
  }

  stopScan() {
    scanSubScription?.cancel();
    scanSubScription = null;
    FlutterBlue.instance.stopScan();
  }

  connectToDevice() async {
    if (targetDevice == null) return;

    setState(() {
      connectionText = "Device Connecting";
    });

    await targetDevice!.connect();
    debugPrint('DEVICE CONNECTED');
    setState(() {
      connectionText = "Device Connected";
    });

    discoverServices();
  }

  disconnectFromDevice() {
    if (targetDevice == null) return;

    targetDevice!.disconnect();

    setState(() {
      connectionText = "Device Disconnected";
    });
  }

  discoverServices() async {
    if (targetDevice == null) return;

    List<BluetoothService> services = await targetDevice!.discoverServices();
    for (var service in services) {
      // do something with service
      if (service.uuid.toString() == SERVICE_UUID) {
        for (var characteristic in service.characteristics) {
          if (characteristic.uuid.toString() == CHARACTERISTIC_UUID) {
            targetCharacteristic = characteristic;
            writeData("Hi there, CircuitPython");
            setState(() {
              connectionText = "All Ready with ${targetDevice!.name}";
            });
          }
        }
      }
    }
  }

  writeData(String data) {
    if (targetCharacteristic == null) return;

    List<int> bytes = utf8.encode(data);
    targetCharacteristic!.write(bytes);
  }

  @override
  Widget build(BuildContext context) {
    JoystickDirectionCallback? onDirectionChanged(
        double degrees, double distance) {
      String data =
          "Degree : ${degrees.toStringAsFixed(2)}, distance : ${distance.toStringAsFixed(2)}";
      debugPrint(data);
      return null;
    }

    PadButtonPressedCallback? padBUttonPressedCallback(
        int buttonIndex, Gestures gesture) {
      String data = "";

      /* change the data sent by the joypad buttons to correspond with the commands 
          hardcoded into the CircuitPython device */
      if (buttonIndex == 3) {
        data = "b";
        setState(() {
          connectionText = "Blue LED On";
        });
      } else if (buttonIndex == 1) {
        data = "g";
        setState(() {
          connectionText = "Green LED On";
        });
      } else if (buttonIndex == 2) {
        data = "r";
        setState(() {
          connectionText = "Red LED On";
        });
      } else if (buttonIndex == 0) {
        data = "x";
        setState(() {
          connectionText = "All LEDs Off";
        });
      }
      debugPrint(data);
      writeData(data);
      return null;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(connectionText),
      ),
      body: Container(
        child: targetCharacteristic == null
            ? Center(
                child: Text(
                  "Waiting...",
                  style: TextStyle(fontSize: 24, color: Colors.red),
                ),
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  JoystickView(
                    onDirectionChanged: onDirectionChanged,
                  ),
                  PadButtonsView(
                    padButtonPressedCallback: padBUttonPressedCallback,
                  ),
                ],
              ),
      ),
    );
  }
}
