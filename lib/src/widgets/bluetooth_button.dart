import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_blue/flutter_blue.dart';
import '../providers/bluetooth_notifier.dart';

class BLEbutton extends StatefulWidget {
  const BLEbutton({Key? key}) : super(key: key);

  @override
  _BLEbuttonState createState() => _BLEbuttonState();
}

class _BLEbuttonState extends State<BLEbutton> {
  @override
  Widget build(BuildContext context) {
    var ble = Provider.of<BTNotifier>(context);
    if (ble.isBLEon) {
      if (ble.isAvailable) {
        switch (ble.targetStatus) {
          case BluetoothDeviceState.disconnected:
            return Opacity(
              opacity: 1,
              child: IconButton(
                  icon: Icon(
                    Icons.bluetooth_searching,
                    color: Colors.blue[900],
                  ),
                  onPressed: () {
                    ble.connectToDevice();
                  }),
            );
          case BluetoothDeviceState.connecting:
            return const Opacity(
              opacity: 0.5,
              child: Icon(
                Icons.bluetooth_searching,
                color: Colors.lightBlueAccent,
              ),
            );
          case BluetoothDeviceState.connected:
            return Opacity(
              opacity: 1,
              child: IconButton(
                  icon: const Icon(
                    Icons.bluetooth_connected,
                    color: Colors.yellow,
                  ),
                  onPressed: () {
                    ble.disconnectFromDevice();
                  }),
            );
          case BluetoothDeviceState.disconnecting:
            return const Opacity(
              opacity: 0.5,
              child: Icon(
                Icons.bluetooth_searching,
                color: Colors.pinkAccent,
              ),
            );
          default:
            return Container();
        }
      } else {
        return Opacity(
          opacity: 0.5,
          child: IconButton(
            icon: const Icon(Icons.bluetooth_searching),
            color: Colors.orangeAccent,
            onPressed: ble.startScan,
          ),
        );
      }
    } else {
      return const Opacity(
        opacity: 1,
        child: Icon(
          Icons.bluetooth_disabled,
          color: Colors.red,
        ),
      );
    }
  }
}
