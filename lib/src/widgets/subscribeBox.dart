import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import '../providers/bluetooth_notifier.dart';
import 'package:provider/provider.dart';

class SubscribeBox extends StatelessWidget {
  const SubscribeBox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Selector<BTNotifier, BluetoothCharacteristic?>(
            selector: (_, notifier) => notifier.margCharacteristic,
            builder: (_, c, __) {
              if (c != null && c.isNotifying == true) {
                return IconButton(
                    icon: const Icon(Icons.fitness_center),
                    color: Colors.yellowAccent,
                    onPressed: () =>
                        Provider.of<BTNotifier>(context, listen: false)
                            .unsubscribe("MARG"));
              } else {
                return IconButton(
                    icon: const Icon(Icons.fitness_center),
                    color: Colors.white,
                    onPressed: () =>
                        Provider.of<BTNotifier>(context, listen: false)
                            .listenCharacteristic("MARG"));
              }
            }),
        Selector<BTNotifier, BluetoothCharacteristic?>(
            selector: (_, notifier) => notifier.hrmCharacteristic,
            builder: (_, c, __) {
              if (c != null && c.isNotifying == true) {
                return IconButton(
                    icon: const Icon(Icons.favorite_border),
                    color: Colors.yellowAccent,
                    onPressed: () => Provider.of<BTNotifier>(context, listen: false).unsubscribe("HRM"));
              } else {
                return IconButton(
                    icon: const Icon(Icons.favorite_border),
                    color: Colors.white,
                    onPressed: () => Provider.of<BTNotifier>(context, listen: false).listenCharacteristic("HRM"));
              }
            }),
        IconButton(
            icon: const Icon(Icons.access_time),
            color: Colors.white,
            onPressed: () => Provider.of<BTNotifier>(context, listen: false).sendRealTimeData()),
      ],
    );
  }
}
