abstract class CounterEvent {} //作為觸發事件的自訂義抽象物件

class IncrementEvent extends CounterEvent {}

class DecrementEvent extends CounterEvent {}

class UpdateEvent extends CounterEvent {
  //帶有argument的物件
  final int newValue;
  UpdateEvent(this.newValue);
}
