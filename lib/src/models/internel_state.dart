import 'dart:convert';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import '/src/data/constant.dart';
import '/src/models/freeweight_item.dart';
import '/src/models/date_model.dart';
import '/src/blocs/exercise_bloc.dart';
import '/src/blocs/exercise_event.dart';
import '../services/local_database_service.dart';
import '../data/global.dart';

//自由重量訓練的運動處方變項之內部狀態
class InternalState extends ChangeNotifier {
  InternalState({int initLabel = 0}) {
    label = initLabel;
  }

  int _label = 0;
  int get label => _label;
  set label(int newValue) {
    if (newValue != _label) {
      _label = newValue;
      notifyListeners();
    }
  }

  ExerciseBloc? bloc;
  Item get item => DUMBBELL_ITEMs[label];

  //狀態:當前運動項目的反覆運動期間
  int _state = 0;
  int get state => _state;
  set state(int val) {
    if (val != _state) {
      _state = val;
      notifyListeners();
    }
  }

  List<double> lowerBuffer = [];
  List<double> upperBuffer = [];

  List<double> valleys = [];
  List<double> peaks = [];

  void detectPeak() {
    final peak = upperBuffer.reduce(max);
    peaks.add(peak);
    upperBuffer.clear();
  }

  void detectValley() {
    final valley = lowerBuffer.reduce(min);
    valleys.add(valley);
    lowerBuffer.clear();
  }

  // List<double> positions = List.filled(3, 0.0);
  // List<double> velocities = List.filled(2, 0.0);

  bool isTraining = false; //旗標(休息中/訓練中)
  DateTime? _ts; //每次開始計算反覆運動的起始時間點
  int numOfSets = 0; //組數
  int get repetition => peaks.length; //波峰的數量即為反覆次數

  //移除所有局部最大及最小值
  void _clearExtremum() {
    peaks.clear();
    valleys.clear();
  }

  double get realLowBound => item.lowerThreshold * (upperArm + foreArm);

  /// 用前臂與上臂的單位向量來更新運動狀態
  void updateWith(double verticalComponentOfUnitUpperArm,
      double verticalComponentOfUnitLowerArm) {
    //是否啟動運動紀錄儀錶板
    if (bloc != null) {
      if (!isTraining) {
        //如果現在使用者處於休息狀態，
        //偵測是否開始運動?
        if (label != 0) {
          //如果剛要開始運動
          isTraining = true; //改變旗標(訓練中)
          numOfSets++;
          _clearExtremum();
          if (_ts != null) {
            //如果不是第一組
            final diff = DateTime.now().difference(_ts!).inSeconds;
            bloc!.exerciseEventSink.add(StartEvent(numOfSets, Global.bellWeight,
                restBetweenSets: diff)); //要記得添加上一組到這一組間的休息時間
          } else {
            bloc!.exerciseEventSink
                .add(StartEvent(numOfSets, Global.bellWeight));
          }
          _ts = DateTime.now();
        }
      } else {
        if (label != 0) {
          //如果辨識結果是正在運動
          final realPosition = getRealPositionFrom(
              verticalComponentOfUnitUpperArm, verticalComponentOfUnitLowerArm);
          bloc!.exerciseEventSink.add(MovementEvent(realPosition * 100));

          // 波峰波谷檢測模組
          final virtualUnitPosition =
              verticalComponentOfUnitUpperArm + verticalComponentOfUnitLowerArm;
          switch (state) {
            case 0:
              lowerBuffer.add(realPosition);
              if (virtualUnitPosition > item.lowerThreshold) {
                detectValley();
                state = 1;
              }
              break;
            case 1:
              if (virtualUnitPosition > item.upperThreshold) {
                state = 2;
              }
              break;
            case 2:
              upperBuffer.add(realPosition);
              if (virtualUnitPosition < item.upperThreshold) {
                detectPeak();
                state = 3;
              }
              break;
            case 3:
              if (virtualUnitPosition < item.lowerThreshold) {
                state = 0;
                Duration diff = DateTime.now().difference(_ts!);
                _ts = DateTime.now();
                int durationInSeconds = diff.inSeconds; //計算當次反覆時間
                bloc!.exerciseEventSink.add(RepeatEvent(
                    9.8 * 2 * (peaks.last - valleys.last),
                    durationInSeconds,
                    repetition)); //觸發反覆事件
              }
              break;
            default:
              break;
          }
        } else {
          final diff = DateTime.now().difference(_ts!).inSeconds;
          if (diff > 15) {
            // 正要休息
            isTraining = false;
          }
        }
      }
    }
  }

  void processState(double virtualPosi) {
    if (label != 0) {
      switch (state) {
        case 0:
          lowerBuffer.add(virtualPosi);
          if (virtualPosi > item.lowerThreshold) {
            detectValley();
            state = 1;
          }
          break;
        case 1:
          if (virtualPosi > item.upperThreshold) {
            state = 2;
          }
          break;
        case 2:
          upperBuffer.add(virtualPosi);
          if (virtualPosi < item.upperThreshold) {
            detectPeak();
            state = 3;
          }
          break;
        case 3:
          if (virtualPosi < item.lowerThreshold) {
            state = 0;
          }
          break;
        default:
          break;
      }
    }
  }

  Future<void> saveDataset(String userId, int itemId) async {
    if (bloc != null) {
      if (bloc!.workTotal != 0) {
        final timeString = Date.dateFormat.format(DateTime.now());
        await LocalDatabaseService.open();
        await LocalDatabaseService.insertData({
          'user_id': userId,
          'item_id': itemId,
          'datetime': timeString,
          'dataset': json.encode(bloc!.theSet),
          'interRest': json.encode(bloc!.restBetweenSets),
        });
        initVar();
      }
    }
  }

  //結束任務，重置所有運動參數
  void initVar() {
    bloc = null;
    _ts = null;
    numOfSets = 0;
    isTraining = false;
    _clearExtremum();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
