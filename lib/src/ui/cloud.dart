import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '/src/data/constant.dart';
import 'dart:async';
import '../models/ExerciseData_model.dart';
import '../models/history_line.dart';
import '../models/permission.dart';
import '../models/user_model.dart';
import '../models/date_model.dart';
import '../models/route_paths.dart' as routes;
import 'day_screen.dart';
import 'item_screen.dart';
import '../services/cloud_service.dart';
import '../services/localstorage_service.dart';
import '../services/navigation_service.dart';
import '../widgets/material.dart';
import '../service_locator.dart';
import '../widgets/theme.dart';
import '../widgets/itemCard.dart';
import 'login_server.dart';

class DatabaseManagement extends StatelessWidget {
  //判斷是否已經登錄過
  final LocalStorageService storageService = locator<LocalStorageService>();

  @override
  Widget build(BuildContext context) {
    UserModel mySavedUser =
        storageService.user ?? UserModel(id: 0, email: 'null', name: 'null');
    var _id = mySavedUser.id;
    if (_id > 0) {
      //若使用者已經登錄過
      return CloudViewer(); //進入雲端服務頁面
    } else {
      //若使用者尚未登錄
      return LoginPage(); //進入登錄頁面
    }
  }
}

class CloudViewer extends StatefulWidget {
  CloudViewerState createState() => CloudViewerState();
}

class CloudViewerState extends State {
  final LocalStorageService storageService = locator<LocalStorageService>();
  final NavigationService _navigationService = locator<NavigationService>();
  final HistoryData _history = HistoryData();
  String? _pickedDate;
  List<DataSet> _pickedList = [];
  List<String> _dateList = [];
  UserModel? _user;
  int _actualIndex = 0;
  Permission? _permission;
  bool? _isEnable;

  void Function(bool?)? _changedCloud(bool? isCheck) {
    _isEnable = isCheck!;
    _permission!.isCloudDBenable = isCheck;
    storageService.permission = _permission;
    setState(() {});
  }

  final List<String> _lineDropdownItems = [
    '總做功量',
    '總運動期間',
    '平均功率',
    '總訓練量',
  ];

  //刷新資料
  Future<void> refreshList() async {
    DataSet.list = await CloudService.fetchSessions(_user!.id);
    _pickedList = DataSet.list;
    setState(() {
      _dateList = assortWithDate(_pickedList);
    });
    return;
  }

  Future _selectDate() async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(DateTime.now().year + 1),
    );
    if (picked != null) {
      _pickedDate = Date.ymdFormat.format(picked);
      List<DataSet> _list = <DataSet>[];
      for (var dataset in DataSet.list) {
        var _date = dataset.datetime.split(" "); //將日期部分的字串提出
        if (_date[0] == _pickedDate) {
          ///與選擇的日期比較
          _list.add(dataset);

          ///將當天每項運動加入列表 [List<DataSet>]
        }
      }
      _pickedList = _list;
      setState(() {
        _dateList = assortWithDate(_pickedList);
      });
    }
  }

  //創造一個為運動日的日期列表
  List<String> assortWithDate(List<DataSet> _list) {
    List<String> _stringList = <String>[];
    String _currentDateStr = 'current';
    String _previousDateStr = 'previous';

    _history.initial(_history.baseIndex);

    for (var dataset in _list) {
      _currentDateStr = dataset.datetime.split(" ")[0];
      if (_currentDateStr != _previousDateStr) {
        _stringList.add(_currentDateStr);
        _previousDateStr = _currentDateStr;
        _history.addNextList(Date.ymdFormat.parse(_currentDateStr), dataset,
            _history.baseIndex); //加入至歷史曲線圖中的一天
      } else {
        _history.updateCurrentList(dataset); //若是同一天則整合當天的運動數據
      }
    }
    return _stringList;
  }

  //移動到所選擇日期的運動列表頁面
  navigateToTheDate(BuildContext context, String _date) {
    List<DataSet> _dayList = <DataSet>[];
    for (var dataset in _pickedList) {
      if (_date == dataset.datetime.split(" ")[0]) {
        _dayList.add(dataset);
      }
    }
    _navigationService.navigateTo(routes.DayRoute,
        arguments: TheDayScreen(_date, _dayList));
  }

  //進入到某項運動的歷史數據頁面
  navigateToItem(BuildContext context, int _index) {
    List<DataSet> _itemList = <DataSet>[];
    for (var _dataset in _pickedList) {
      if (_index + 1 == _dataset.itemID) {
        _itemList.add(_dataset);
      }
    }
    _navigationService.navigateTo(routes.ItemRoute,
        arguments: TheItemScreen(_index + 1, _itemList));
  }

  // User Logout Function.
  void logout() {
    _user!.id = 0;
    storageService.user = _user;
    _navigationService.navigatorKey.currentState!.pushNamedAndRemoveUntil(
        routes.DBManagement, (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    super.initState();
    _permission = storageService.permission ??
        Permission(isCloudDBenable: true, isLocalDBenable: true);
    _isEnable = _permission!.isCloudDBenable;
    _user = storageService.user;
    _history.index = _lineDropdownItems[_actualIndex];
    _history.baseIndex = 'on_Date';
    refreshList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: colorBackground,
        title: const Text(
          '雲端服務',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w700,
            fontSize: 30.0,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.all_inclusive),
            onPressed: () => setState(() {
              _pickedList = DataSet.list;
              _dateList = assortWithDate(_pickedList);
            }),
            color: colorPrimary,
          ),
          IconButton(
            icon: const Icon(Icons.date_range),
            onPressed: _selectDate,
            color: colorPrimary,
          ),
        ],
      ),
      drawer: Drawer(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: ListView(
            children: <Widget>[
              UserAccountsDrawerHeader(
                accountName: Text("歡迎，${_user!.name}"),
                accountEmail: Text("Email: ${_user!.email}"),
                currentAccountPicture: GestureDetector(
                  onTap: () {},
                  child: const CircleAvatar(backgroundColor: Colors.white),
                ),
              ),
              const Divider(),
              ListTile(
                title: const Text("登出"),
                trailing: const Icon(Icons.cloud_off),
                onTap: logout,
              ),
              CheckboxListTile(
                value: _isEnable,
                onChanged: _changedCloud,
                title: const Text('雲端資料庫'),
                controlAffinity: ListTileControlAffinity.leading,
                subtitle: const Text('開啟本地與雲端資料同步功能'),
                secondary: const Icon(Icons.update),
              ),
            ],
          ),
        ),
      ),
      body: Visibility(
        visible: _isEnable!,
        child: ScreenBackground(
          child: RefreshIndicator(
            child: Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
              child: StaggeredGrid.count(
                crossAxisCount: 2,
                crossAxisSpacing: 12.0,
                mainAxisSpacing: 12.0,
                children: <Widget>[
                  StaggeredGridTile.count(
                    crossAxisCellCount: 2,
                    mainAxisCellCount: 1,
                    child: DashboardElement(
                      child: Padding(
                        padding: const EdgeInsets.all(24.0),
                        child: ListView(
                          children: _dateList
                              .map(
                                (data) => Column(
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () {
                                        navigateToTheDate(context, data);
                                      },
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                20, 5, 0, 5),
                                            child: Text(data,
                                                style: const TextStyle(
                                                    fontSize: 21),
                                                textAlign: TextAlign.left),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const Divider(color: Colors.black),
                                  ],
                                ),
                              )
                              .toList(),
                        ),
                      ),
                    ),
                  ),
                  StaggeredGridTile.count(
                    crossAxisCellCount: 2,
                    mainAxisCellCount: 1,
                    child: DashboardElement(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                DropdownButton(
                                  isDense: true,
                                  value: _history.index,
                                  onChanged: (value) {
                                    if (value != null) {
                                      final text = value as String;
                                      setState(() {
                                        _history.index = text;
                                        _actualIndex =
                                            _lineDropdownItems.indexOf(
                                                value); // Refresh the chart
                                      });
                                    }
                                  },
                                  items: _lineDropdownItems.map((String title) {
                                    return DropdownMenuItem(
                                      value: title,
                                      child: Text(title,
                                          style: const TextStyle(
                                              color: Colors.blue,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14.0)),
                                    );
                                  }).toList(),
                                ),
                              ],
                            ),
                            const Padding(
                                padding: EdgeInsets.only(bottom: 20.0)),
                            Expanded(
                              child: TimeSeriesChart(_history, animate: true),
                            ),
                          ],
                        ),
                      ),
                      onTap: () => debugPrint(
                          _history.maptoLine[_actualIndex].toString()),
                    ),
                  ),
                  StaggeredGridTile.count(
                    crossAxisCellCount: 2,
                    mainAxisCellCount: 2,
                    child: DashboardElement(
                      child: Center(
                        child: Container(
                          height: 500,
                          child: Swiper(
                            itemCount: itemLen,
                            loop: false,
                            viewportFraction: 0.8,
                            scale: 0.82,
                            outer: true,
                            pagination: const SwiperPagination(
                              alignment: Alignment.bottomCenter,
                              margin: EdgeInsets.all(16.0),
                            ),
                            onTap: (index) => navigateToItem(context, index),
                            itemBuilder: (BuildContext context, int index) {
                              return Center(
                                child: Container(
                                  height: 360,
                                  child: ItemCard(
                                    item: ITEM_NAME[index],
                                    color: colorGrey,
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
                // staggeredTiles: [
                //   StaggeredTile.extent(2, 240.0),
                //   StaggeredTile.extent(2, 250.0),
                //   StaggeredTile.extent(2, 400.0),
                // ],
              ),
            ),
            onRefresh: refreshList,
          ),
        ),
      ),
    );
  }
}
