import 'package:flutter/material.dart';
import '../widgets/material.dart';

class OtherScreen extends StatelessWidget {
  final String name;

  OtherScreen(this.name);

  @override
  Widget build(BuildContext context) {
    return ScreenBackground(
      child: Center(
        child: Text(this.name, style: Theme.of(context).textTheme.bodyText1),
      ),
    );
  }
}
