import 'package:flutter/material.dart';

final colorBackground = const Color(0xFFF3F4F7);
final colorPrimary = const Color(0xFF35465B);
final colorAccent = const Color(0xFF7576FD);
final colorGrey = const Color(0xFFA5ADB7);

final ThemeData androidTheme = ThemeData(
    brightness: Brightness.light,
    backgroundColor: colorBackground,
    primaryColor: colorPrimary,
    accentColor: colorAccent,
    splashColor: colorAccent,
    disabledColor: colorGrey);

final ThemeData iOSTheme = ThemeData(
  brightness: Brightness.dark,
  backgroundColor: Color(0x00030407),
  primarySwatch: Colors.orange,
  primaryColor: Colors.grey[100],
  primaryColorBrightness: Brightness.light,
);

final TextStyle textStyle = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

///
const double kBottomContainerHeight = 80.0;
const Color kBottomContainerColor = Color(0xFFEB1555);
const Color kActiveCardColor = Color(0xFF1D1E33);
const Color kInactiveCardColor = Color(0xFF111328);

const kLabelTextStyle = TextStyle(
  fontSize: 16.0,
  color: Color(0xFF8D8E98),
);

const kNumberTextStyle = TextStyle(fontSize: 30.0, fontWeight: FontWeight.w900);

const kTitleTextStyle = TextStyle(
  fontSize: 50.0,
  fontWeight: FontWeight.bold,
);

const kResultTextStyle = TextStyle(
  color: Color(0xFF24D876),
  fontSize: 22.0,
  fontWeight: FontWeight.bold,
);

const kBMITextStyle = TextStyle(
  fontSize: 100.0,
  fontWeight: FontWeight.bold,
);

const kResultBodyTextStyle = TextStyle(
  fontSize: 22.0,
);
///