import '../models/freeweight_item.dart';

const List<int> BellWeights = [
  5,
  9,
  14,
  18,
  23,
  27,
  32,
  36,
  41,
  45,
  50,
  54,
  59,
  64,
  68,
  73,
  77,
  82,
  86,
  91
];

const int current_total_number = 3; //現在周圍中有可能存在的項目數量
int get itemLen => ITEM_NAME.length; //所有項目的數量

const Map<int, List<String>> mapIDToItem = {
  // 自由重量訓練
  0: ['free-weight', 'Biceps Curl'],
  1: ['free-weight', 'Lateral Raise'],
  2: ['free-weight', 'Shoulder Press'],
  // 胸部重量訓練(Chest weight training)
  3: ['machine', '胸部推舉'],
  // 肩膀與手臂訓練(Shoulders and arms training)
  4: ['machine', '臂部彎舉'],
  5: ['machine', '側平舉'],
  6: ['machine', '分動式肩部推舉'],
  7: ['machine', 'Multi-position Press'],
  8: ['machine', '分動式坐姿划船'],
  9: ['machine', '坐姿腿部彎舉'],
  10: ['machine', '腿部伸屈'],
  11: ['machine', '髖關節內收'],
  12: ['machine', '俯臥腿彎舉'],
  13: ['machine', '腿部推舉'],
  14: ['machine', '小腿推舉'],
  15: ['machine', '腿部伸屈彎舉'],
  16: ['machine', '髖關節旋轉'],
  17: ['machine', '軀幹旋轉'],
  18: ['machine', '分動式背部下拉'],
  19: ['machine', 'Lat Pulldown-low Row'],
  20: ['machine', 'Adjustable Cable Crossover'],
};

const CATEGORIES = [
  'free-weight',
  'free-weight',
  'free-weight',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
  'machine',
];

const List<String> ITEM_NAME = [
  // 自由重量訓練(手)
  'Biceps Curl',
  'Lateral Raise',
  'Shoulder Press',
  // 手部
  '胸部推舉',
  '臂部彎舉',
  '側平舉',
  '分動式肩部推舉',
  'Multi-position Press',
  '分動式坐姿划船',
  //腳部
  '坐姿腿部彎舉',
  '腿部伸屈',
  '髖關節內收',
  '俯臥腿彎舉',
  '腿部推舉',
  '小腿推舉',
  '腿部伸屈彎舉',
  '髖關節旋轉',
  //身體
  '軀幹旋轉',
  '分動式背部下拉',
  'Lat Pulldown-low Row',
  'Adjustable Cable Crossover'
];

List<Item> DUMBBELL_ITEMs = const [
  Item(title: 'non_exercise', lowerThreshold: 0.0, upperThreshold: 0.0),
  Item(
      title: 'biceps_curl',
      lowerThreshold: -1.6,
      upperThreshold: -0.15), // -1.3 ,-0.15
  Item(title: 'lateral_raise', lowerThreshold: -1.65, upperThreshold: -0.55),
  Item(title: 'shoulder_press', lowerThreshold: 1, upperThreshold: 1.65),
];
const defaultImageAsset = 'assets/images/free-weight/biceps_curl.png';
const Map<String, String> ImageAssets = {
  //free-weight
  'Biceps Curl': 'assets/images/free-weight/biceps_curl.png',
  'Lateral Raise': 'assets/images/free-weight/lateral_raise.png',
  'Shoulder Press': 'assets/images/free-weight/shoulder_press.png',
  //手部
  '胸部推舉': 'assets/images/machine/chest_press.png',
  // 肩膀與手臂訓練(Shoulders and arms training)
  '臂部彎舉': 'assets/images/machine/arm_curl.gif',
  '側平舉': 'assets/images/machine/lateral_raise.png', //Lateral Raise
  '分動式肩部推舉':
      'assets/images/machine/converging_shoulder_press.png', //Converging Shoulder Press
  'Multi-position Press':
      'assets/images/machine/multi-position_press.png', //Multi-position Press
  '分動式坐姿划船':
      'assets/images/machine/diverging_seated_row.png', //Diverging Seated Row
  '坐姿腿部彎舉': 'assets/images/machine/seated_leg_curl.png', //Seated Leg Curl
  '腿部伸屈': 'assets/images/machine/leg_extension.png', //Leg Extension
  '髖關節內收':
      'assets/images/machine/hip_adduction.png', //Hip Adduction / Hip Abduction
  '俯臥腿彎舉': 'assets/images/machine/prone_leg_curl.png', //Prone Leg Curl
  '腿部推舉': 'assets/images/machine/leg_press.png', //Leg Press
  '小腿推舉': 'assets/images/machine/calf_press.png', //Calf Press
  '腿部伸屈彎舉': 'assets/images/machine/leg_extension_curl.png', //Leg Extension Curl
  '髖關節旋轉': 'assets/images/machine/rotary_hip.png', //Rotary Hip
  '軀幹旋轉': 'assets/images/machine/rotary_torso.png', //Rotary Torso
  '分動式背部下拉':
      'assets/images/machine/diverging_lat_pulldown.gif', //Diverging Lat Pulldown
  'Lat Pulldown-low Row': 'assets/images/machine/lat_Pulldown-low_row.png',
  'Adjustable Cable Crossover':
      'assets/images/machine/adjustable_cable_crossover.png',
};

///BLE
// 機械式
const String M_SERVICE_UUID = "00001523-1212-efde-1523-785feabcd123";
const String M_CHARACTERISTIC_UUID = "00001524-1212-efde-1523-785feabcd123";

const Map<String, int> mapBLEToItemID = {
  '3C:71:BF:88:D6:96': 3, //胸部推舉
  '24:6F:28:17:13:96': 4, //臂部彎舉
  '80:7D:3A:DB:F6:C6': 19, //Lat Pulldown-low Row
};

// 自由重量訓練
const W_SERVICE_UUID = "00001523-1212-efde-1523-785feabcd123";
const Map<String, String> CHARACTERISTIC_UUID_MAP = {
  "MARG": "00001524-1212-efde-1523-785feabcd123",
  "HRM": "00001525-1212-efde-1523-785feabcd123",
  "TIMER": "00001526-1212-efde-1523-785feabcd123"
};
