abstract class SystemEvent {
  List<int> packet;
  SystemEvent(this.packet);
}

class MargEvent extends SystemEvent {
  List<int> packet;
  MargEvent(this.packet) : super(packet);
}

class HeartRateEvent extends SystemEvent {
  List<int> packet;
  HeartRateEvent(this.packet) : super(packet);
}