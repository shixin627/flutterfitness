import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';

class FirebaseService {
  /// upload file to firebase storage
  Future<bool> uploadFile(String path, File file) async {
    return await FirebaseStorage.instance.ref().child(path).putFile(file).then((snapshot) {
      if (snapshot.ref.fullPath == null) {
        return false;
      } else {
        return true;
      }
      // storageReference.getDownloadURL().then((fileURL) {
      //   print(fileURL);
      // });
    });
  }

  /// delete file from firebase storage
  Future<void> deleteFile(String path) async {
    final child = FirebaseStorage.instance.ref().child(path);
    print(child.fullPath);
    child.delete();
  }

  dynamic isExisted(String path) async {
    return FirebaseStorage.instance.ref().child(path).getDownloadURL().then((url) {
      if (url != null) {
        return url;
      } else {
        return null;
      }
    });
  }
}
