﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = 
{
	{ 15151, 0,  1 } /*tableIndex: 0 */,
};
#else
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const char* g_methodExecutionContextInfoStrings[1] = 
{
	"declaration",
};
#else
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[2] = 
{
	{ 0, 1 } /* 0x06000001 System.Boolean UnityEngine.SubsystemRegistration::CreateDescriptor(UnityEngine.SubsystemDescriptor) */,
	{ 0, 0 } /* 0x06000002 System.Void UnityEngine.SubsystemRegistration::.cctor() */,
};
#else
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[1] = { { 0, 0} };
#endif
#if IL2CPP_MONO_DEBUGGER
IL2CPP_EXTERN_C Il2CppSequencePoint g_sequencePointsUnity_Subsystem_Registration[];
Il2CppSequencePoint g_sequencePointsUnity_Subsystem_Registration[26] = 
{
	{ 54017, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 0 } /* seqPointIndex: 0 */,
	{ 54017, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 1 } /* seqPointIndex: 1 */,
	{ 54017, 1, 22, 22, 41, 63, 0, kSequencePointKind_Normal, 0, 2 } /* seqPointIndex: 2 */,
	{ 54017, 1, 22, 22, 41, 63, 5, kSequencePointKind_StepOut, 0, 3 } /* seqPointIndex: 3 */,
	{ 54017, 1, 22, 22, 0, 0, 11, kSequencePointKind_Normal, 0, 4 } /* seqPointIndex: 4 */,
	{ 54017, 1, 22, 22, 22, 37, 13, kSequencePointKind_Normal, 0, 5 } /* seqPointIndex: 5 */,
	{ 54017, 1, 22, 22, 22, 37, 15, kSequencePointKind_StepOut, 0, 6 } /* seqPointIndex: 6 */,
	{ 54017, 1, 24, 24, 17, 103, 21, kSequencePointKind_Normal, 0, 7 } /* seqPointIndex: 7 */,
	{ 54017, 1, 24, 24, 17, 103, 22, kSequencePointKind_StepOut, 0, 8 } /* seqPointIndex: 8 */,
	{ 54017, 1, 24, 24, 17, 103, 28, kSequencePointKind_StepOut, 0, 9 } /* seqPointIndex: 9 */,
	{ 54017, 1, 24, 24, 17, 103, 33, kSequencePointKind_StepOut, 0, 10 } /* seqPointIndex: 10 */,
	{ 54017, 1, 25, 25, 21, 34, 40, kSequencePointKind_Normal, 0, 11 } /* seqPointIndex: 11 */,
	{ 54017, 1, 22, 22, 38, 40, 44, kSequencePointKind_Normal, 0, 12 } /* seqPointIndex: 12 */,
	{ 54017, 1, 22, 22, 38, 40, 46, kSequencePointKind_StepOut, 0, 13 } /* seqPointIndex: 13 */,
	{ 54017, 1, 22, 22, 0, 0, 55, kSequencePointKind_Normal, 0, 14 } /* seqPointIndex: 14 */,
	{ 54017, 1, 22, 22, 0, 0, 63, kSequencePointKind_StepOut, 0, 15 } /* seqPointIndex: 15 */,
	{ 54017, 1, 28, 28, 13, 78, 69, kSequencePointKind_Normal, 0, 16 } /* seqPointIndex: 16 */,
	{ 54017, 1, 28, 28, 13, 78, 70, kSequencePointKind_StepOut, 0, 17 } /* seqPointIndex: 17 */,
	{ 54017, 1, 29, 29, 13, 52, 75, kSequencePointKind_Normal, 0, 18 } /* seqPointIndex: 18 */,
	{ 54017, 1, 29, 29, 13, 52, 81, kSequencePointKind_StepOut, 0, 19 } /* seqPointIndex: 19 */,
	{ 54017, 1, 31, 31, 13, 25, 86, kSequencePointKind_Normal, 0, 20 } /* seqPointIndex: 20 */,
	{ 54017, 1, 32, 32, 9, 10, 88, kSequencePointKind_Normal, 0, 21 } /* seqPointIndex: 21 */,
	{ 54018, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 22 } /* seqPointIndex: 22 */,
	{ 54018, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 23 } /* seqPointIndex: 23 */,
	{ 54018, 1, 12, 12, 9, 108, 0, kSequencePointKind_Normal, 0, 24 } /* seqPointIndex: 24 */,
	{ 54018, 1, 12, 12, 9, 108, 0, kSequencePointKind_StepOut, 0, 25 } /* seqPointIndex: 25 */,
};
#else
extern Il2CppSequencePoint g_sequencePointsUnity_Subsystem_Registration[];
Il2CppSequencePoint g_sequencePointsUnity_Subsystem_Registration[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#else
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[] = {
{ "", { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} }, //0 
{ "/Users/linshixin/Code/flutter_fitness/unity/My project/Library/PackageCache/com.unity.subsystemregistration@1.1.0/Runtime/SubsystemRegistration.cs", { 174, 199, 234, 199, 197, 15, 10, 253, 169, 23, 228, 197, 72, 13, 143, 250} }, //1 
};
#else
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = 
{
	{ 7331, 1 },
};
#else
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodScope g_methodScopes[2] = 
{
	{ 0, 90 },
	{ 13, 44 },
};
#else
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[2] = 
{
	{ 90, 0, 2 } /* System.Boolean UnityEngine.SubsystemRegistration::CreateDescriptor(UnityEngine.SubsystemDescriptor) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.SubsystemRegistration::.cctor() */,
};
#else
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[1] = { { 0, 0, 0 } };
#endif
IL2CPP_EXTERN_C const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnity_Subsystem_Registration;
const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnity_Subsystem_Registration = 
{
	(Il2CppMethodExecutionContextInfo*)g_methodExecutionContextInfos,
	(Il2CppMethodExecutionContextInfoIndex*)g_methodExecutionContextInfoIndexes,
	(Il2CppMethodScope*)g_methodScopes,
	(Il2CppMethodHeaderInfo*)g_methodHeaderInfos,
	(Il2CppSequencePointSourceFile*)g_sequencePointSourceFiles,
	26,
	(Il2CppSequencePoint*)g_sequencePointsUnity_Subsystem_Registration,
	0,
	(Il2CppCatchPoint*)g_catchPoints,
	1,
	(Il2CppTypeSourceFilePair*)g_typeSourceFiles,
	(const char**)g_methodExecutionContextInfoStrings,
};
