import 'dart:async';
import 'package:flutter/material.dart';
import './src/models/fitness_item.dart';
import './src/service_locator.dart';
import 'package:provider/provider.dart';
import 'src/models/internel_state.dart';
import 'src/motion_tracking_app.dart';
import 'src/providers/artificial_intelligent.dart';
import 'src/providers/data_monitor.dart';
import 'src/services/database.dart';
import 'src/providers/bluetooth_notifier.dart';
import 'src/providers/prefs.dart';
import 'src/providers/unity_provider.dart';
import 'src/providers/menu_provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'src/services/auth.dart';
import 'src/services/firebase.dart';


Future<void> _setupLocator() async {
  setupNavLocator();
  await setupLocator();
}

Future<void> main() async {
  debugPrint('-- main');
  WidgetsFlutterBinding.ensureInitialized();
  debugPrint('-- WidgetsFlutterBinding.ensureInitialized');
  await _setupLocator();
  await Firebase.initializeApp();
  debugPrint('-- main: Firebase.initializeApp');

  runApp(
    MultiProvider(
      providers: [
        StreamProvider.value(
          value: userStream,
          initialData: null,
        ),
        Provider(create: (context) => FirebaseService()),
        Provider(create: (context) => DataBaseSQLite()),
        ChangeNotifierProvider(create: (context) => PrefsNotifier()),
        ChangeNotifierProvider(
          create: (context) => DataMonitor(),
          lazy: true,
        ),
        ChangeNotifierProvider(
          create: (context) => ArtificialIntelligent(),
          lazy: true,
        ),
        ChangeNotifierProxyProvider<ArtificialIntelligent, InternalState>(
            create: (context) => InternalState(),
            update: (_, ai, state) {
              state = ai.internalState!;
              return state;
            }),
        ChangeNotifierProvider(
          create: (context) => UnityNotifier(context.read),
          // lazy: true,
        ),
        ChangeNotifierProvider<FitnessItemModel>(
          create: (context) => FitnessItemModel(),
          lazy: true,
        ),
        ChangeNotifierProvider(
          create: (context) {
            locator.registerLazySingleton(() => BTNotifier(context.read));
            return locator<BTNotifier>();
          }, // 設置一個locator，以便從前代獲取數據
          lazy: true,
        ),
        ChangeNotifierProvider(
          create: (context) => MenuProvider(),
          lazy: true,
        )
      ],
      child: const MotionTrackingApp(), // SmartFitnessApp()
    ),
  );
}
