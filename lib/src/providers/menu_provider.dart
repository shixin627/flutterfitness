import 'package:flutter/foundation.dart';

class MenuProvider extends ChangeNotifier {
  int selectedIndex = 0;

  bool _isDoingFreeWeight = false;
  bool get isDoingFreeWeight => _isDoingFreeWeight;
  set isDoingFreeWeight(bool val) {
    if (val != _isDoingFreeWeight) {
      _isDoingFreeWeight = val;
      notifyListeners();
    }
  }

  void doIt(int index) {
    debugPrint('Do: $index');
    selectedIndex = index;
    isDoingFreeWeight = true;
  }
}
