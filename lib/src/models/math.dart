import 'package:flutter/material.dart';

class Euler {
  double roll;
  double pitch;
  double yaw;
  Euler(this.roll, this.pitch, this.yaw);

  @override
  String toString() {
    return "Euler: $roll,$pitch,$yaw";
  }
}

class Quaternion {
  double w;
  double x;
  double y;
  double z;
  Quaternion(this.w, this.x, this.y, this.z)
      : assert(w.runtimeType == double),
        assert(x.runtimeType == double),
        assert(y.runtimeType == double),
        assert(z.runtimeType == double);
  @override
  String toString() {
    return "$w,$x,$y,$z";
  }
}

class Acceleration {
  double x;
  double y;
  double z;
  Acceleration(this.x, this.y, this.z);
  @override
  String toString() {
    return "$x,$y,$z";
  }
}

class AngularVelocity {
  int x;
  int y;
  int z;
  AngularVelocity(this.x, this.y, this.z);
  @override
  String toString() {
    return "$x,$y,$z";
  }
}

class Heartrate {
  int bps;
  Heartrate(this.bps);
  @override
  String toString() {
    return "$bps";
  }
}

class Arm {
  int systemTime;
  final Acceleration? acceleration;
  final AngularVelocity? angularVelocity;
  final Quaternion? elbowQuaternion;
  final Quaternion? wristQuaternion;
  String? frame;
  Arm(
      {required this.systemTime,
      this.acceleration,
      this.angularVelocity,
      this.elbowQuaternion,
      this.wristQuaternion,
      this.frame});

  bool get isNotEmpty => elbowQuaternion != null && wristQuaternion != null;

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'timestamp': '$systemTime',
      'data':
          '$acceleration,$angularVelocity,$elbowQuaternion,$wristQuaternion',
      'frame': '$frame'
    };
    return map;
  }

  factory Arm.fromMap(Map<String, dynamic> map) {
    int systemTime = map['timestamp'];
    String str = map['data'];
    String frame = map['frame'];
    // if (systemTime == null || str == null) {
    //   return null;
    // }
    List<String> dataset = str.split(',');
    return Arm.construct(systemTime, dataset, frame: frame);
  }

  /// transfer list of string to list of each type of the element.
  static List<dynamic>? _transformType(List<String> dataString) {
    try {
      List<dynamic> result = [];
      result.addAll(List.generate(3, (i) => double.parse(dataString[i]))); //acc
      result
          .addAll(List.generate(3, (i) => int.parse(dataString[i + 3]))); //gyro
      result.addAll(List.generate(
          8, (i) => double.parse(dataString[i + 6]))); //quaternion
      return result;
    } catch (e) {
      debugPrint('catch error: $e');
      return null;
    }
  }

  factory Arm.construct(int systemTime, List<String> data, {String? frame}) {
    final dataSet = _transformType(data)!;
    return Arm(
      systemTime: systemTime,
      acceleration: Acceleration(
          dataSet[0] as double, dataSet[1] as double, dataSet[2] as double),
      angularVelocity: AngularVelocity(
          dataSet[3] as int, dataSet[4] as int, dataSet[5] as int),
      elbowQuaternion: Quaternion(dataSet[6] as double, dataSet[7] as double,
          dataSet[8] as double, dataSet[9] as double),
      wristQuaternion: Quaternion(dataSet[10] as double, dataSet[11] as double,
          dataSet[12] as double, dataSet[13] as double),
      frame: frame, // Global Frame
    );
  }

  String get packet {
    return "$systemTime,$acceleration,$angularVelocity,$elbowQuaternion,$wristQuaternion,$frame";
  }

  List<double> get sixAxisDataset {
    return [
      (acceleration!.x),
      (acceleration!.y),
      (acceleration!.z),
      (angularVelocity!.x.toDouble()),
      (angularVelocity!.y.toDouble()),
      (angularVelocity!.z.toDouble())
    ];
  }

  String get quaternions => "$elbowQuaternion,$wristQuaternion";
}
