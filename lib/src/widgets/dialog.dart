import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import '../providers/bluetooth_notifier.dart';

class ChoiceField extends StatelessWidget {
  final String content;
  final String textLeft;
  final String textRight;
  final VoidCallback? callbackLeft;
  final VoidCallback? callbackRight;
  const ChoiceField(
      {Key? key,
      required this.content,
      required this.textLeft,
      required this.textRight,
      this.callbackLeft,
      this.callbackRight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      content: Text(
        content,
        style: const TextStyle(fontSize: 20),
      ),
      actions: <Widget>[
        CupertinoButton(
          child: Text(textLeft),
          onPressed: callbackLeft,
        ),
        CupertinoButton(
          child: Text(textRight),
          onPressed: callbackRight,
        ),
      ],
    );
  }
}

class RequestMtuDialog extends StatelessWidget {
  final TextEditingController controller;
  const RequestMtuDialog(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: const Text("Request MTU"),
      content: CupertinoTextField(
        controller: controller,
      ),
      actions: <Widget>[
        CupertinoButton(
          child: const Text('(bytes) Send'),
          onPressed: () {
            if (controller.text != '') {
              Provider.of<BTNotifier>(context, listen: false)
                  .setMTUas(int.parse(controller.text))
                  .then((isOk) {
                if (isOk) {
                  Navigator.of(context).pop();
                }
              });
            }
          },
        ),
      ],
    );
  }
}
