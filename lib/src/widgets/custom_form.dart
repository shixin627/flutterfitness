// Create a Form widget.
import 'package:flutter/material.dart';
import '/src/models/body_size.dart';

class MyCustomForm extends StatefulWidget {
  Body? body;
  final void Function(Body) callback;
  MyCustomForm({Key? key, this.body, required this.callback}) : super(key: key);
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController? _upperArmController;
  TextEditingController? _lowerArmController;

  @override
  void initState() {
    super.initState();
    print('My body: ' + widget.body.toString());
    widget.body ??= Body(foreArm: 29.0, upperArm: 28.0);
    _upperArmController =
        TextEditingController(text: widget.body!.upperArm.toString());
    _lowerArmController =
        TextEditingController(text: widget.body!.foreArm.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            TextFormField(
              controller: _upperArmController,
              decoration: const InputDecoration(
                  hintText: '可輸入浮點數', prefix: Text('上臂長'), suffix: Text('cm')),
              validator: (value) {
                if (value != null) {
                  if (value.isEmpty ||
                      double.parse(value).runtimeType != double) {
                    return '1';
                  }
                }
                return null;
              },
            ),
            TextFormField(
              controller: _lowerArmController,
              decoration: const InputDecoration(
                  hintText: '可輸入浮點數', prefix: Text('前臂長'), suffix: const Text('cm')),
              validator: (value) {
                if (value!.isEmpty ||
                    double.parse(value).runtimeType != double) {
                  return '1';
                }
                return null;
              },
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    final body = Body(
                        foreArm: double.parse(_lowerArmController!.text),
                        upperArm: double.parse(_upperArmController!.text));
                    widget.callback(body);

                    ScaffoldMessenger.of(context)
                        .showSnackBar(const SnackBar(content: const Text('Updated')));
                  }
                },
                child: const Text('Submit'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
