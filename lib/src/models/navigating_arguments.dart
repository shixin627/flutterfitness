import 'dart:io';

class NavigatingArguments {
  final int label;
  final String message;
  final List<File> files;

  NavigatingArguments({required this.label, required this.message, required this.files});
}
