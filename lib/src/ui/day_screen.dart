import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '/src/data/constant.dart';
import '../models/ExerciseData_model.dart';
import '../models/route_paths.dart';
import 'session_screen.dart';
import '../services/navigation_service.dart';
import '../widgets/material.dart';
import '../widgets/theme.dart';

import '../service_locator.dart';

class TheDayScreen extends StatefulWidget {
  final String theDate;
  final List<DataSet> dataSetList;
  TheDayScreen(this.theDate, this.dataSetList);

  @override
  State<StatefulWidget> createState() {
    return TheDayScreenState(this.theDate, this.dataSetList);
  }
}

class TheDayScreenState extends State<TheDayScreen> {
  final String theDate;
  final List<DataSet> dataSetList;
  TheDayScreenState(this.theDate, this.dataSetList);

  navigateToNextActivity(BuildContext context, DataSet dataSet) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => SessionScreen(dataSet: dataSet)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        title: Text(
          theDate,
          style: const TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w700,
            fontSize: 30.0,
          ),
        ),
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => locator<NavigationService>().goBack(),
        ),
      ),
      body: ScreenBackground(
        child: Padding(
          padding: const EdgeInsets.all(30.0),
          child: ListView(
            children: dataSetList
                .map((dataSet) => Column(
                      children: <Widget>[
                        InkWell(
                          child: Row(
                            children: <Widget>[
                              CircleAvatar(
                                radius: 40.0,
                                backgroundColor: Colors.transparent,
                                child: Image.asset(
                                  ImageAssets[ITEM_NAME[dataSet.itemID]] ??
                                      'assets/images/free-weight/biceps_curl.png',
                                  fit: BoxFit.contain,
                                ),
                              ),
                              const SizedBox(width: 20),
                              Column(
                                children: <Widget>[
                                  Text(
                                    ITEM_NAME[dataSet.itemID],
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ),
                                  const SizedBox(),
                                  Text(
                                    dataSet.datetime,
                                    style: TextStyle(color: colorGrey),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          onTap: () => locator<NavigationService>().navigateTo(
                              SessionRoute,
                              arguments: SessionScreen(dataSet: dataSet)),
                        ),
                        const Divider(color: Colors.black),
                      ],
                    ))
                .toList(),
          ),
        ),
      ),
    );
  }
}
