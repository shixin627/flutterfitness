import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'ExerciseData_model.dart';

///用於展示某項運動的歷史資料
class HistoryData {
  List<TimeSeriesWork> workList = [];
  List<TimeSeriesWork> durationList = [];
  List<TimeSeriesWork> avePowerList = [];
  List<TimeSeriesWork> trainList = [];
  List<TimeSeriesWork> maxEnduList = [];
  List<TimeSeriesWork> maxPowerList = [];

  String index = '總做功量';
  String baseIndex = 'on_Item';

  Map<String, List<TimeSeriesWork>> get maptoLine {
    return {
      '總做功量': workList,
      '總運動期間': durationList,
      '平均功率': avePowerList,
      '總訓練量': trainList,
      '肌耐力': maxEnduList, //肌耐力是指可以一次連續做的總功
      '爆發力': maxPowerList, //爆發力是指單次反覆的功率
    };
  }

  int get lenght => workList.length;

  static final List<String> dateDropdownItems = [
    'Last 7 days',
    'Last month',
    'Last year'
  ];

  initial(String _based) {
    if (_based == 'on_Item') {
      maxEnduList = <TimeSeriesWork>[];
      maxPowerList = <TimeSeriesWork>[];
    }
    workList = <TimeSeriesWork>[];
    durationList = <TimeSeriesWork>[];
    avePowerList = <TimeSeriesWork>[];
    trainList = <TimeSeriesWork>[];
  }

  addNextList(DateTime dateTime, DataSet _dataSet, String _based) {
    if (_based == 'on_Item') {
      maxEnduList
          .add(TimeSeriesWork(dateTime, _dataSet.analysis.maxEnduration));
      maxPowerList.add(TimeSeriesWork(dateTime, _dataSet.analysis.maxPower));
    }
    workList.add(TimeSeriesWork(dateTime, _dataSet.analysis.totalWork));
    durationList.add(TimeSeriesWork(dateTime, _dataSet.analysis.totalDuration));
    avePowerList.add(TimeSeriesWork(dateTime, _dataSet.analysis.averagePower));
    trainList.add(TimeSeriesWork(dateTime, _dataSet.analysis.totalTraining));
  }

  updateCurrentList(DataSet _dataSet) {
    workList[lenght - 1].work += _dataSet.analysis.totalWork;
    durationList[lenght - 1].work += _dataSet.analysis.totalDuration;
    avePowerList[lenght - 1].work =
        (workList[lenght - 1].work / durationList[lenght - 1].work).floor();
    trainList[lenght - 1].work += _dataSet.analysis.totalTraining;
  }

  static const List<String> title = [
    '總作功量',
    '總持續時間',
    '平均功率',
    '總訓練量',
    '肌耐力',
    '爆發力'
  ];

  static const Map<String, String> unit = {
    '總做功量': '焦耳',
    '總運動期間': '秒',
    '平均功率': '瓦特',
    '總訓練量': '公斤',
    '肌耐力': '焦耳',
    '爆發力': '瓦特',
  };

  static const Map<String, String> axisX = {
    'on_Date': '天數',
    'on_Item': '課數',
  };
}

class TimeSeriesChart extends StatelessWidget {
  final HistoryData data;
  final bool animate;

  const TimeSeriesChart(this.data, {Key? key, this.animate = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return charts.TimeSeriesChart(
      _creatSeriesData(data),
      animate: animate,
      dateTimeFactory: const charts.LocalDateTimeFactory(),
    );
  }

  List<charts.Series<TimeSeriesWork, DateTime>> _creatSeriesData(
      HistoryData _data) {
    return [
      charts.Series<TimeSeriesWork, DateTime>(
        id: _data.index,
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (TimeSeriesWork series, _) => series.time,
        measureFn: (TimeSeriesWork series, _) => series.work,
        data: _data.maptoLine[_data.index] ?? [],
      )
    ];
  }
}

/// Sample time series data type.
class TimeSeriesWork {
  DateTime time;
  int work;

  TimeSeriesWork(this.time, this.work);
}

// class HistoryLine extends StatelessWidget {
//   final HistoryData data;
//   final bool animate;

//   HistoryLine(this.data, {this.animate});

//   @override
//   Widget build(BuildContext context) {
//     return charts.LineChart(
//       _creatChartData(this.data),
//       animate: animate,
//       defaultRenderer:
//           charts.LineRendererConfig(includeArea: true, stacked: true),
//       behaviors: [
//         charts.ChartTitle(
//           HistoryData.AxisX[data.baseIndex],
//           behaviorPosition: charts.BehaviorPosition.bottom,
//           titleOutsideJustification: charts.OutsideJustification.middleDrawArea,
//         ),
//         charts.ChartTitle(
//           HistoryData.Unit[data.index],
//           behaviorPosition: charts.BehaviorPosition.start,
//           titleOutsideJustification: charts.OutsideJustification.middleDrawArea,
//         ),
//         charts.ChartTitle(
//           data.index,
//           behaviorPosition: charts.BehaviorPosition.top,
//           titleOutsideJustification: charts.OutsideJustification.middleDrawArea,
//         ),
//       ],
//     );
//   }

//   List<charts.Series<Index, int>> _creatChartData(HistoryData _data) {
//     return [
//       charts.Series<Index, int>(
//         id: _data.index,
//         colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
//         domainFn: (Index index, _) => index.axisX,
//         measureFn: (Index index, _) => index.axisY,
//         data: _data.maptoLine[_data.index],
//       )
//     ];
//   }
// }