class Permission {
  //本地資料庫與雲端資料庫的使用權限
  bool isLocalDBenable;
  bool isCloudDBenable;

  Permission({required this.isCloudDBenable, required this.isLocalDBenable});

  Map<String, dynamic> toJson() {
    return {
      'CloudDB': isCloudDBenable,
      'LocalDB': isLocalDBenable,
    };
  }

  factory Permission.fromJson(Map<String, dynamic> json) {
    return Permission(
      isCloudDBenable: json['CloudDB'] as bool,
      isLocalDBenable: json['LocalDB'] as bool,
    );
  }
}
