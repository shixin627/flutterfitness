import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import '/src/data/constant.dart';
import '/src/helpers/function.dart';
import '../data/global.dart';
import '../models/navigating_arguments.dart';
import '../models/math.dart';
import '../helpers/data_manager.dart';

class FileCollectionScreen extends StatefulWidget {
  const FileCollectionScreen({Key? key}) : super(key: key);

  @override
  _FileCollectionScreenState createState() => _FileCollectionScreenState();
}

class _FileCollectionScreenState extends State<FileCollectionScreen>
    with AutomaticKeepAliveClientMixin {
  // static double previousHeight = -2;
  @override
  bool get wantKeepAlive => true;

  List<Arm> queries = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            height: 280,
            padding: const EdgeInsets.all(5.0),
            child: ListView(
              children: List.generate(DUMBBELL_ITEMs.length, (i) {
                var title = DUMBBELL_ITEMs[i].title;
                return ListTile(
                  title: Text(title.replaceAll('_', ' ')),
                  onTap: () {
                    DataManager.getFilesFromDir(title, userId).then((files) {
                      if (files.isNotEmpty) {
                        Navigator.pushNamed(context, '/files',
                            arguments: NavigatingArguments(
                                label: i, message: "data", files: files));
                      } else {
                        showMessage(context, 'No any file.');
                      }
                    });
                  },
                );
              }),
            ),
          ),
          /*
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              height: 200,
              child: (queries != null && queries.length != 0)
                  ? ListView.builder(
                      itemCount: queries.length,
                      itemBuilder: (context, i) {
                        var data = queries[i];
                        return ListTile(
                          title: Text(
                              '${data.systemTime - queries[0].systemTime}'),
                          subtitle: Text(data.toString()),
                          trailing: IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () {
                                context
                                    .read<DataBaseSQLite>()
                                    .deleteData(
                                        context
                                            .read<DataMonitor>()
                                            .freeWeightItem,
                                        data.systemTime)
                                    .then((_) {
                                  setState(() {
                                    queries.remove(data);
                                  });
                                });
                              }),
                        );
                      })
                  : Center(
                      child: Text(
                        'No data',
                        style: TextStyle(fontSize: 30),
                      ),
                    ),
            ),
          ),
          Row(
            children: [
              Expanded(
                child: TextButton(
                  onPressed: () {
                    context.read<DataBaseSQLite>().deleteTable(
                        context.read<DataMonitor>().freeWeightItem);
                  },
                  child: Text('Delete'),
                ),
              ),
              Expanded(
                child: TextButton(
                  onPressed: () async {
                    queries = await context.read<DataBaseSQLite>().queryDataset(
                        context.read<DataMonitor>().freeWeightItem);
                    setState(() {});
                  },
                  child: Text('Query'),
                ),
              ),
              Expanded(
                child: ElevatedButton.icon(
                  onPressed: () {
                    if (queries != null) {
                      final item = context.read<DataMonitor>().freeWeightItem;
                      final timeChip = context.read<DataMonitor>().timeChip;
                      final timePhone = context.read<DataMonitor>().timePhone;
                      List<String> lines = List.generate(
                          queries.length, (index) => queries[index].toPacket());
                      DataManager.writeToFile(timeChip, timePhone, lines, item,
                              Global.user.uid, '${DateTime.now()}.csv')
                          .then((file) {
                        if (file != null) {
                          context
                              .read<DataBaseSQLite>()
                              .deleteTable(item)
                              .then((_) => Navigator.of(context).pop());
                        } else {
                          Navigator.of(context).pop();
                        }
                      });
                    }
                  },
                  icon: Icon(Icons.save_alt),
                  label: Text('Save'),
                ),
              ),
            ],
          ),*/
        ],
      ),
    );
  }

  //inspired here: https://www.366service.com/zh-tw/qa/7b02b6165bfab784cc2568d4620a4a81
}
