import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/user_model.dart';
import '../models/route_paths.dart' as routes;
import '../services/localstorage_service.dart';
import '../services/navigation_service.dart';
import '../widgets/theme.dart';
import '../service_locator.dart';

class LoginPage extends StatefulWidget {
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  UserModel? _user;

  // For CircularProgressIndicator.
  bool visible = false;

  // Getting value from TextField widget.
  TextEditingController? emailController;
  TextEditingController? passwordController;

  Future userLogin() async {
    // Showing CircularProgressIndicator.
    setState(() {
      visible = true;
    });

    // Getting value from Controller
    String email = emailController!.text;
    String password = passwordController!.text;

    // SERVER LOGIN API URL
    var apiURL = 'http://120.105.133.147/project/filter/login_user.php';

    // Store all data with Param Name.
    var data = {'email': email, 'password': password};

    // Starting Web API Call.
    var response = await http.post(Uri.parse(apiURL), body: json.encode(data));

    // Getting Server response into variable.
    var message = jsonDecode(response.body);
    print(message);
    // If the Response Message is Matched.
    if (message != 'Invalid Username or Password Please Try Again') {
      // Hiding the CircularProgressIndicator.
      setState(() {
        visible = false;
      });

      UserID _userId = UserID.fromJson(message);

      _user!.id = _userId.key;
      _user!.email = email;

      locator<LocalStorageService>().user = _user; //儲存使用者ID,EMAIL和PASSWORD

      // Navigate to Profile Screen & Sending Email to Next Screen.
      locator<NavigationService>().navigatorKey.currentState!.pushNamedAndRemoveUntil(
          routes.DBManagement, (Route<dynamic> route) => false);
    } else {
      // If Email or Password did not Matched.
      // Hiding the CircularProgressIndicator.
      setState(() {
        visible = false;
      });

      // Showing Alert Dialog with Response JSON Message.
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(message),
            actions: <Widget>[
              TextButton(
                child: const Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _user = locator<LocalStorageService>().user ??
        UserModel(id: 0, email: 'null', name: 'null');
    emailController = TextEditingController();
    passwordController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    final emailField = TextField(
      obscureText: false,
      style: textStyle,
      controller: emailController,
      autocorrect: true,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Email",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    );
    final passwordField = TextField(
      obscureText: true,
      style: textStyle,
      controller: passwordController,
      autocorrect: true,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Password",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    );
    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: const Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: userLogin,
        child: Text(
          "Login",
          textAlign: TextAlign.center,
          style: textStyle.copyWith(
              color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
    );

    final registerButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: const Color(0xff0211C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () => locator<NavigationService>().navigateTo(routes.RegisterRoute),
        child: Text(
          "Register",
          textAlign: TextAlign.center,
          style: textStyle.copyWith(
              color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
    );

    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 155.0,
                    child: Image.asset(
                      'assets/images/logo.jpg',
                      fit: BoxFit.contain,
                    ),
                  ),
                  const SizedBox(height: 45.0),
                  emailField,
                  const SizedBox(height: 25.0),
                  passwordField,
                  const SizedBox(
                    height: 35.0,
                  ),
                  loginButon,
                  const SizedBox(
                    height: 15.0,
                  ),
                  const Divider(),
                  Visibility(
                    visible: visible,
                    child: Container(
                      margin: const EdgeInsets.only(bottom: 30),
                      child: const CircularProgressIndicator(
                        backgroundColor: Colors.redAccent,
                      ),
                    ),
                  ),
                  registerButon,
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ProfileScreen extends StatelessWidget {
// Creating String Var to Hold sent Email.
  final String email;
// Receiving Email using Constructor.
  const ProfileScreen({Key? key, required this.email}) : super(key: key);

// User Logout Function.
  logout(BuildContext context) {
    locator<LocalStorageService>().user = UserModel(id: 0, email: '');
    Navigator.of(context).pop(true);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
            title: const Text('Profile Screen'), automaticallyImplyLeading: false),
        body: Center(
          child: Column(
            children: <Widget>[
              Container(
                  width: 280,
                  padding: const EdgeInsets.all(10.0),
                  child: Text('Email = \n' + '' + email,
                      style: const TextStyle(fontSize: 20))),
              ElevatedButton(
                onPressed: () {
                  logout(context);
                },
                child: const Text('Click Here To Logout'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
