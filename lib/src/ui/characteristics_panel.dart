import 'package:flutter/material.dart';
import '/src/service_locator.dart';
import 'package:provider/provider.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../providers/artificial_intelligent.dart';
import '../providers/bluetooth_notifier.dart';
import '../providers/data_monitor.dart';

class CharacteristicPanel extends StatefulWidget {
  CharacteristicPanel({Key? key}) : super(key: key);

  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<CharacteristicPanel>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      padding: const EdgeInsets.all(4.0),
      child: StaggeredGrid.count(
          crossAxisCount: 10,
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
          // staggeredTiles: [
          //   StaggeredTile.extent(2, 230.0),
          //   StaggeredTile.extent(2, 100.0),
          //   StaggeredTile.extent(2, 200.0),
          // ],
          children: [
            StaggeredGridTile.count(
              crossAxisCellCount: 4,
              mainAxisCellCount: 3,
              child: Card(
                elevation: 10,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ListTile(
                      title: const Text("ML output vector"),
                      subtitle: Selector<ArtificialIntelligent, List<double>>(
                        selector: (_, prov) => prov.output,
                        builder: (context, output, child) => Text(
                          output.isNotEmpty
                              ? output
                                  .map((e) => e.toStringAsFixed(1))
                                  .toString()
                              : "No output.",
                        ),
                      ),
                    ),
                    ListTile(
                      title: const Text("姿態標籤(機率>50%)"),
                      subtitle: Selector<ArtificialIntelligent, int>(
                        selector: (_, prov) => prov.result.index,
                        builder: (context, index, child) => Text(
                          Labels[index] ?? 'Non Exercise',
                        ),
                      ),
                    ),
                    ListTile(
                      title: const Text("Label count"),
                      subtitle: Selector<ArtificialIntelligent, Result>(
                        selector: (_, prov) => prov.result,
                        builder: (context, result, child) => Text(
                          Result.counter.toString(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 2,
              mainAxisCellCount: 3,
              child: Card(
                  elevation: 10,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Selector<BTNotifier, BluetoothCharacteristic?>(
                        selector: (_, notifier) => notifier.hrmCharacteristic,
                        builder: (_, c, __) {
                          if (c == null) {
                            return ListTile(
                                leading: const Icon(Icons.favorite_border),
                                title: const Text("心率"),
                                subtitle: Text(c.toString()));
                          } else {
                            return Selector<BTNotifier, bool>(
                                selector: (_, notifier) =>
                                    notifier.hrmCharacteristic!.isNotifying,
                                builder: (_, isNotifying, __) {
                                  if (isNotifying) {
                                    return ListTile(
                                      leading: const Icon(
                                        Icons.favorite,
                                        color: Colors.red,
                                      ),
                                      title: Selector<DataMonitor, int>(
                                        selector: (_, provider) =>
                                            provider.heartrate,
                                        builder: (context, value, child) {
                                          if (value != 0) {
                                            return Text(
                                              '$value bpm',
                                              style:
                                                  DefaultTextStyle.of(context)
                                                      .style
                                                      .copyWith(
                                                        fontSize: 24.0,
                                                      ),
                                            );
                                          } else {
                                            return const LinearProgressIndicator();
                                          }
                                        },
                                      ),
                                      onTap: () {
                                        debugPrint("取消訂閱心率");
                                        locator<BTNotifier>()
                                            .unsubscribe("HRM");
                                      },
                                    );
                                  } else {
                                    return ListTile(
                                        leading: const Icon(
                                          Icons.favorite_border,
                                          color: Colors.red,
                                        ),
                                        title: const Text("訂閱心率"),
                                        onTap: () {
                                          debugPrint("訂閱心率");
                                          locator<BTNotifier>()
                                              .listenCharacteristic("HRM");
                                        });
                                  }
                                });
                          }
                        },
                      ),
                    ],
                  )),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 3,
              mainAxisCellCount: 3,
              child: Card(
                elevation: 10,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Selector<BTNotifier, BluetoothCharacteristic?>(
                        selector: (_, notifier) => notifier.rtcCharacteristic,
                        builder: (_, c, __) {
                          if (c == null) {
                            return ListTile(
                                leading: const Icon(Icons.timer),
                                title: const Text('RTC時鐘'),
                                subtitle: Text(c.toString()));
                          } else {
                            return Column(
                              children: [
                                ListTile(
                                  leading: const Icon(
                                    Icons.settings,
                                    color: Colors.blue,
                                  ),
                                  title: const Text('設定RTC時間'),
                                  onTap: () =>
                                      locator<BTNotifier>().sendRealTimeData(),
                                ),
                                ListTile(
                                  leading: const Icon(
                                    Icons.access_time,
                                    color: Colors.lightBlue,
                                  ),
                                  title: const Text('讀取RTC時間'),
                                  onTap: () => locator<BTNotifier>()
                                      .writeCommand([93]), // write command 'S'
                                ),
                              ],
                            );
                          }
                        }),
                    ListTile(
                      title: const Text('RTC Time'),
                      subtitle: Selector<DataMonitor, DateTime?>(
                          selector: (_, provider) => provider.rtcTime,
                          builder: (context, result, child) {
                            if (result != null) {
                              return Text('$result');
                            } else {
                              return const Text('Waiting');
                            }
                          }),
                    ),
                  ],
                ),
              ),
            ),
          ]),
    );
  }
}
