class TheSet {
  int weight;
  int repetitions;
  int trainingVolume;
  int joule;
  int duration;
  int maxPower;

  TheSet(
      {required this.weight,
      required this.repetitions,
      required this.joule,
      required this.duration,
      required this.trainingVolume,
      required this.maxPower});

  // 平均功率
  int get powerAve => (joule / duration).round();

  Map<String, dynamic> toJson() {
    return {
      'weight': weight,
      'repeat': repetitions,
      'amount': trainingVolume,
      'joule': joule,
      'duration': duration,
      'power': maxPower
    };
  }

  factory TheSet.fromJson(Map<String, dynamic> json) {
    return TheSet(
      weight: json['weight'] as int,
      repetitions: json['repeat'] as int,
      joule: json['joule'] as int,
      duration: json['duration'] as int,
      trainingVolume: json['amount'] as int,
      maxPower: json['power'] as int,
    );
  }

  @override
  String toString() {
    return '{ ${this.weight}, ${this.repetitions}, ${this.trainingVolume}, ${this.joule}, ${this.duration}, ${this.maxPower} }';
  }
}

class InterRest {
  int interRest;
  InterRest({required this.interRest});

  factory InterRest.fromJson(Map<String, dynamic> json) {
    return InterRest(
      interRest: json['InterRest'] as int,
    );
  }
}
