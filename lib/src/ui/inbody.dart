import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import '../models/inbody_model.dart';
import '../models/date_model.dart';
import '../services/cloud_service.dart';
import '../widgets/theme.dart';

class UploadInBody extends StatefulWidget {
  final int userID;
  const UploadInBody(this.userID, {Key? key}) : super(key: key);
  @override
  UploadInBodyState createState() => UploadInBodyState(userID);
}

class UploadInBodyState extends State<UploadInBody> {
  final int userID;
  UploadInBodyState(this.userID);
  String? _timeString;
  final weightController = TextEditingController();
  final skeletalMController = TextEditingController();
  final fatController = TextEditingController();
  final scoreController = TextEditingController();

  Future userBodyComposition() async {
    String weight = weightController.text;
    String skeletalM = skeletalMController.text;
    String fat = fatController.text;
    String score = scoreController.text;

    BodyQuality bodyQuality = BodyQuality(
        weight: double.parse(weight),
        smm: double.parse(skeletalM),
        fat: double.parse(fat),
        score: int.parse(score));
    if (_timeString != null) {
      InBody inbody = InBody(0, userID, _timeString!, bodyQuality);
      CloudService.updateBody(context, inbody);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final weightField = TextFormField(
      obscureText: false,
      style: textStyle,
      controller: weightController,
      autocorrect: true,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "體重",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    );

    final skeletalMField = TextFormField(
      obscureText: false,
      style: textStyle,
      controller: skeletalMController,
      autocorrect: true,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "骨骼肌重",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    );

    final fatField = TextFormField(
      obscureText: false,
      style: textStyle,
      controller: fatController,
      autocorrect: true,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "體脂肪重",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    );

    final scoreField = TextFormField(
      obscureText: false,
      style: textStyle,
      controller: scoreController,
      autocorrect: true,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "健身評分",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    );

    final uploadButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: colorAccent,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () => userBodyComposition,
        child: Text(
          "上傳InBody數據",
          textAlign: TextAlign.center,
          style: textStyle.copyWith(
              fontSize: 21, color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: colorGrey,
        title: const Text(
          'Body Composition Analysis Record Form',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w700,
            fontSize: 30.0,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text('請點選下方欄位輸入量測時間'),
                  DateTimeField(
                    format: Date.dateFormat,
                    onShowPicker: (context, currentValue) async {
                      final date = await showDatePicker(
                          context: context,
                          firstDate: DateTime(1900),
                          initialDate: currentValue!,
                          lastDate: DateTime(2100));
                      if (date != null) {
                        final time = await showTimePicker(
                          context: context,
                          initialTime: TimeOfDay.fromDateTime(
                              currentValue),
                        );
                        var datetime = DateTimeField.combine(date, time);
                        _timeString = Date.dateFormat.format(datetime);
                        return datetime;
                      } else {
                        return currentValue;
                      }
                    },
                  ),
                  const Divider(),
                  weightField,
                  const SizedBox(height: 25.0),
                  skeletalMField,
                  const SizedBox(height: 25.0),
                  fatField,
                  const SizedBox(height: 25.0),
                  scoreField,
                  const SizedBox(height: 25.0),
                  uploadButton,
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
