import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';
import '/src/models/math.dart';
import '/src/providers/artificial_intelligent.dart';
import '/src/providers/data_monitor.dart';
import '/src/services/database.dart';
import 'package:provider/provider.dart';

const GameObjects = ['FreeCube'];
const MethodNames = ['ReceiveMessage', 'SetOrigin', 'SetLength'];

class UnityNotifier extends ChangeNotifier {
  Locator locator;
  UnityNotifier(this.locator);

  UnityWidgetController? _unityWidgetController;
  UnityWidgetController? get unityWidgetController => _unityWidgetController;
  // Callback that connects the created controller to the unity controller
  void onUnityCreated(controller) {
    _unityWidgetController = controller;
  }

  int get objectIndex => 0;

  bool _isTriggered = false;
  bool get isTriggered => _isTriggered;
  set isTriggered(bool value) {
    _isTriggered = value;
    notifyListeners();
  }

  bool _isRecording = false;
  bool get isRecording => _isRecording;
  set isRecording(bool value) {
    assert(isRecording != value);
    _isRecording = value;
    notifyListeners();
  }

  bool _inserting = false;
  bool get inserting => _inserting;
  set inserting(bool value) {
    if (inserting != value) {
      _inserting = value;
      notifyListeners();
    }
  }

  double _sliderSpeed = 0.0;
  double get sliderSpeed => _sliderSpeed;

  double _sliderAngle = 0.0;
  double get sliderAngle => _sliderAngle;

  bool _isMoving = false;
  bool get isMoving => _isMoving;
  set isMoving(bool value) {
    if (value != _isMoving) {
      _isMoving = value;
      notifyListeners();
    }
  }

  int _stageCount = 0;
  int get stageCount => _stageCount;

  List<Arm> _leftArmList = [];
  List<Arm> get leftArmList => _leftArmList;

  bool _taskRunning = false;
  bool get taskRunning => _taskRunning;

  List<double> _buffer = [];
  List<double> get buffer => _buffer;
  set buffer(List<double> data) {
    _buffer = data;
    notifyListeners();
  }

  void setBuffer(List<double> value) {
    if (_buffer.length != LENGTH_OF_BUFFER) {
      _buffer.addAll(value);
    } else {
      for (int i = 1; i < LENGTH_OF_BUFFER; i++) {
        _buffer[i - 1] = _buffer[i + 5];
      }
      for (int i = 0; i < 6; i++) {
        _buffer[LENGTH_OF_BUFFER - 6 + i] = value[i];
      }
    }
  }

  // Transmit wrist and upper arm quaternion dataset of six continuous samples to Unity app.
  void postMessageToModel(String message) {
    _unityWidgetController!.postMessage(
      GameObjects[objectIndex],
      MethodNames[0],
      message,
    );
  }

  /// 資料分流程序 Create a new task.
  Future<void> createNewTask(List<Arm> samples) async {
    if (!taskRunning) {
      _taskRunning = true;
      _leftArmList.clear();
      _leftArmList = samples;

      String quaternions = "";
      for (int i = 0; i < 6; i++) {
        // 將四元數資料打包，準備送進Unity
        quaternions += samples[i].quaternions;
        if (i != 5) quaternions += ",";

        // 姿態辨識程序(將6筆樣本分批輸入)
        final sixAxisData = samples[i].sixAxisDataset;
        locator<ArtificialIntelligent>().input(sixAxisData);
      }

      // 姿態估測程序(一次輸入6筆樣本)
      postMessageToModel(quaternions);
      debugPrint("New Task created.");
    } else {
      debugPrint("Task running...");
    }
  }

  //
  void _dealWithTask(message) {
    // [index, ux, uy, uz, lx, ly, lz]
    final decoded = message.toString().split(",");
    final index = int.parse(decoded[0]);

    double verticalComponentOfUnitUpperArm = double.parse(decoded[2]);
    double verticalComponentOfUnitLowerArm = double.parse(decoded[5]);

    locator<ArtificialIntelligent>().internalState!.updateWith(
        verticalComponentOfUnitUpperArm, verticalComponentOfUnitLowerArm);

    final frameString = message.toString().substring(2);
    final leftArm = leftArmList[index]..frame = frameString;

    //若記錄按鈕已被按下
    if (isRecording) {
      final item = locator<DataMonitor>().freeWeightItem;
      locator<DataBaseSQLite>().insertData(item, leftArm.toMap()).then((res) {
        if (res == false) {
          isRecording = false; // 停止紀錄
        }
      });
    }
    if (index == 5) {
      // int difference = DateTime.now().difference(startTime).inMilliseconds;
      // print("difference: " + difference.toString() + " ms");
      _taskRunning = false;
    }
    // locator<ArtificialIntelligent>().input(leftArm);
  }

  void onUnityMessage(message) {
    if (taskRunning) {
      _dealWithTask(message);
      // _executeTask(message);
    }
  }

  void onUnitySceneLoaded(SceneLoaded? sceneInfo) {
    debugPrint('場景名稱: ${sceneInfo!.name}');
    debugPrint('場景編號: ${sceneInfo.buildIndex}');
  }

  // 將當前使用者抬手向前之方位校正為Unity中角色的正前方
  void resetOrigin(String message) async {
    _unityWidgetController!.postMessage(
      GameObjects[objectIndex],
      MethodNames[1],
      message,
    );
  }

  // 設定手臂長度(unit: cm)
  void setLengthOfArm(double upper, double lower) {
    _unityWidgetController!.postMessage(
      GameObjects[objectIndex],
      MethodNames[2],
      '$upper,$lower',
    );
  }
}
