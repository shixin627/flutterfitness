import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '/src/data/constant.dart';
import '/src/data/global.dart';
import '/src/helpers/data_manager.dart';
import '/src/helpers/function.dart';
import '/src/models/navigating_arguments.dart';
import '../models/ExerciseData_model.dart';
import '../models/history_line.dart';
import '../models/permission.dart';
import '../models/date_model.dart';
import '../models/route_paths.dart' as routes;
import 'day_screen.dart';
import 'item_screen.dart';
import '../services/local_database_service.dart';
import '../services/localstorage_service.dart';
import '../services/navigation_service.dart';
import '../widgets/material.dart';
import '../widgets/theme.dart';
import '../widgets/itemCard.dart';
import '../service_locator.dart';

class HistoryStatistic extends StatefulWidget {
  const HistoryStatistic({Key? key}) : super(key: key);

  StatisticState createState() => StatisticState();
}

class StatisticState extends State<HistoryStatistic> {
  final HistoryData _history = HistoryData();
  String? _pickedDate;
  List<DataSet> _pickedList = [];
  List<String> _dateList = [];
  int _actualIndex = 0;
  Permission? _permission;
  bool? _isEnable;

  void _changedLocal(bool isCheck) {
    _isEnable = isCheck;
    _permission!.isLocalDBenable = isCheck;
    locator<LocalStorageService>().permission = _permission;
    setState(() {});
  }

  final List<String> _lineDropdownItems = [
    '總做功量',
    '總運動期間',
    '平均功率',
    '總訓練量',
  ];

  Future<void> refreshList() async {
    DataSet.list = await LocalDatabaseService.fetchSessions();
    _pickedList = DataSet.list;
    setState(() {
      _dateList = assortWithDate(_pickedList);
    });
  }

  Future _selectDate() async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2021), //set the earliest start year
      lastDate: DateTime.now(),
    );
    if (picked != null) {
      _pickedDate = Date.ymdFormat.format(picked);
      List<DataSet> _list = <DataSet>[];
      for (var dataset in DataSet.list) {
        var _date = dataset.datetime.split(" "); //將日期字串提出並與選擇的日期比較，將當天每項運動加入清單。
        if (_date[0] == _pickedDate) {
          _list.add(dataset);
        }
      }
      _pickedList = _list;
      setState(() {
        _dateList = assortWithDate(_pickedList);
      });
    }
  }

  //創造一個為運動日的日期列表
  List<String> assortWithDate(List<DataSet> _list) {
    List<String> _stringList = <String>[];
    String _currentDateStr = 'current';
    String _previousDateStr = 'previous';

    _history.initial('on_date');

    for (var dataset in _list) {
      _currentDateStr = dataset.datetime.split(" ")[0];
      if (_currentDateStr != _previousDateStr) {
        _stringList.add(_currentDateStr);
        _previousDateStr = _currentDateStr;
        _history.addNextList(Date.ymdFormat.parse(_currentDateStr), dataset,
            _history.baseIndex); //加入至歷史曲線圖中的一天
      } else {
        _history.updateCurrentList(dataset); //若是同一天則整合當天的運動數據
      }
    }
    return _stringList;
  }

  // 移動到所選擇日期的運動列表頁面
  navigateToTheDate(BuildContext context, String _date) {
    List<DataSet> _dayList = <DataSet>[];
    for (var dataset in _pickedList) {
      if (_date == dataset.datetime.split(" ")[0]) {
        //同一天
        _dayList.add(dataset);
      }
    }
    locator<NavigationService>()
        .navigateTo(routes.DayRoute, arguments: TheDayScreen(_date, _dayList));
  }

  // 進入到某項運動的歷史數據頁面
  navigateToItem(BuildContext context, int _index) {
    List<DataSet> _itemList = <DataSet>[];
    for (var _dataset in _pickedList) {
      if (_index == _dataset.itemID) {
        _itemList.add(_dataset);
      }
    }
    locator<NavigationService>().navigateTo(routes.ItemRoute,
        arguments: TheItemScreen(_index, _itemList));
  }

  Widget _itemDirectories() {
    return Container(
      height: 280.0,
      width: 300.0,
      padding: const EdgeInsets.all(5.0),
      child: ListView(
        children: List.generate(DUMBBELL_ITEMs.length, (i) {
          var title = DUMBBELL_ITEMs[i].title;
          return ListTile(
            title: Text(title.replaceAll('_', ' ')),
            onTap: () {
              DataManager.getFilesFromDir(title, userId).then((files) {
                if (files.length != 0) {
                  Navigator.pushNamed(context, '/files',
                      arguments: NavigatingArguments(
                          label: i, message: "data", files: files));
                } else {
                  showMessage(context, 'No any file.');
                }
              });
            },
          );
        }),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _permission = locator<LocalStorageService>().permission ??
        Permission(isCloudDBenable: true, isLocalDBenable: true);
    _isEnable = _permission!.isLocalDBenable;

    _history.index = _lineDropdownItems[_actualIndex];
    refreshList();
  }

  @override
  Widget build(BuildContext context) {
    if ((_pickedList.isNotEmpty)) {
      return const Center(child: CircularProgressIndicator());
    } else {
      return Visibility(
        visible: _isEnable!,
        child: ScreenBackground(
          child: RefreshIndicator(
            child: Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 30.0),
              child: StaggeredGrid.count(
                crossAxisCount: 2,
                crossAxisSpacing: 12.0,
                mainAxisSpacing: 12.0,
                children: <Widget>[
                  StaggeredGridTile.count(
                    crossAxisCellCount: 2,
                    mainAxisCellCount: 1,
                    child: DashboardElement(
                      child: Center(
                        child: Container(
                          child: Row(
                            children: [
                              const SizedBox(width: 15),
                              const Text(
                                "History",
                                style: TextStyle(
                                  fontSize: 36.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              IconButton(
                                icon: const Icon(Icons.all_inclusive),
                                onPressed: () => setState(() {
                                  _pickedList = DataSet.list;
                                  _dateList = assortWithDate(_pickedList);
                                }),
                                color: colorPrimary,
                              ),
                              IconButton(
                                icon: const Icon(Icons.date_range),
                                onPressed: _selectDate,
                                color: colorPrimary,
                              ),
                              IconButton(
                                icon: const Icon(Icons.file_copy_outlined),
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: const Text('Item List'),
                                          content: _itemDirectories(),
                                        );
                                      });
                                },
                                color: colorPrimary,
                              ),
                              // CheckboxListTile(
                              //   value: _isEnable,
                              //   onChanged: _changedLocal,
                              //   title: Text('本地資料庫'),
                              //   controlAffinity:
                              //       ListTileControlAffinity.leading,
                              //   subtitle: Text('結束運動時自動儲存運動數據於手機記憶體'),
                              //   secondary: Icon(Icons.archive),
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  StaggeredGridTile.count(
                    crossAxisCellCount: 1,
                    mainAxisCellCount: 2,
                    child: DashboardElement(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListView(
                          children: _dateList
                              .map(
                                (data) => Column(
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () {
                                        navigateToTheDate(context, data);
                                      },
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                20, 5, 0, 5),
                                            child: Text(data,
                                                style: const TextStyle(
                                                    fontSize: 21),
                                                textAlign: TextAlign.left),
                                          )
                                        ],
                                      ),
                                    ),
                                    const Divider(color: Colors.black),
                                  ],
                                ),
                              )
                              .toList(),
                        ),
                      ),
                    ),
                  ),
                  StaggeredGridTile.count(
                    crossAxisCellCount: 1,
                    mainAxisCellCount: 2,
                    child: DashboardElement(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                          child: Container(
                            height: 300,
                            child: Swiper(
                              itemCount: itemLen,
                              loop: false,
                              viewportFraction: 0.9,
                              scale: 0.8,
                              outer: true,
                              // pagination: SwiperPagination(
                              //   alignment: Alignment.bottomCenter,
                              //   margin: EdgeInsets.all(3.0),
                              // ),
                              onTap: (index) => navigateToItem(context, index),
                              itemBuilder: (BuildContext context, int index) {
                                return Center(
                                  child: Container(
                                    height: 250,
                                    child: ItemCard(
                                      item: ITEM_NAME[index],
                                      color: colorGrey,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  StaggeredGridTile.count(
                    crossAxisCellCount: 2,
                    mainAxisCellCount: 2,
                    child: DashboardElement(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                DropdownButton(
                                  isDense: true,
                                  value: _history.index,
                                  onChanged: (value) {
                                    var txt = value as String;
                                    setState(() {
                                      _history.index = txt;
                                      _actualIndex = _lineDropdownItems
                                          .indexOf(txt); // Refresh the chart
                                    });
                                  },
                                  items: _lineDropdownItems.map((String title) {
                                    return DropdownMenuItem(
                                      value: title,
                                      child: Text(title,
                                          style: const TextStyle(
                                              color: Colors.blue,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14.0)),
                                    );
                                  }).toList(),
                                ),
                              ],
                            ),
                            const Padding(
                                padding: EdgeInsets.only(bottom: 20.0)),
                            Expanded(
                              child: TimeSeriesChart(_history),
                            ),
                          ],
                        ),
                      ),
                      onTap: () =>
                          print(_history.maptoLine[_actualIndex].toString()),
                    ),
                  ),
                ],
                // staggeredTiles: [
                //   StaggeredTile.extent(2, 80.0),
                //   StaggeredTile.extent(1, 250.0),
                //   StaggeredTile.extent(1, 250.0),
                //   StaggeredTile.extent(2, 300.0),
                // ],
              ),
            ),
            onRefresh: refreshList,
          ),
        ),
      );
    }
  }
}
