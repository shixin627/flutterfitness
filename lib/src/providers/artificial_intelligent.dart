import 'package:flutter/cupertino.dart';
import 'package:tflite_flutter/tflite_flutter.dart';
import '../models/internel_state.dart';

const Map<int, String> Labels = {
  0: 'Non Exercise',
  1: 'Biceps Curl',
  2: 'Lateral Raise',
  3: 'Shoulder Press',
};

const int TIME_STEPS = 128;
const int STEP = 32;
final int numOfLabels = Labels.length;

class Result {
  int index;
  double score;
  Result({this.index = 0, this.score = 0.0});

  static List<int> counter = [0, 0, 0, 0];

  void count() => counter[index]++;

  // String get item =>
  //     Labels[counter.getRange(counter.length - 5, counter.length).reduce(max)];
}

class ArtificialIntelligent extends ChangeNotifier {
  final _modelFile = 'models/gesture_model.tflite';

  // TensorFlow Lite Interpreter object
  Interpreter? _interpreter;

  ArtificialIntelligent() {
    // Load model when the classifier is initialized.
    _loadModel();
  }

  void _loadModel() async {
    debugPrint('Interpreter is loading...');
    // Creating the interpreter using Interpreter.fromAsset
    _interpreter = await Interpreter.fromAsset(_modelFile);
    debugPrint('Interpreter loaded successfully');
  }

  List<List<double>> _buffer = [];
  List<List<List<double>>> inputData = []..length = 1;
  // List<List<double>> _stepBuffer = List<List<double>>();

  List<double> _output = [];
  List<double> get output => _output;
  set output(List<double> res) {
    _output = res;
    notifyListeners();
  }

  Result _result = Result(index: 0);
  Result get result => _result;
  set result(Result res) {
    _result = res;
    notifyListeners();
  }

  InternalState? _internalState = InternalState(initLabel: 0);
  InternalState? get internalState => _internalState;
  set internalState(InternalState? newState) {
    assert(newState != _internalState);
    _internalState = newState;
    notifyListeners();
  }

  Future<InternalState?> detectPeaks(int label, List<double> data) async {
    if (label != 0) {
      _internalState = InternalState(initLabel: label);
      for (int i = 0; i < data.length; i++) {
        internalState!.processState(data[i]);
      }
      internalState!.detectValley();
      return internalState;
    }
    return null;
  }

  Future<bool> isTimeWindowFilled(List<double> sixAxisData) async {
    if (_buffer.length == TIME_STEPS) {
      // 蒐集完128個樣本
      ///generate a new List Object instead of getting value form [_buffer].
      inputData[0] = List.from(_buffer);
      _buffer.removeRange(0, STEP);
      return true;
    } else {
      _buffer.add(sixAxisData);
      // print(_buffer.length);
      // print(inputData[0].length);
      return false;
    }
  }

  Future<List<double>> classify(List<List<List<double>>> input) async {
    // Input shape: [TIME_STEPS, 6]
    // Output shape: [1,4]
    // if (input.length == 128) {
    debugPrint('start classification with input length: ' +
        input[0].length.toString());
    final output =
        List<double>.filled(numOfLabels, 0.0).reshape([1, numOfLabels]);
    _interpreter!.run(input, output);
    debugPrint('classification finished: $output');
    return [output[0][0], output[0][1], output[0][2], output[0][3]];
    // }
    // return null;
  }

  int continueCount = 0; //機器學習模型連續輸出同一個標籤的次數
  int prevLabel = 0; //上一個認定的標籤

  // 決定標籤：根據當前ML模型的辨識結果 與 連續累積相同該結果的次數，推測當前使用者的運動項目(包括不運動狀態)，以整數型別的標籤輸出
  Future<int?> decideLabel(int outputIndex) async {
    if (outputIndex == prevLabel) {
      if (continueCount < 4) {
        continueCount++;
      } else if (continueCount == 4) {
        // 連續4次為同一個項目就判斷該姿態標籤
        continueCount++;
        return outputIndex;
      }
    } else {
      continueCount = 0;
      prevLabel = outputIndex;
    }
    return null;
  }

  /// 將[sixAxisData]樣本輸入姿態辨識模型
  Future<void> input(List<double> sixAxisData) async {
    isTimeWindowFilled(sixAxisData).then((success) {
      if (success) {
        // 如果蒐集滿128個樣本，
        classify(inputData).then((vector) {
          output = vector;
          int index = output.indexWhere((element) => element >= 0.50);
          if (index >= 0) {
            decideLabel(index).then((decidedIndex) => (decidedIndex != null)
                ? internalState!.label = decidedIndex
                : null);
            result = Result(index: index, score: output[index])..count();
          }
        });
      }
    });
  }
}
