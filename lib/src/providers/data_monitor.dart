import 'package:flutter/material.dart';
import '/src/data/constant.dart';

const int LENGTH_OF_BUFFER = 512;

class DataMonitor extends ChangeNotifier {
  DateTime? _phoneTime;
  DateTime? get phoneTime => _phoneTime;
  void getPhoneTime() {
    _phoneTime = DateTime.now();
    debugPrint(phoneTime!.toIso8601String());
    notifyListeners();
  }

  DateTime? _rtcTime;
  DateTime? get rtcTime => _rtcTime;
  void setChipTime(DateTime newData) {
    debugPrint("RTC time : " + newData.toString());
    _rtcTime = newData;
    notifyListeners();
  }

  int? systemTimeStamp;

  int _heartrate = 0;
  int get heartrate => _heartrate;
  set heartrate(int value) {
    if (value != _heartrate) {
      _heartrate = value;
      notifyListeners();
    }
  }

  int _itemIndex = 1;
  set itemIndex(int i) {
    if (i != _itemIndex) {
      _itemIndex = i;
      notifyListeners();
    }
  }

  String get freeWeightItem => DUMBBELL_ITEMs[_itemIndex].title;
}
