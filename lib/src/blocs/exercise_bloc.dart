import 'dart:async';
import 'package:flutter/material.dart';

import '../models/set_model.dart';
import 'exercise_event.dart';

/// 處理運動事件的 Bloc(商業邏輯) pattern
class ExerciseBloc {
  /// "事件控制者"，型別為自訂義的抽象物件"ExerciseEvent"
  final _exerciseEventController = StreamController<ExerciseEvent>();
  // For events, exposing only a sink which is an input
  Sink<ExerciseEvent> get exerciseEventSink => _exerciseEventController.sink;
  /// 每當有新事件發生時，我們都希望將其映射到新狀態
  ExerciseBloc() {
    _exerciseEventController.stream.listen(_mapEventToState);
  }

  /// 是否分析開始事件的設定旗標
  bool isAnalysis = false;

  /// repetition"狀態控制broadcast"，型別為integer
  final _repetitionController = StreamController<int>.broadcast();
  StreamSink<int> get _inRepeat => _repetitionController.sink;
  Stream<int> get repeat => _repetitionController.stream;

  /// position"狀態控制者"，型別為String
  final _positionStateController = StreamController<double>();
  StreamSink<double> get _positionSink => _positionStateController.sink;
  Stream<double> get positionStream => _positionStateController.stream;

  List<double> instantPower = [];

  /// 每次反覆的做功量
  int workPerRepeat = 0;

  /// 每次反覆的期間
  int durationPerRepeat = 0;

  /// 每次反覆的功率
  int get powerPerRepeat => (workPerRepeat / durationPerRepeat).floor();

  /// 每組的物件
  List<TheSet> theSet = [];
  int get sets => theSet.length;
  TheSet get currentSet => theSet[sets - 1];

  /// 每組的做功量
  int workPerSet = 0;

  /// 每組的期間
  int durationPerSet = 0;

  /// 每組的功率
  int get powerPerSet => (workPerSet / durationPerSet).floor();

  /// 總做功量
  int workTotal = 0;

  /// 總期間
  int durationTotal = 0;

  /// 總功率
  int get powerTotal => (workTotal / durationTotal).floor();

  /// 各組間休息時間
  List<int> restBetweenSets = <int>[];

  /// 選擇重量
  int weightKG = 0;

  /// 最大肌力指標：1RM(repetition maximum) 的重量
  int get muscleStrength =>
      (weightKG / (1.0278 - 0.0278 * currentSet.repetitions)).floor();

  /// 肌耐力指標：一鼓作氣累積的做功量(焦耳)
  // int get muscleEndurance => currentSet.joule;

  /// 爆發力指標
  // int get musclePower => powerPerRepeat.floor();

  /// 磅轉公斤
  // double lbsToKG(int _weightLbs) {
  //   return _weightLbs * 0.45359237;
  // }

  /// 焦耳轉千卡
  // double jouletoKCal(var _data) {
  //   return _data * 0.000239;
  // }

  /// 將發生的事件轉為狀態
  void _mapEventToState(ExerciseEvent event) {
    if (event is RepeatEvent) {
      // 完成一次反覆時
      int repetitions = event.repetitions;
      if (currentSet.repetitions != repetitions) {
        // 若是反覆次數改變了才更新變數，避免因為其他setState函式而二度觸發了"RepeatEvent"
        currentSet.repetitions = repetitions;
        _inRepeat.add(repetitions); //將變化告知"狀態控制者"repeat

        workPerRepeat = (weightKG * event.accVsLengthIntergration).round();
        durationPerRepeat = event.duration;

        // 更新當組的總做功量
        workPerSet += workPerRepeat;
        currentSet.joule += workPerRepeat;
        // 更新當組的總時間
        durationPerSet += durationPerRepeat;
        currentSet.duration += durationPerRepeat;

        currentSet.trainingVolume += weightKG;
        if (powerPerRepeat > currentSet.maxPower) {
          currentSet.maxPower = powerPerRepeat;
        }

        workTotal += workPerRepeat;
        durationTotal += durationPerRepeat;
      }
    } else if (event is RestEvent) {
      //休息
    } else if (event is StartEvent) {
      //開始新的一組
      if (isAnalysis) {
        //旗標判斷
      } else {
        //當讀取到不同組數時，代表新的一組運動正式開始了
        if (sets != event.sets) {
          //紀錄當前儀表板所選擇的重量，在下次新的一組運動開始前都不能再更改
          weightKG = event.weight;
          if (event.restBetweenSets != null) {
            restBetweenSets.add(event.restBetweenSets!); //更新上一組和本組間的休息時間
            debugPrint('組間休息時間 $restBetweenSets 秒');
          }
          //初始化本組的各項資訊
          theSet.add(TheSet(
              weight: weightKG,
              repetitions: 0,
              joule: 0,
              duration: 0,
              trainingVolume: 0,
              maxPower: 0));

          workPerSet = 0;
          durationPerSet = 0;
        }
      }
    } else if (event is MovementEvent) {
      //運動(移動)中事件，為最常觸發的事件(在ESP32端，每當位置變化1公分時會觸發)
      double position = event.data; //位置(cm)
      _positionSink.add(position); //更新當前位置狀態
    }
  }

  //關閉Stream Controller
  void dispose() {
    _exerciseEventController.close();
    _positionStateController.close();
    _repetitionController.close();
  }
}
