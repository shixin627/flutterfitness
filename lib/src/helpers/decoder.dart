import 'dart:convert';
import '../models/math.dart';

/// 解碼器：解析來自於SERVER端的藍芽封包
class Decoder {
  static Future<List<String>> decodeUTF8(List<int> data) async {
    return utf8.decode(data).split(",");
  }

  /// 解析封包(Decipher Packet)：將UTF-8編碼解為字串，拆解字串中的逗點成為清單並檢查是否包含6筆樣本，若符合則將其轉為物件Arm的清單List<Arm>
  static Future<List<Arm>?> decipher(List<int> packet) async {
    return decodeUTF8(packet).then((strings) {
      if (strings.length == 90) {
        final samples = List.generate(6, (i) {
          return Arm.construct(int.parse(strings[i * 15]),
              strings.sublist((i * 15) + 1, (i + 1) * 15));
        });
        if (!samples.contains(null)) {
          return samples;
        }
      }
      return null;
    });
  }
}
