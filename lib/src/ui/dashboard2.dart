import 'package:flutter/material.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '/src/blocs/exercise_bloc.dart';
import '/src/data/constant.dart';
import '/src/data/global.dart';
import '/src/helpers/function.dart';
import '/src/models/internel_state.dart';
// import '/src/providers/artificial_intelligent.dart';
import '/src/providers/menu_provider.dart';
import '/src/services/localstorage_service.dart';
import '/src/widgets/CircleProgress.dart';
import '/src/widgets/material.dart';
import '/src/service_locator.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';

class DashBoardFreeWeight extends StatefulWidget {
  final int itemIndex;
  const DashBoardFreeWeight(this.itemIndex, {Key? key}) : super(key: key);

  @override
  _DashBoardFreeWeightState createState() => _DashBoardFreeWeightState();
}

class _DashBoardFreeWeightState extends State<DashBoardFreeWeight> {
  // User's information
  int _calories = 10000;
  int _muscleStrength = 1;
  late int _ratio;
  // Targets of the item
  static int targetSet = 2;
  static int targetRepi = 8;

  // dataset of position
  bool switchDataSet = false;
  List<double> baseData = <double>[0.0, 0.0];
  List<double> dataSetA = <double>[];
  List<double> dataSetB = <double>[];

  //更新資料集(位置)
  void _setNewDataSet(double position) {
    if (switchDataSet) {
      baseData = setDataSet(dataSetB, dataSetA, position);
    } else {
      baseData = setDataSet(dataSetA, dataSetB, position);
    }
    switchDataSet = !switchDataSet;
  }

  Future<void> _showDataDiaolog() async {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: const Text('提醒'),
              content: const Text('結束訓練\n本次的運動數據將會被儲存至手機\n若有開啟網路也會同時上傳至雲端'),
              actions: <Widget>[
                TextButton(
                  child: const Text('重置'),
                  onPressed: () {
                    Provider.of<MenuProvider>(context, listen: false)
                        .isDoingFreeWeight = false;
                    Provider.of<InternalState>(context, listen: false)
                        .initVar();
                    Navigator.of(context).pop(false);
                  },
                ),
                TextButton(
                  child: const Text('繼續訓練'),
                  onPressed: () => Navigator.of(context).pop(false),
                ),
                TextButton(
                  child: const Text('是'),
                  onPressed: () async {
                    if (widget.itemIndex >= 0) {
                      await Provider.of<InternalState>(context, listen: false)
                          .saveDataset(
                              localUser.id.toString(), widget.itemIndex);
                      Provider.of<MenuProvider>(context, listen: false)
                          .isDoingFreeWeight = false;
                      Navigator.of(context).pop(false);
                    }
                  },
                ),
              ],
            ));
  }

  ValueChanged<int?>? changeWeight(int? value) {
    if (value != null) {
      setState(() {
        Global.bellWeightIndex = BellWeights.indexOf(value);
        if (Global.bellWeight < _muscleStrength) {
          _ratio = (Global.bellWeight / _muscleStrength * 100).floor();
        } else {
          _ratio = 100;
        }
      });
    }
  }

  List<int> _strength = [];

  void _strengthDialog() {
    showDialog(
        context: context,
        builder: (context) {
          bool selected = false;
          return AlertDialog(
            title: const Text("盡量選擇較大的負重，以正常速度持續反覆直到沒力為止"),
            actions: <Widget>[
              TextButton(
                child: const Text('不儲存'),
                onPressed: () => Navigator.of(context).pop(false),
              ),
              TextButton(
                child: const Text('儲存紀錄'),
                onPressed: () {
                  _strength[widget.itemIndex] =
                      Provider.of<InternalState>(context, listen: false)
                          .bloc!
                          .muscleStrength;
                  locator<LocalStorageService>().strength = _strength;
                  Navigator.of(context).pop(false);
                },
              ),
            ],
            content: StatefulBuilder(builder: (context, StateSetter setState) {
              return CheckboxListTile(
                  title: Visibility(
                    visible: selected,
                    child: const Icon(Icons.fitness_center,
                        color: Colors.blueGrey, size: 30.0),
                  ),
                  value: selected,
                  onChanged: (_) {
                    setState(() {
                      selected = !selected;
                    });
                  });
            }),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    // Provider.of<AiNotifier>(context, listen: false).internalState = InternalState(initLabel: 0, model: ExerciseBloc());
    Provider.of<InternalState>(context, listen: false).bloc = ExerciseBloc();
    Global.currentSelectedItemID = widget.itemIndex;
    // _itemID = Provider.of<InternalState>(context, listen: false).label - 1;
    if (Global.bellWeight < _muscleStrength) {
      _ratio = (Global.bellWeight / _muscleStrength * 100).floor();
    } else {
      _ratio = 100;
    }
    _strength = locator<LocalStorageService>().strength;
    if (widget.itemIndex >= 0) {
      _muscleStrength = _strength[widget.itemIndex];
    }
  }

  @override
  void dispose() {
    super.dispose();
    // Provider.of<InternalState>(context, listen: false).dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StaggeredGrid.count(
        crossAxisCount: 2,
        crossAxisSpacing: 12.0,
        mainAxisSpacing: 12.0,
        // padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        // staggeredTiles: [
        //   StaggeredTile.extent(1, 220.0),
        //   StaggeredTile.extent(1, 220.0),
        //   StaggeredTile.extent(2, 230.0),
        //   StaggeredTile.extent(1, 210.0),
        //   StaggeredTile.extent(1, 210.0),
        // ],
        children: <Widget>[
          StaggeredGridTile.count(
            crossAxisCellCount: 1,
            mainAxisCellCount: 2,
            child: DashboardElement(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // IconButton(
                      //   onPressed: null,
                      //   icon: Icon(Icons.cancel_outlined, color: Colors.red),
                      // ),
                      IconButton(
                        onPressed: () {
                          _showDataDiaolog();
                        },
                        icon: const Icon(Icons.check_circle_outline_outlined,
                            color: Colors.green),
                      ),
                    ],
                  ),
                  const Divider(height: 5),
                  TextButton(
                    onPressed: () async => targetSet =
                        await setTargetNum(context, 0, 10, targetSet),
                    child: Column(
                      children: <Widget>[
                        const Text(
                          '目標組數',
                          style: TextStyle(color: Colors.grey, fontSize: 17),
                        ),
                        Text(
                          '$targetSet 組',
                          style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                            fontSize: 20.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const Divider(height: 5),
                  TextButton(
                    onPressed: () async {
                      targetRepi =
                          await setTargetNum(context, 0, 30, targetRepi);
                      setState(() {});
                    },
                    child: Column(
                      children: <Widget>[
                        const Text(
                          '目標反覆次數',
                          style: TextStyle(color: Colors.grey, fontSize: 17),
                        ),
                        Text(
                          "$targetRepi 次",
                          style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w700,
                              fontSize: 20.0),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          StaggeredGridTile.count(
            crossAxisCellCount: 1,
            mainAxisCellCount: 2,
            child: DashboardElement(
              child: Column(
                children: <Widget>[
                  Selector<InternalState, int>(
                      selector: (_, state) => state.label,
                      builder: (_, label, __) {
                        // _itemID = label - 1;
                        return Text(
                          (label != 0) ? ITEM_NAME[label - 1] : 'Non-Exercise',
                          style: const TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w700,
                            fontSize: 20.0,
                          ),
                        );
                      }),
                  const SizedBox(
                    height: 10,
                  ),
                  // 選擇槓片重量，儲存資料的按鍵
                  Wrap(
                    children: <Widget>[
                      const Icon(
                        Icons.fitness_center,
                        color: Colors.orange,
                        size: 20.0,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      DropdownButton(
                        isDense: true,
                        value: Global.bellWeight,
                        onChanged: changeWeight,
                        items: BellWeights.map((int weight) {
                          return DropdownMenuItem(
                            value: weight,
                            child: Text(
                              weight.toString(),
                              style: const TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w700,
                                fontSize: 20.0,
                              ),
                            ),
                          );
                        }).toList(),
                      ),
                      const Text(
                        'KG',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                          fontSize: 20.0,
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CircularPercentIndicator(
                    radius: 140.0,
                    lineWidth: 13.0,
                    animation: true,
                    animationDuration: 600,
                    percent: (Global.bellWeight < _muscleStrength)
                        ? (Global.bellWeight / _muscleStrength)
                        : 1.0,
                    circularStrokeCap: CircularStrokeCap.round,
                    progressColor: Colors.indigo[400],
                    center: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextButton(
                          onPressed: _strengthDialog,
                          child: Text(
                            '1RM(${_muscleStrength}KG)',
                            style: const TextStyle(color: Colors.grey, fontSize: 20),
                          ),
                        ),
                        Text(
                          '$_ratio%',
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 26.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          StaggeredGridTile.count(
            crossAxisCellCount: 2,
            mainAxisCellCount: 2,
            child: // 運動學參數表格
                DashboardElement(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: StreamBuilder<int>(
                  stream: Provider.of<InternalState>(context, listen: false)
                      .bloc!
                      .repeat,
                  initialData: 0,
                  builder: (context, snapshot) {
                    return SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: DataTable(
                          columns: const [
                            DataColumn(
                              label: Text('重量'),
                            ),
                            DataColumn(
                              label: Text('次數'),
                            ),
                            DataColumn(
                              label: Text('訓練量'),
                            ),
                            DataColumn(
                              label: Text('做功量'),
                            ),
                            DataColumn(
                              label: Text('平均功率'),
                            ),
                            DataColumn(
                              label: Text('最大爆發力'),
                            ),
                          ],
                          rows:
                              Provider.of<InternalState>(context, listen: false)
                                  .bloc!
                                  .theSet
                                  .map(
                                    (theSet) => DataRow(cells: [
                                      DataCell(
                                        Text('${theSet.weight}KG'),
                                        onTap: null,
                                      ),
                                      DataCell(
                                        Text(
                                          '${theSet.repetitions}次',
                                        ),
                                        onTap: null,
                                      ),
                                      DataCell(
                                        Text(
                                          '${theSet.trainingVolume}KG',
                                        ),
                                        onTap: null,
                                      ),
                                      DataCell(
                                        Text(
                                          '${theSet.joule}焦耳',
                                        ),
                                        onTap: null,
                                      ),
                                      DataCell(
                                        Text(
                                          '${theSet.powerAve}W',
                                        ),
                                        onTap: null,
                                      ),
                                      DataCell(
                                        Text(
                                          '${theSet.maxPower}W',
                                        ),
                                        onTap: null,
                                      ),
                                    ]),
                                  )
                                  .toList(),
                        ),
                      ),
                    );
                  },
                ),
              ),
              onTap: () {},
            ),
          ),
          StaggeredGridTile.count(
            crossAxisCellCount: 1,
            mainAxisCellCount: 2,
            child: // 位移曲線
                DashboardElement(
              child: Padding(
                padding: const EdgeInsets.all(24.0),
                child: StreamBuilder<double>(
                    stream: Provider.of<InternalState>(context, listen: false)
                        .bloc!
                        .positionStream,
                    initialData: 0.0,
                    builder:
                        (BuildContext context, AsyncSnapshot<double> snapshot) {
                      if (snapshot.hasError) {
                        return Text('Error: ${snapshot.error}');
                      }
                      if (!snapshot.hasData) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      if (snapshot.connectionState == ConnectionState.active) {
                        final position = snapshot.data!;
                        _setNewDataSet(position);
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    const Text(
                                      '位置',
                                      style: TextStyle(
                                          color: Colors.green, fontSize: 17.0),
                                    ),
                                    Text(position.toStringAsFixed(2) + '公分',
                                        style: const TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 17.0)),
                                  ],
                                ),
                              ],
                            ),
                            const Padding(padding: EdgeInsets.only(bottom: 4.0)),
                            Sparkline(
                              data: baseData,
                              lineGradient: LinearGradient(
                                begin: Alignment.bottomLeft,
                                end: Alignment.topRight,
                                stops: const [0.1, 0.5, 0.7, 0.9],
                                colors: [
                                  Colors.indigo[100]!,
                                  Colors.indigo[400]!,
                                  Colors.indigo[600]!,
                                  Colors.indigo[900]!
                                ],
                              ),
                              lineWidth: 4,
                              fillMode: FillMode.none,
                              pointsMode: PointsMode.last,
                              pointSize: 10.0,
                              pointColor: Colors.red,
                              sharpCorners: false,
                            ),
                          ],
                        );
                      } else {
                        return const Center(
                          child: const CircularProgressIndicator(),
                        );
                      }
                    }),
              ),
            ),
          ),
          StaggeredGridTile.count(
            crossAxisCellCount: 1,
            mainAxisCellCount: 2,
            child: DashboardElement(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: StreamBuilder<int>(
                    stream: Provider.of<InternalState>(context, listen: false)
                        .bloc!
                        .repeat,
                    initialData: 0,
                    builder: (context, snapshot) {
                      final repeatition = snapshot.data;
                      var setNum =
                          Provider.of<InternalState>(context, listen: false)
                              .bloc!
                              .sets;
                      return Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              RichText(
                                text: TextSpan(
                                  text: '第$setNum組/$targetSet ',
                                  style: const TextStyle(
                                    color: Colors.green,
                                  ),
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: ' 第$repeatition次/$targetRepi',
                                      style: TextStyle(
                                        color: Theme.of(context).accentColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              if (repeatition! >= targetRepi)
                                const Icon(
                                  Icons.check_circle,
                                  color: Colors.green,
                                )
                              else
                                const Icon(
                                  Icons.trending_up,
                                  color: Colors.orange,
                                ),
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(bottom: 10.0)),
                          LinearProgressIndicator(
                            value: snapshot.data! / targetRepi,
                          ),
                          Expanded(
                            child: CustomPaint(
                              foregroundPainter: CircleProgress(
                                  Provider.of<InternalState>(context,
                                              listen: false)
                                          .bloc!
                                          .workTotal
                                          .roundToDouble(),
                                  true,
                                  _calories),
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                  children: <Widget>[
                                    const Text(
                                      '做功量',
                                      style: TextStyle(
                                        color: Colors.black45,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    Text(
                                      '${Provider.of<InternalState>(context, listen: false).bloc!.workTotal}',
                                      style: const TextStyle(
                                        color: Colors.black,
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    const Text(
                                      '焦耳',
                                      style: TextStyle(
                                        color: Colors.black45,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    }),
              ),
            ),
          ),
        ]);
  }
}
