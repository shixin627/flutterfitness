﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 <id>j__TPar <>f__AnonymousType0`4::get_id()
// 0x00000002 <seq>j__TPar <>f__AnonymousType0`4::get_seq()
// 0x00000003 <name>j__TPar <>f__AnonymousType0`4::get_name()
// 0x00000004 <data>j__TPar <>f__AnonymousType0`4::get_data()
// 0x00000005 System.Void <>f__AnonymousType0`4::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
// 0x00000006 System.Boolean <>f__AnonymousType0`4::Equals(System.Object)
// 0x00000007 System.Int32 <>f__AnonymousType0`4::GetHashCode()
// 0x00000008 System.String <>f__AnonymousType0`4::ToString()
// 0x00000009 System.Void CubeInteraction::Start()
extern void CubeInteraction_Start_m4E26587DBF6A476068F4F67CC8F96CA177BDACAF (void);
// 0x0000000A System.Void CubeInteraction::Update()
extern void CubeInteraction_Update_mCD3E33F086894B0950A53DB769DB31618B05A205 (void);
// 0x0000000B System.Void CubeInteraction::SetRotationSpeed(System.String)
extern void CubeInteraction_SetRotationSpeed_m62936A4DFA69B5D4FE7092D18E32E7910CF4A77B (void);
// 0x0000000C System.Void CubeInteraction::.ctor()
extern void CubeInteraction__ctor_m3110F7B4017C210FB4336EBB5D8275CEFC022F47 (void);
// 0x0000000D System.Void NativeAPI::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void NativeAPI_OnSceneLoaded_m6609CFF938FAF711ACD63A689B9042B27187D216 (void);
// 0x0000000E System.Void NativeAPI::SendMessageToFlutter(System.String)
extern void NativeAPI_SendMessageToFlutter_m73284C4A7935581C6650800F8F9892F4D8D134A6 (void);
// 0x0000000F System.Void NativeAPI::ShowHostMainWindow()
extern void NativeAPI_ShowHostMainWindow_m8E7E15088B4E5AC59DDE1416EFB192E4C28AE98C (void);
// 0x00000010 System.Void NativeAPI::UnloadMainWindow()
extern void NativeAPI_UnloadMainWindow_mD03B92808173E3E47E91FEB1AED073BE31385C85 (void);
// 0x00000011 System.Void NativeAPI::QuitUnityWindow()
extern void NativeAPI_QuitUnityWindow_mFEC56E99667FDBE86BDB88FBD0FBB051E1EECCCB (void);
// 0x00000012 System.Void NativeAPI::.ctor()
extern void NativeAPI__ctor_m18E74AFFE46A30E49D4DFDD14BB1D30EEB609295 (void);
// 0x00000013 T SingletonMonoBehaviour`1::get_Instance()
// 0x00000014 T SingletonMonoBehaviour`1::CreateSingleton()
// 0x00000015 System.Void SingletonMonoBehaviour`1::.ctor()
// 0x00000016 System.Void SingletonMonoBehaviour`1::.cctor()
// 0x00000017 MessageHandler MessageHandler::Deserialize(System.String)
extern void MessageHandler_Deserialize_mAF411EC208E02166C9302B5B54878A61C5EA03C5 (void);
// 0x00000018 T MessageHandler::getData()
// 0x00000019 System.Void MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
extern void MessageHandler__ctor_m6D62D4CD1FDC1ACBECA277EC939F7BF1A5C0EE0F (void);
// 0x0000001A System.Void MessageHandler::send(System.Object)
extern void MessageHandler_send_m3C7C735E5E60707CFF5545CB117B748643ABBA53 (void);
// 0x0000001B System.Void UnityMessage::.ctor()
extern void UnityMessage__ctor_mB0E205137A7DD4D3E27548C36A0749055E1F64CB (void);
// 0x0000001C System.Int32 UnityMessageManager::generateId()
extern void UnityMessageManager_generateId_m0B012964338C75AE18EDCBB09F0D011845A0052E (void);
// 0x0000001D System.Void UnityMessageManager::add_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_add_OnMessage_m8842E4D444AF900692199658BFE2CB4088DF9E31 (void);
// 0x0000001E System.Void UnityMessageManager::remove_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_remove_OnMessage_m979E48B4644FB584C09CB452AC98F9FD83FD02A0 (void);
// 0x0000001F System.Void UnityMessageManager::add_OnFlutterMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_add_OnFlutterMessage_m67E181A3FAC7FBFC2F52668FB5B26AFD5ED2D8F9 (void);
// 0x00000020 System.Void UnityMessageManager::remove_OnFlutterMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_remove_OnFlutterMessage_m0F6986CFEE7B22A3AFAA04A315CFA296CE393B9E (void);
// 0x00000021 System.Void UnityMessageManager::Start()
extern void UnityMessageManager_Start_mAE1B02EE254A2BF2AAC242DDEB3DB2F983D0E2C9 (void);
// 0x00000022 System.Void UnityMessageManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void UnityMessageManager_OnSceneLoaded_m680CA2A97F09F4FD54B10C042D4D624273DFB3D6 (void);
// 0x00000023 System.Void UnityMessageManager::ShowHostMainWindow()
extern void UnityMessageManager_ShowHostMainWindow_mFABFF2B8A3CFCC9F1F28B19BACF73F6955A16D8A (void);
// 0x00000024 System.Void UnityMessageManager::UnloadMainWindow()
extern void UnityMessageManager_UnloadMainWindow_mB690FF15D63C51FF0F4D431FDCDFACEC973D05BD (void);
// 0x00000025 System.Void UnityMessageManager::QuitUnityWindow()
extern void UnityMessageManager_QuitUnityWindow_mB63D4AB9D0D258FDF8ABB9269D6834DA7DAD065D (void);
// 0x00000026 System.Void UnityMessageManager::SendMessageToFlutter(System.String)
extern void UnityMessageManager_SendMessageToFlutter_m58436B596EA9228CC2CBE497DFD03F58DD589968 (void);
// 0x00000027 System.Void UnityMessageManager::SendMessageToFlutter(UnityMessage)
extern void UnityMessageManager_SendMessageToFlutter_m2CD52D1AFC3D10D2172450BD5D29DD27E3CADDE8 (void);
// 0x00000028 System.Void UnityMessageManager::onMessage(System.String)
extern void UnityMessageManager_onMessage_m844E444B72E9D34837224BE24F45BC4582962B97 (void);
// 0x00000029 System.Void UnityMessageManager::onFlutterMessage(System.String)
extern void UnityMessageManager_onFlutterMessage_m6D227EE9C962486D5BF3CEEFA0272AD3D674219F (void);
// 0x0000002A System.Void UnityMessageManager::.ctor()
extern void UnityMessageManager__ctor_mA4511CB120AE9C3096F93F3AC504156301DAB9F7 (void);
// 0x0000002B System.Void UnityMessageManager/MessageDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageDelegate__ctor_m072A79953B4D2581E0213262217A4B6E4DC9EF3E (void);
// 0x0000002C System.Void UnityMessageManager/MessageDelegate::Invoke(System.String)
extern void MessageDelegate_Invoke_m5C88BF65BFF94836174CF133488C77B334853DCC (void);
// 0x0000002D System.IAsyncResult UnityMessageManager/MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MessageDelegate_BeginInvoke_mBE7EE70496B9C6B3A8161EA15F617E3CA836C016 (void);
// 0x0000002E System.Void UnityMessageManager/MessageDelegate::EndInvoke(System.IAsyncResult)
extern void MessageDelegate_EndInvoke_mB4EBDE0D4F999D929FD66E2D856F7A804D638148 (void);
// 0x0000002F System.Void UnityMessageManager/MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageHandlerDelegate__ctor_m9D49A5D03589E382C69571E88278E84CA1F9FF4F (void);
// 0x00000030 System.Void UnityMessageManager/MessageHandlerDelegate::Invoke(MessageHandler)
extern void MessageHandlerDelegate_Invoke_mC612A87743048C33E692A082D52C47AB68181A80 (void);
// 0x00000031 System.IAsyncResult UnityMessageManager/MessageHandlerDelegate::BeginInvoke(MessageHandler,System.AsyncCallback,System.Object)
extern void MessageHandlerDelegate_BeginInvoke_m1395182BD73CBCCF20E768F09ECEF37B9B835EA2 (void);
// 0x00000032 System.Void UnityMessageManager/MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void MessageHandlerDelegate_EndInvoke_mE403CD191F1F35E443B9221558466560258C80F6 (void);
// 0x00000033 System.Void printOnCanvas::Awake()
extern void printOnCanvas_Awake_m77406E59F8FC6F6ADE67D409E52F9CDB58876635 (void);
// 0x00000034 System.Void printOnCanvas::Start()
extern void printOnCanvas_Start_m0F1B180758F6EF1D8EFE2746B6DAEC8D21DBAA22 (void);
// 0x00000035 System.Void printOnCanvas::Update()
extern void printOnCanvas_Update_mB85A814517BC69A035564E65CAFC0AC0FA0CD0DD (void);
// 0x00000036 System.Void printOnCanvas::.ctor()
extern void printOnCanvas__ctor_m18E8F64FCB3F5F74D6EFAF6CCF3D53FD19E609D5 (void);
// 0x00000037 System.Void Rotate::Awake()
extern void Rotate_Awake_m37950270F7B1C4B3300A061D890B62DF9CF2D481 (void);
// 0x00000038 System.Void Rotate::Start()
extern void Rotate_Start_mD322E77A3CF2BEF28C4DF71D3F529107F511B1FB (void);
// 0x00000039 System.Void Rotate::Update()
extern void Rotate_Update_m73D585515036D9B7AAD8336BFB8567283CE4C7E7 (void);
// 0x0000003A System.Void Rotate::ReceiveMessage(System.String)
extern void Rotate_ReceiveMessage_m47A42582A8D02686DF8FD0A59D750CBD720ABADA (void);
// 0x0000003B System.Void Rotate::dealWithMessage()
extern void Rotate_dealWithMessage_m368E6C9057C6D5ABF1F8067817A79216B60CB473 (void);
// 0x0000003C System.Void Rotate::FixedUpdate()
extern void Rotate_FixedUpdate_m9F61E1367C647FC604344A5CBA52B939F6948AC4 (void);
// 0x0000003D System.Void Rotate::OnGUI()
extern void Rotate_OnGUI_m162D13D6A738B54124E284BD7EC894A8C89201A5 (void);
// 0x0000003E System.Collections.IEnumerator Rotate::Loop()
extern void Rotate_Loop_m487BBCFDAD67CDBF70416584B98FED31708F4383 (void);
// 0x0000003F System.Void Rotate::SetOrigin(System.String)
extern void Rotate_SetOrigin_mBB07FE0AAE5E7B485982F1B7AA766A5154063AE9 (void);
// 0x00000040 System.Void Rotate::SetArmLength(System.String)
extern void Rotate_SetArmLength_m785F83D2C814C94BB49D42D727805362A10CC5E4 (void);
// 0x00000041 System.String Rotate::dataProcess(System.String[])
extern void Rotate_dataProcess_m8E759573B276942610197FA6F674F2EEEC24F29A (void);
// 0x00000042 System.Void Rotate::SetPosition()
extern void Rotate_SetPosition_m51B9EC06FF15BC1E57134DDCD18EDF57EFFE6DC5 (void);
// 0x00000043 System.Void Rotate::.ctor()
extern void Rotate__ctor_m0EE5CC8EB699542BFC438DC3D547D39E442E9EE4 (void);
// 0x00000044 System.Void Rotate/<Loop>d__41::.ctor(System.Int32)
extern void U3CLoopU3Ed__41__ctor_mC06534DFD89B8B78624A9A23A7278E91B6F9712C (void);
// 0x00000045 System.Void Rotate/<Loop>d__41::System.IDisposable.Dispose()
extern void U3CLoopU3Ed__41_System_IDisposable_Dispose_mC994134B373EAEC39409C866AA14EB831AFABA2C (void);
// 0x00000046 System.Boolean Rotate/<Loop>d__41::MoveNext()
extern void U3CLoopU3Ed__41_MoveNext_m33E3DD77CCC1958EAF8170FAF81067FD045D1EC8 (void);
// 0x00000047 System.Object Rotate/<Loop>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoopU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BF9027DA08FB6E39BFBE8930B61D6E76CF5619A (void);
// 0x00000048 System.Void Rotate/<Loop>d__41::System.Collections.IEnumerator.Reset()
extern void U3CLoopU3Ed__41_System_Collections_IEnumerator_Reset_m57A33D797746B7457F11C0976A0A8F52F7C1A2D5 (void);
// 0x00000049 System.Object Rotate/<Loop>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CLoopU3Ed__41_System_Collections_IEnumerator_get_Current_m8332555F91F32BE4FC7FBACC3FFCB1B7AB0B4B02 (void);
// 0x0000004A System.Void testQuat::Start()
extern void testQuat_Start_mB57B10FB70D524C8AD09D66E9DEDE319D4E441B6 (void);
// 0x0000004B System.Void testQuat::Update()
extern void testQuat_Update_m3ED9B1FF8E086ED5FFDFA1F865B1C1044C1CF398 (void);
// 0x0000004C System.Void testQuat::.ctor()
extern void testQuat__ctor_mF515874458312D45D590D2C99917EC104719CE62 (void);
// 0x0000004D System.Void UMMInject::Start()
extern void UMMInject_Start_mCEBECFACD560C56E51BD4FDCAF9267677EBF0B08 (void);
// 0x0000004E System.Void UMMInject::.ctor()
extern void UMMInject__ctor_m5F8C40F8C3A0F7E741B7B880758662EE90F42449 (void);
// 0x0000004F System.Void DitzelGames.FastIK.FastIKFabric::Awake()
extern void FastIKFabric_Awake_m796C522D61246A2EF67C3C0F85A784C173572CBC (void);
// 0x00000050 System.Void DitzelGames.FastIK.FastIKFabric::Init()
extern void FastIKFabric_Init_m1CC0B45816C4F1E55CFAD5CD5DE0F0527416CB81 (void);
// 0x00000051 System.Void DitzelGames.FastIK.FastIKFabric::LateUpdate()
extern void FastIKFabric_LateUpdate_m2D8DDDE8C8F6B4380C25016E3AFC4D2A435197BE (void);
// 0x00000052 System.Void DitzelGames.FastIK.FastIKFabric::ResolveIK()
extern void FastIKFabric_ResolveIK_m893F291E6D1E5DD230F8865615D67AB3E0B28773 (void);
// 0x00000053 UnityEngine.Vector3 DitzelGames.FastIK.FastIKFabric::GetPositionRootSpace(UnityEngine.Transform)
extern void FastIKFabric_GetPositionRootSpace_mE2E9893481DA76ED72134516AC08F55E32DD7DD2 (void);
// 0x00000054 System.Void DitzelGames.FastIK.FastIKFabric::SetPositionRootSpace(UnityEngine.Transform,UnityEngine.Vector3)
extern void FastIKFabric_SetPositionRootSpace_m39FD5377A618165F2CA50DDDCADE69A93EDE9BF3 (void);
// 0x00000055 UnityEngine.Quaternion DitzelGames.FastIK.FastIKFabric::GetRotationRootSpace(UnityEngine.Transform)
extern void FastIKFabric_GetRotationRootSpace_m458EDD2792FB3871CE8DA0E69FC133216ED01A4F (void);
// 0x00000056 System.Void DitzelGames.FastIK.FastIKFabric::SetRotationRootSpace(UnityEngine.Transform,UnityEngine.Quaternion)
extern void FastIKFabric_SetRotationRootSpace_mA49456DDAD05E01A462CEA42BF95B1F228D54C76 (void);
// 0x00000057 System.Void DitzelGames.FastIK.FastIKFabric::OnDrawGizmos()
extern void FastIKFabric_OnDrawGizmos_m8DF02471A11D31F86432F6AA5A0BD3DEF6F78F2D (void);
// 0x00000058 System.Void DitzelGames.FastIK.FastIKFabric::.ctor()
extern void FastIKFabric__ctor_m66C9657C0AB3B23A70BAA585A87CBBA36BC35FEB (void);
// 0x00000059 System.Void DitzelGames.FastIK.FastIKLook::Awake()
extern void FastIKLook_Awake_m18F2C0F567240C69A1691CDC9ACC6ACF15B41E69 (void);
// 0x0000005A System.Void DitzelGames.FastIK.FastIKLook::Update()
extern void FastIKLook_Update_m689C0F835B3091F1EB1F8CBE66C3FBD86099538C (void);
// 0x0000005B System.Void DitzelGames.FastIK.FastIKLook::.ctor()
extern void FastIKLook__ctor_m2F0CC15AB0BDF416D7DB51F972232E78D4279BEA (void);
// 0x0000005C System.Void DitzelGames.FastIK.SampleProcedualAnimation::LateUpdate()
extern void SampleProcedualAnimation_LateUpdate_mCA371346458F6B11F2D6F719DC913B12FB84B277 (void);
// 0x0000005D System.Void DitzelGames.FastIK.SampleProcedualAnimation::.ctor()
extern void SampleProcedualAnimation__ctor_mB9D06F457E24095FABC6CA82E0AB1A17FD22E903 (void);
// 0x0000005E System.Void DitzelGames.FastIK.SampleRotator::Update()
extern void SampleRotator_Update_m8AED1638FF4A95EF714CBA813E3274B62CFB378D (void);
// 0x0000005F System.Void DitzelGames.FastIK.SampleRotator::.ctor()
extern void SampleRotator__ctor_m530FCE0591B7E101C622BBF8C9FC46F5FD5115BD (void);
// 0x00000060 System.Void DitzelGames.FastIK.SampleSinMover::Awake()
extern void SampleSinMover_Awake_m192531A7855D6C9B6976EBC3DAA9C30BF1B27EAC (void);
// 0x00000061 System.Void DitzelGames.FastIK.SampleSinMover::Update()
extern void SampleSinMover_Update_m76072057A82D6DB0FDF173404A8D627A6E1B0123 (void);
// 0x00000062 System.Void DitzelGames.FastIK.SampleSinMover::.ctor()
extern void SampleSinMover__ctor_m9A9CD65E4BA91931B5AAF688F1E4A89653407ACB (void);
static Il2CppMethodPointer s_methodPointers[98] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CubeInteraction_Start_m4E26587DBF6A476068F4F67CC8F96CA177BDACAF,
	CubeInteraction_Update_mCD3E33F086894B0950A53DB769DB31618B05A205,
	CubeInteraction_SetRotationSpeed_m62936A4DFA69B5D4FE7092D18E32E7910CF4A77B,
	CubeInteraction__ctor_m3110F7B4017C210FB4336EBB5D8275CEFC022F47,
	NativeAPI_OnSceneLoaded_m6609CFF938FAF711ACD63A689B9042B27187D216,
	NativeAPI_SendMessageToFlutter_m73284C4A7935581C6650800F8F9892F4D8D134A6,
	NativeAPI_ShowHostMainWindow_m8E7E15088B4E5AC59DDE1416EFB192E4C28AE98C,
	NativeAPI_UnloadMainWindow_mD03B92808173E3E47E91FEB1AED073BE31385C85,
	NativeAPI_QuitUnityWindow_mFEC56E99667FDBE86BDB88FBD0FBB051E1EECCCB,
	NativeAPI__ctor_m18E74AFFE46A30E49D4DFDD14BB1D30EEB609295,
	NULL,
	NULL,
	NULL,
	NULL,
	MessageHandler_Deserialize_mAF411EC208E02166C9302B5B54878A61C5EA03C5,
	NULL,
	MessageHandler__ctor_m6D62D4CD1FDC1ACBECA277EC939F7BF1A5C0EE0F,
	MessageHandler_send_m3C7C735E5E60707CFF5545CB117B748643ABBA53,
	UnityMessage__ctor_mB0E205137A7DD4D3E27548C36A0749055E1F64CB,
	UnityMessageManager_generateId_m0B012964338C75AE18EDCBB09F0D011845A0052E,
	UnityMessageManager_add_OnMessage_m8842E4D444AF900692199658BFE2CB4088DF9E31,
	UnityMessageManager_remove_OnMessage_m979E48B4644FB584C09CB452AC98F9FD83FD02A0,
	UnityMessageManager_add_OnFlutterMessage_m67E181A3FAC7FBFC2F52668FB5B26AFD5ED2D8F9,
	UnityMessageManager_remove_OnFlutterMessage_m0F6986CFEE7B22A3AFAA04A315CFA296CE393B9E,
	UnityMessageManager_Start_mAE1B02EE254A2BF2AAC242DDEB3DB2F983D0E2C9,
	UnityMessageManager_OnSceneLoaded_m680CA2A97F09F4FD54B10C042D4D624273DFB3D6,
	UnityMessageManager_ShowHostMainWindow_mFABFF2B8A3CFCC9F1F28B19BACF73F6955A16D8A,
	UnityMessageManager_UnloadMainWindow_mB690FF15D63C51FF0F4D431FDCDFACEC973D05BD,
	UnityMessageManager_QuitUnityWindow_mB63D4AB9D0D258FDF8ABB9269D6834DA7DAD065D,
	UnityMessageManager_SendMessageToFlutter_m58436B596EA9228CC2CBE497DFD03F58DD589968,
	UnityMessageManager_SendMessageToFlutter_m2CD52D1AFC3D10D2172450BD5D29DD27E3CADDE8,
	UnityMessageManager_onMessage_m844E444B72E9D34837224BE24F45BC4582962B97,
	UnityMessageManager_onFlutterMessage_m6D227EE9C962486D5BF3CEEFA0272AD3D674219F,
	UnityMessageManager__ctor_mA4511CB120AE9C3096F93F3AC504156301DAB9F7,
	MessageDelegate__ctor_m072A79953B4D2581E0213262217A4B6E4DC9EF3E,
	MessageDelegate_Invoke_m5C88BF65BFF94836174CF133488C77B334853DCC,
	MessageDelegate_BeginInvoke_mBE7EE70496B9C6B3A8161EA15F617E3CA836C016,
	MessageDelegate_EndInvoke_mB4EBDE0D4F999D929FD66E2D856F7A804D638148,
	MessageHandlerDelegate__ctor_m9D49A5D03589E382C69571E88278E84CA1F9FF4F,
	MessageHandlerDelegate_Invoke_mC612A87743048C33E692A082D52C47AB68181A80,
	MessageHandlerDelegate_BeginInvoke_m1395182BD73CBCCF20E768F09ECEF37B9B835EA2,
	MessageHandlerDelegate_EndInvoke_mE403CD191F1F35E443B9221558466560258C80F6,
	printOnCanvas_Awake_m77406E59F8FC6F6ADE67D409E52F9CDB58876635,
	printOnCanvas_Start_m0F1B180758F6EF1D8EFE2746B6DAEC8D21DBAA22,
	printOnCanvas_Update_mB85A814517BC69A035564E65CAFC0AC0FA0CD0DD,
	printOnCanvas__ctor_m18E8F64FCB3F5F74D6EFAF6CCF3D53FD19E609D5,
	Rotate_Awake_m37950270F7B1C4B3300A061D890B62DF9CF2D481,
	Rotate_Start_mD322E77A3CF2BEF28C4DF71D3F529107F511B1FB,
	Rotate_Update_m73D585515036D9B7AAD8336BFB8567283CE4C7E7,
	Rotate_ReceiveMessage_m47A42582A8D02686DF8FD0A59D750CBD720ABADA,
	Rotate_dealWithMessage_m368E6C9057C6D5ABF1F8067817A79216B60CB473,
	Rotate_FixedUpdate_m9F61E1367C647FC604344A5CBA52B939F6948AC4,
	Rotate_OnGUI_m162D13D6A738B54124E284BD7EC894A8C89201A5,
	Rotate_Loop_m487BBCFDAD67CDBF70416584B98FED31708F4383,
	Rotate_SetOrigin_mBB07FE0AAE5E7B485982F1B7AA766A5154063AE9,
	Rotate_SetArmLength_m785F83D2C814C94BB49D42D727805362A10CC5E4,
	Rotate_dataProcess_m8E759573B276942610197FA6F674F2EEEC24F29A,
	Rotate_SetPosition_m51B9EC06FF15BC1E57134DDCD18EDF57EFFE6DC5,
	Rotate__ctor_m0EE5CC8EB699542BFC438DC3D547D39E442E9EE4,
	U3CLoopU3Ed__41__ctor_mC06534DFD89B8B78624A9A23A7278E91B6F9712C,
	U3CLoopU3Ed__41_System_IDisposable_Dispose_mC994134B373EAEC39409C866AA14EB831AFABA2C,
	U3CLoopU3Ed__41_MoveNext_m33E3DD77CCC1958EAF8170FAF81067FD045D1EC8,
	U3CLoopU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BF9027DA08FB6E39BFBE8930B61D6E76CF5619A,
	U3CLoopU3Ed__41_System_Collections_IEnumerator_Reset_m57A33D797746B7457F11C0976A0A8F52F7C1A2D5,
	U3CLoopU3Ed__41_System_Collections_IEnumerator_get_Current_m8332555F91F32BE4FC7FBACC3FFCB1B7AB0B4B02,
	testQuat_Start_mB57B10FB70D524C8AD09D66E9DEDE319D4E441B6,
	testQuat_Update_m3ED9B1FF8E086ED5FFDFA1F865B1C1044C1CF398,
	testQuat__ctor_mF515874458312D45D590D2C99917EC104719CE62,
	UMMInject_Start_mCEBECFACD560C56E51BD4FDCAF9267677EBF0B08,
	UMMInject__ctor_m5F8C40F8C3A0F7E741B7B880758662EE90F42449,
	FastIKFabric_Awake_m796C522D61246A2EF67C3C0F85A784C173572CBC,
	FastIKFabric_Init_m1CC0B45816C4F1E55CFAD5CD5DE0F0527416CB81,
	FastIKFabric_LateUpdate_m2D8DDDE8C8F6B4380C25016E3AFC4D2A435197BE,
	FastIKFabric_ResolveIK_m893F291E6D1E5DD230F8865615D67AB3E0B28773,
	FastIKFabric_GetPositionRootSpace_mE2E9893481DA76ED72134516AC08F55E32DD7DD2,
	FastIKFabric_SetPositionRootSpace_m39FD5377A618165F2CA50DDDCADE69A93EDE9BF3,
	FastIKFabric_GetRotationRootSpace_m458EDD2792FB3871CE8DA0E69FC133216ED01A4F,
	FastIKFabric_SetRotationRootSpace_mA49456DDAD05E01A462CEA42BF95B1F228D54C76,
	FastIKFabric_OnDrawGizmos_m8DF02471A11D31F86432F6AA5A0BD3DEF6F78F2D,
	FastIKFabric__ctor_m66C9657C0AB3B23A70BAA585A87CBBA36BC35FEB,
	FastIKLook_Awake_m18F2C0F567240C69A1691CDC9ACC6ACF15B41E69,
	FastIKLook_Update_m689C0F835B3091F1EB1F8CBE66C3FBD86099538C,
	FastIKLook__ctor_m2F0CC15AB0BDF416D7DB51F972232E78D4279BEA,
	SampleProcedualAnimation_LateUpdate_mCA371346458F6B11F2D6F719DC913B12FB84B277,
	SampleProcedualAnimation__ctor_mB9D06F457E24095FABC6CA82E0AB1A17FD22E903,
	SampleRotator_Update_m8AED1638FF4A95EF714CBA813E3274B62CFB378D,
	SampleRotator__ctor_m530FCE0591B7E101C622BBF8C9FC46F5FD5115BD,
	SampleSinMover_Awake_m192531A7855D6C9B6976EBC3DAA9C30BF1B27EAC,
	SampleSinMover_Update_m76072057A82D6DB0FDF173404A8D627A6E1B0123,
	SampleSinMover__ctor_m9A9CD65E4BA91931B5AAF688F1E4A89653407ACB,
};
static const int32_t s_InvokerIndices[98] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4074,
	4074,
	3304,
	4074,
	5675,
	6131,
	6264,
	6264,
	6264,
	4074,
	0,
	0,
	0,
	0,
	5980,
	0,
	683,
	3304,
	4074,
	6233,
	3304,
	3304,
	3304,
	3304,
	4074,
	1895,
	4074,
	4074,
	4074,
	3304,
	3304,
	3304,
	3304,
	4074,
	1866,
	3304,
	902,
	3304,
	1866,
	3304,
	902,
	3304,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	3304,
	4074,
	4074,
	4074,
	3988,
	3304,
	3304,
	2924,
	4074,
	4074,
	3287,
	4074,
	3926,
	3988,
	4074,
	3988,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	3043,
	1882,
	2949,
	1869,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
	4074,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000002, { 0, 29 } },
	{ 0x02000005, { 29, 9 } },
	{ 0x06000018, { 38, 1 } },
};
extern const uint32_t g_rgctx_U3CU3Ef__AnonymousType0_4_t93FD0713D40011506D6B5581D9D5064D1A1A31B8;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_mCBE753640994A70A4B0EB9A431EEC6EC666C3927;
extern const uint32_t g_rgctx_EqualityComparer_1_tE2981F42233FBD735ACCD8037D75D3BB4CD17AB4;
extern const uint32_t g_rgctx_EqualityComparer_1_tE2981F42233FBD735ACCD8037D75D3BB4CD17AB4;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m3A73853DF38D7A5F7435032EADA0E70E576A57E9;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_m539382C7982D9E2955F588CA23AE8EF5AFC5848B;
extern const uint32_t g_rgctx_EqualityComparer_1_tAD002BC32A9F45FB9ADC01B444A699824C79C189;
extern const uint32_t g_rgctx_EqualityComparer_1_tAD002BC32A9F45FB9ADC01B444A699824C79C189;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m87448C8C6392FA740FB5A3A3B9A4FCB16896BDEE;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_m73C9E5DCDC640EC4F088E091D50B61E296387009;
extern const uint32_t g_rgctx_EqualityComparer_1_t86248AA917021F43C77C70906218B4C5685ED26B;
extern const uint32_t g_rgctx_EqualityComparer_1_t86248AA917021F43C77C70906218B4C5685ED26B;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_mEBE02056FDBB2ECEBEFCA1A556F823D3F4040362;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_mDFE711273CB32460A725376F86D0DCFEA7BE3C66;
extern const uint32_t g_rgctx_EqualityComparer_1_tD053F5CE6551182CE911D8C3CE2540B97F156987;
extern const uint32_t g_rgctx_EqualityComparer_1_tD053F5CE6551182CE911D8C3CE2540B97F156987;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m6BA30BEAA0DF6985F9B87C8479952DB424D8241B;
extern const uint32_t g_rgctx_EqualityComparer_1_GetHashCode_mAEF4D26E612F6E42ECFBEAFDF77767D2988E1619;
extern const uint32_t g_rgctx_EqualityComparer_1_GetHashCode_m6089000CD165B6ABC68857AA438A3950CDCC5B43;
extern const uint32_t g_rgctx_EqualityComparer_1_GetHashCode_mBCF474B9011C058E0570312097A7F34C09C33DF6;
extern const uint32_t g_rgctx_EqualityComparer_1_GetHashCode_mB641ECF38D3923819EB55D5F12C5B16C3264E1A1;
extern const uint32_t g_rgctx_U3CidU3Ej__TPar_tB4283D6614B0A1B9713411CF590AE6445EC76C76;
extern const Il2CppRGCTXConstrainedData g_rgctx_U3CidU3Ej__TPar_tB4283D6614B0A1B9713411CF590AE6445EC76C76_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_U3CseqU3Ej__TPar_t5290712E6748D411D87B0461BCF3FCF7A8412B71;
extern const Il2CppRGCTXConstrainedData g_rgctx_U3CseqU3Ej__TPar_t5290712E6748D411D87B0461BCF3FCF7A8412B71_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_U3CnameU3Ej__TPar_tF038DFA17A672607E28AD9AB1F07F3C69402F7F1;
extern const Il2CppRGCTXConstrainedData g_rgctx_U3CnameU3Ej__TPar_tF038DFA17A672607E28AD9AB1F07F3C69402F7F1_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_U3CdataU3Ej__TPar_tA1CCD24F7E35C5B61B29E317B30895EC9D15768F;
extern const Il2CppRGCTXConstrainedData g_rgctx_U3CdataU3Ej__TPar_tA1CCD24F7E35C5B61B29E317B30895EC9D15768F_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_SingletonMonoBehaviour_1_tA8F980F1A4D2DA2F48E858FFFDA5F4D13A0D4063;
extern const uint32_t g_rgctx_Lazy_1_t1C4897E5FBC8891B01D8D3B0B6CFA4E18974A8D4;
extern const uint32_t g_rgctx_Lazy_1_get_Value_m4E2C72C47D27B454F97BA9079088DFDF0DF86A68;
extern const uint32_t g_rgctx_T_t8F6FCAC7D8192731C50D798732A4E7C76CE7936E;
extern const uint32_t g_rgctx_GameObject_AddComponent_TisT_t8F6FCAC7D8192731C50D798732A4E7C76CE7936E_m6225C9FC78CAF862EE83E2B33A0CE681BE4562A3;
extern const uint32_t g_rgctx_SingletonMonoBehaviour_1_CreateSingleton_m68A3AFE92EFA65722778D4C7B58FF1DA88D3EAE6;
extern const uint32_t g_rgctx_Func_1_tACD7DBE3E3D5C33F1088A8DA07C947EC0B819DAE;
extern const uint32_t g_rgctx_Func_1__ctor_mE9D6AE5C7628FF3BCB1D27840C8C3E05E83254F0;
extern const uint32_t g_rgctx_Lazy_1__ctor_m6990B7C537DBA57961F99FA00368FB57B76E5047;
extern const uint32_t g_rgctx_Extensions_Value_TisT_t4148FB4D9D18C75DC9922798FDB4DF511D04E40B_mADE502482047AE8765D72A61A47C506F63FA4F69;
static const Il2CppRGCTXDefinition s_rgctxValues[39] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ef__AnonymousType0_4_t93FD0713D40011506D6B5581D9D5064D1A1A31B8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_mCBE753640994A70A4B0EB9A431EEC6EC666C3927 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tE2981F42233FBD735ACCD8037D75D3BB4CD17AB4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tE2981F42233FBD735ACCD8037D75D3BB4CD17AB4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m3A73853DF38D7A5F7435032EADA0E70E576A57E9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_m539382C7982D9E2955F588CA23AE8EF5AFC5848B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tAD002BC32A9F45FB9ADC01B444A699824C79C189 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tAD002BC32A9F45FB9ADC01B444A699824C79C189 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m87448C8C6392FA740FB5A3A3B9A4FCB16896BDEE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_m73C9E5DCDC640EC4F088E091D50B61E296387009 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t86248AA917021F43C77C70906218B4C5685ED26B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t86248AA917021F43C77C70906218B4C5685ED26B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_mEBE02056FDBB2ECEBEFCA1A556F823D3F4040362 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_mDFE711273CB32460A725376F86D0DCFEA7BE3C66 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tD053F5CE6551182CE911D8C3CE2540B97F156987 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tD053F5CE6551182CE911D8C3CE2540B97F156987 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m6BA30BEAA0DF6985F9B87C8479952DB424D8241B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_GetHashCode_mAEF4D26E612F6E42ECFBEAFDF77767D2988E1619 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_GetHashCode_m6089000CD165B6ABC68857AA438A3950CDCC5B43 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_GetHashCode_mBCF474B9011C058E0570312097A7F34C09C33DF6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_GetHashCode_mB641ECF38D3923819EB55D5F12C5B16C3264E1A1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CidU3Ej__TPar_tB4283D6614B0A1B9713411CF590AE6445EC76C76 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_U3CidU3Ej__TPar_tB4283D6614B0A1B9713411CF590AE6445EC76C76_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CseqU3Ej__TPar_t5290712E6748D411D87B0461BCF3FCF7A8412B71 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_U3CseqU3Ej__TPar_t5290712E6748D411D87B0461BCF3FCF7A8412B71_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CnameU3Ej__TPar_tF038DFA17A672607E28AD9AB1F07F3C69402F7F1 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_U3CnameU3Ej__TPar_tF038DFA17A672607E28AD9AB1F07F3C69402F7F1_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CdataU3Ej__TPar_tA1CCD24F7E35C5B61B29E317B30895EC9D15768F },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_U3CdataU3Ej__TPar_tA1CCD24F7E35C5B61B29E317B30895EC9D15768F_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SingletonMonoBehaviour_1_tA8F980F1A4D2DA2F48E858FFFDA5F4D13A0D4063 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Lazy_1_t1C4897E5FBC8891B01D8D3B0B6CFA4E18974A8D4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Lazy_1_get_Value_m4E2C72C47D27B454F97BA9079088DFDF0DF86A68 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t8F6FCAC7D8192731C50D798732A4E7C76CE7936E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_GameObject_AddComponent_TisT_t8F6FCAC7D8192731C50D798732A4E7C76CE7936E_m6225C9FC78CAF862EE83E2B33A0CE681BE4562A3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SingletonMonoBehaviour_1_CreateSingleton_m68A3AFE92EFA65722778D4C7B58FF1DA88D3EAE6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_tACD7DBE3E3D5C33F1088A8DA07C947EC0B819DAE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1__ctor_mE9D6AE5C7628FF3BCB1D27840C8C3E05E83254F0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Lazy_1__ctor_m6990B7C537DBA57961F99FA00368FB57B76E5047 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Extensions_Value_TisT_t4148FB4D9D18C75DC9922798FDB4DF511D04E40B_mADE502482047AE8765D72A61A47C506F63FA4F69 },
};
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationAssemblyU2DCSharp;
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	98,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	39,
	s_rgctxValues,
	&g_DebuggerMetadataRegistrationAssemblyU2DCSharp,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
