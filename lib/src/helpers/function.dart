import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';

void showMessage(BuildContext context, String message) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(message)));
}

Future<int> setTargetNum(BuildContext context, int minValue, int maxValue,
    int originalTarget) async {
  int target = originalTarget;
  await showDialog<int>(
      context: context,
      builder: (BuildContext context) {
        return NumberPicker(
          minValue: minValue,
          maxValue: maxValue,
          value: originalTarget,
          onChanged: (int value) {
            target = value;
          },
        );
      });
  return target;
}

List<double> setDataSet(
    List<double> currentDataSet, List<double> previousDataSet, double newData,
    {int sizeOfArray = 10}) {
  currentDataSet.clear();
  currentDataSet.addAll(previousDataSet);
  currentDataSet.add(newData);
  if (currentDataSet.length >= sizeOfArray) {
    for (int i = 0; i <= currentDataSet.length - sizeOfArray; i++) {
      currentDataSet.removeAt(i);
    }
  }
  return currentDataSet;
}

Future<int> getStamp(List<int> data) async =>
    (data[0] + 256 * data[1] + 65536 * data[2] + 16777216 * data[3]);
