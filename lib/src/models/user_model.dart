class UserModel {  //使用者登入資料
  int id;  //註冊後會取得的使用者編號,尚未註冊或是還沒登入都為0
  String email;
  String? name;
  UserModel({required this.id, required this.email, this.name});

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'email': email,
      'name': name,
    };
  }

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json['id'] as int,
      email: json['email'] as String,
      name: json['name'] as String,
    );
  }
}

class UserID {
  int key;
  UserID(this.key);

  factory UserID.fromJson(dynamic json) {
    return UserID(json['id'] as int);
  }

  @override
  String toString() {
    return '{ $key }';
  }
}
