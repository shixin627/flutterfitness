import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter_blue/flutter_blue.dart';
import '/src/data/constant.dart';
import '/src/data/global.dart';
import '/src/helpers/function.dart';
import '/src/models/fitness_item.dart';
import 'package:provider/provider.dart';
import '../models/math.dart';
import 'data_monitor.dart';
import 'unity_provider.dart';
import '../helpers/decoder.dart';
import '../blocs/system_event.dart';

// class BLEUUID {
//   final String address;
//   final String serviceUUID;
//   final String characteristicUUID;
//   const BLEUUID({this.address, this.serviceUUID, this.characteristicUUID});
// }

class BTNotifier extends ChangeNotifier {
  Locator locator;
  BTNotifier(this.locator) {
    _listenBluetoothStatus();
  }

  bool _isBLEon = false;
  bool get isBLEon => _isBLEon;
  set isBLEon(bool state) {
    _isBLEon = state;
    notifyListeners();
  }

  bool _isScanning = false;
  bool get isScanning => _isScanning;
  set isScanning(bool value) {
    if (value != _isScanning) {
      _isScanning = value;
      notifyListeners();
    }
  }

  // 是否找存有特徵
  bool _isAvailable = false;
  bool get isAvailable => _isAvailable;
  set isAvailable(bool value) {
    if (value != _isAvailable) {
      _isAvailable = value;
      notifyListeners();
    }
  }

  BluetoothDeviceState? _targetStatus = BluetoothDeviceState.disconnected;
  BluetoothDeviceState? get targetStatus => _targetStatus;
  set targetStatus(BluetoothDeviceState? status) {
    _targetStatus = status;
    notifyListeners();
  }

  StreamSubscription<ScanResult>? _scanSubScription;

  BluetoothDevice? _targetDevice;
  BluetoothDevice? get targetDevice => _targetDevice;
  set targetDevice(BluetoothDevice? device) {
    _targetDevice = device;
    notifyListeners();
  }

  BluetoothCharacteristic? _rtcCharacteristic;
  BluetoothCharacteristic? get rtcCharacteristic => _rtcCharacteristic;
  set rtcCharacteristic(BluetoothCharacteristic? c) {
    if (_rtcCharacteristic != c) {
      _rtcCharacteristic = c;
      notifyListeners();
    }
  }

  BluetoothCharacteristic? _margCharacteristic;
  BluetoothCharacteristic? get margCharacteristic => _margCharacteristic;
  set margCharacteristic(BluetoothCharacteristic? c) {
    if (c != margCharacteristic) {
      _margCharacteristic = c;
      notifyListeners();
    }
  }

  BluetoothCharacteristic? _hrmCharacteristic;
  BluetoothCharacteristic? get hrmCharacteristic => _hrmCharacteristic;
  set hrmCharacteristic(BluetoothCharacteristic? c) {
    if (hrmCharacteristic != c) {
      _hrmCharacteristic = c;
      debugPrint("set $hrmCharacteristic");
      notifyListeners();
    }
  }

  StreamSubscription<SystemEvent>? _margSubscription;
  StreamSubscription<SystemEvent>? get margSubscription => _margSubscription;
  set margSubscription(StreamSubscription<SystemEvent>? subscriber) {
    if (subscriber != null) {
      _margSubscription = subscriber;
      notifyListeners();
    }
  }

  StreamSubscription<SystemEvent>? _hrmSubscription;
  StreamSubscription<SystemEvent>? get hrmSubscription => _hrmSubscription;
  set hrmSubscription(StreamSubscription<SystemEvent>? subscriber) {
    if (subscriber != null) {
      _hrmSubscription = subscriber;
      notifyListeners();
    }
  }

  void _listenBluetoothStatus() {
    FlutterBlue.instance.state.listen((event) {
      if (event == BluetoothState.on) {
        if (isBLEon == false) {
          isBLEon = true;
        }
      } else if (event == BluetoothState.off) {
        if (isBLEon == true) {
          isBLEon = false;
          isAvailable = false;
          stopScan();
        }
      }
    });
  }

  void startScan() {
    if (!isAvailable) {
      if (_scanSubScription != null) {
        _scanSubScription!.resume();
      } else {
        debugPrint('scanning...');
        isScanning = true;
        _scanSubScription = FlutterBlue.instance.scan().listen((scanResult) {
          if (scanResult.device.id.toString() == bindedAddress) {
            if (locator<FitnessItemModel>().isAvailable == false) {
              isAvailable = true;
              locator<FitnessItemModel>().isAvailable = true;
              targetDevice = scanResult.device;
              locator<FitnessItemModel>().target = targetDevice;
              listenTargetDevice();
              stopScan();
            }
          }
        }, onDone: () {
          debugPrint("目標搜索完成!");
        });
      }
    }
  }

  void stopScan() {
    debugPrint('stop scanning');
    FlutterBlue.instance.stopScan();
    isScanning = false;
    _scanSubScription!.cancel();
    _scanSubScription = null;
  }

  Future<void> refreshTargetBLE() async {
    await Future.delayed(const Duration(seconds: 1));
    stopScan();
    locator<FitnessItemModel>().resetBLE();
    startScan();
  }

  //連線
  Future<void> connectToDevice() async {
    if (targetDevice == null) return;
    await targetDevice!.connect();
    // discoverServices();
  }

  //斷連
  Future<void> disconnectFromDevice() async {
    if (targetDevice == null) {
      return;
    }
    await targetDevice!.disconnect();
  }

  Future<bool> setMTUas(int mtu) async {
    if (mtu > 0 && mtu <= 512) {
      debugPrint("Set MTU as $mtu");
      await targetDevice!.requestMtu(mtu);
      return true;
    } else {
      return false;
    }
  }

  //讀取RTC的時間
  Future<int?> readTimestamp() async {
    int? stamp;
    await rtcCharacteristic!.read().then((value) async {
      if (value.length == 4) {
        stamp = await getStamp(value);
        debugPrint("Timestamp: $stamp");
      }
    });
    return stamp;
  }

  // 手機向微控制器讀取RTC的時間
  Future<void> readFeedback() async {
    if (rtcCharacteristic != null) {
      await readTimestamp().then((timestamp) {
        final chipTime =
            DateTime.fromMillisecondsSinceEpoch(timestamp! * 1000, isUtc: true);
        locator<DataMonitor>().setChipTime(chipTime);
      });
      Timer(Duration(milliseconds: 200), () async {
        await readTimestamp().then(
            (timestamp) => locator<DataMonitor>().systemTimeStamp = timestamp!);
      });
    }
  }

  //寫入命令，並讀取來自微控制器端的回覆時間
  Future<void> writeCommand(List<int> bytes) async {
    if (rtcCharacteristic == null) return;
    await rtcCharacteristic!.write(bytes).then((_) => readFeedback());
  }

  //RTC寫入
  Future<void> writeTimeData(List<int> bytes) async {
    if (rtcCharacteristic == null) return;
    await rtcCharacteristic!.write(bytes);
  }

  // 將手機端的標準時間寫入微控制器端的RTC模組
  Future<void> sendRealTimeData() async {
    DateTime now = DateTime.now();
    int second = now.second;
    int minute = now.minute;
    int hour = now.hour;
    int dayOfWeek = now.weekday;
    int dayOfMonth = now.day;
    int month = now.month;
    int year = now.year;
    final bytes = [
      second + 1,
      minute,
      hour,
      dayOfWeek,
      dayOfMonth,
      month,
      year - 2000
    ];
    print(bytes);
    await writeTimeData(bytes);
  }

  /// 監聽目標藍芽設備的狀態
  void listenTargetDevice() {
    debugPrint('listen target device');
    targetDevice!.state.listen((state) {
      targetStatus = state;
      if (state == BluetoothDeviceState.disconnected) {
        debugPrint('藍芽斷連');
      }

      if (state == BluetoothDeviceState.connected) {
        // 每當連線成功時自動開始搜索藍芽服務
        discoverServices();
      }
    });
  }

  // 發現藍芽服務
  Future<void> discoverServices() async {
    debugPrint('Discovering Services...');
    if (targetDevice == null) return;
    await targetDevice!.discoverServices().then((services) {
      for (var service in services) {
        
        if (service.uuid.toString() == W_SERVICE_UUID) {
          debugPrint("W_SERVICE_UUID: $service");
          for (var characteristic in service.characteristics) {
            if (characteristic.uuid.toString() ==
                CHARACTERISTIC_UUID_MAP["MARG"]) {
                  debugPrint("MARG found");
              margCharacteristic = characteristic;
            } else if (characteristic.uuid.toString() ==
                CHARACTERISTIC_UUID_MAP["HRM"]) {
                  debugPrint("HRM found");
              hrmCharacteristic = characteristic;
            } else if (characteristic.uuid.toString() ==
                CHARACTERISTIC_UUID_MAP["TIMER"]) {
                  debugPrint("TIMER found");
              rtcCharacteristic = characteristic;
            }
          }
        }
      }
    });
  }

  StreamSubscription<SystemEvent> subscribeMARG() {
    return margCharacteristic!.value
        .map((signal) => MargEvent(signal))
        .listen((event) {
      final packet = event.packet;
      // print('read ${packet.length} packet');
      if (packet.length > 20) {
        // 若封包的位元組數大於20
        Decoder.decipher(packet).then((samples) {
          if (samples != null) {
            locator<UnityNotifier>().createNewTask(samples);
          } else {
            debugPrint('decipher error');
          }
        });
      }
    });
  }

  StreamSubscription<SystemEvent> subscribeHeartRate() {
    return hrmCharacteristic!.value
        .map((signal) => HeartRateEvent(signal))
        .listen((event) {
      final packet = event.packet;
      if (packet.length == 1) {
        locator<DataMonitor>().heartrate = packet[0];
      }
    });
  }

  // 客戶端向服務端要求監聽藍芽特徵
  Future<void> listenCharacteristic(String key) async {
    debugPrint("listen characteristic: " + key);
    if (key == "MARG") {
      await margCharacteristic?.setNotifyValue(true);
      margSubscription = subscribeMARG();
    } else if (key == "HRM") {
      await hrmCharacteristic?.setNotifyValue(true);
      hrmSubscription = subscribeHeartRate();
    }
  }

  void unsubscribe(String key) {
    if (key == "MARG") {
      if (margSubscription != null) {
        margCharacteristic!.setNotifyValue(false).then((value) {
          if (value) {
            margSubscription?.pause();
            notifyListeners();
            debugPrint("unsubscribed MARG");
          }
        });
      }
    } else if (key == "HRM") {
      if (hrmSubscription != null) {
        hrmCharacteristic!.setNotifyValue(false).then((value) {
          if (value) {
            hrmSubscription?.pause();
            notifyListeners();
            debugPrint("unsubscribed HRM");
          }
        });
      }
    }
  }

  Future<List<Arm>> getLeftArm(List values) async {
    List<Arm> list = [];
    values.map((sample) {
      if (sample.runtimeType == Arm) {
        list.add(sample as Arm);
      }
    }).toList();
    return list;
  }
}
