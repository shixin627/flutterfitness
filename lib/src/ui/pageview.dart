import 'dart:async';
import 'package:flutter/material.dart';
import '/src/data/global.dart';
import '/src/helpers/data_manager.dart';
import '/src/ui/fitness_menu.dart';
import 'package:provider/provider.dart';
import '../providers/bluetooth_notifier.dart';
import '../providers/data_monitor.dart';
import '../providers/unity_provider.dart';
import '../service_locator.dart';
import '../services/auth.dart';
import '../services/database.dart';
import '../widgets/bluetooth_button.dart';
import '../widgets/dialog.dart';
import 'characteristics_panel.dart';
import 'unity_view.dart';


class MyPageView extends StatefulWidget {
  const MyPageView({Key? key}) : super(key: key);

  @override
  _MyPageViewState createState() => _MyPageViewState();
}

class _MyPageViewState extends State<MyPageView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  PageController? _pageController;
  StreamController<int>? _pageIndexController;
  int _selectedIndex = 0;

  final textController = TextEditingController();

  List<Widget> _menuOptions = <Widget>[
    FitnessMenuScreen(),
    MyUnityView(),
    CharacteristicPanel(),
    // DashBoardFree(),
  ];

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _selectedIndex);
    _pageIndexController = StreamController<int>();
  }

  @override
  void dispose() {
    _pageController!.dispose();
    _pageIndexController!.close();
    textController.dispose();
    super.dispose();
  }

  void _onItemTapped(int index) {
    _selectedIndex = index;
    _pageIndexController!.add(_selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "SMART LAB",
          style: Theme.of(context).textTheme.headline6,
        ),
        // centerTitle: true,
        actions: [
          BLEbutton(),
          IconButton(
            icon: const Icon(Icons.edit),
            onPressed: () {
              locator<BTNotifier>().setMTUas(512);
            },
          ),
        ],
      ),
      drawer: Drawer(
        child: Container(
          child: ListView(
            children: <Widget>[
              const DrawerHeader(
                child: Text('FITNESS CENTER'),
              ),
              ListTile(
                title: const Text('登出'),
                trailing: const Icon(Icons.logout),
                onTap: () {
                  signOutGoogle();
                },
              ),
              ListTile(
                title: const Text('設定'),
                trailing: const Icon(Icons.settings_applications),
                onTap: () {
                  Navigator.pushNamed(context,
                      '/settings'); // it will navigate to settings route
                },
              ),
              ListTile(
                title: const Text('個人檔案'),
                trailing: const Icon(Icons.person),
                onTap: () {
                  Navigator.pushNamed(context, '/profile');
                },
              ),
              ListTile(
                title: const Text('歷史紀錄'),
                trailing: const Icon(Icons.history),
                onTap: () {
                  Navigator.pushNamed(context, '/history');
                },
              ),
            ],
          ),
        ),
      ),
      body: PageView(
        controller: _pageController,
        onPageChanged: _onItemTapped,
        children: _menuOptions,
        physics: const BouncingScrollPhysics(), // NeverScrollableScrollPhysics(),
      ),
      floatingActionButton: Selector<UnityNotifier, bool>(
        selector: (_, provider) => provider.isRecording,
        builder: (context, isRecording, child) {
          if (isRecording) {
            return FloatingActionButton(
              onPressed: () {
                context.read<UnityNotifier>().isRecording = false;
                showCupertinoDialog(context);
              },
              child: const Icon(Icons.stop),
              backgroundColor: Colors.redAccent,
            );
          } else {
            return FloatingActionButton(
              onPressed: () {
                context.read<BTNotifier>().writeCommand([93]);
                context.read<DataMonitor>().getPhoneTime();
                context.read<UnityNotifier>().isRecording = true;
              },
              child: const Icon(Icons.add),
            );
          }
        },
      ),
      bottomNavigationBar: StreamBuilder<int>(
          stream: _pageIndexController!.stream,
          builder: (context, snapshot) {
            return BottomNavigationBar(
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.menu_rounded),
                  label: 'Menu',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.home_outlined),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.dashboard_customize),
                  label: 'Dashboard',
                ),
              ],
              currentIndex: _selectedIndex,
              selectedItemColor: Theme.of(context).colorScheme.secondary,
              onTap: (index) => _pageController!.jumpToPage(index),
            );
          }),
    );
  }

  Future<void> useDialogToRequestMTU(BuildContext context) async {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return RequestMtuDialog(textController);
      },
    );
  }

  void showCupertinoDialog(BuildContext context) async {
    final item = context.read<DataMonitor>().freeWeightItem;
    // 從資料庫讀出剛才所記錄的所有資料
    await context.read<DataBaseSQLite>().queryDataset(item).then((queries) {
      if (queries != null) {
        showDialog(
            context: context,
            builder: (_) => ChoiceField(
                  content: '是否保留為檔案',
                  textLeft: '刪除',
                  callbackLeft: () {
                    context
                        .read<DataBaseSQLite>()
                        .deleteTable(item)
                        .then((_) => Navigator.pop(context));
                  },
                  textRight: '儲存',
                  callbackRight: () {
                    if (queries.isNotEmpty) {
                      final rtcTime = context.read<DataMonitor>().rtcTime;
                      final sysTimestamp =
                          context.read<DataMonitor>().systemTimeStamp;
                      List<String> lines =
                          List.generate(queries.length, (index) {
                        queries[index].systemTime =
                            rtcTime!.millisecondsSinceEpoch +
                                (queries[index].systemTime - sysTimestamp!);
                        return queries[index].packet;
                      });

                      DataManager.writeCSVFile(
                              lines, item, userId, '${DateTime.now()}.csv')
                          .then((file) {
                        if (file != null) {
                          context
                              .read<DataBaseSQLite>()
                              .deleteTable(item)
                              .then((_) => Navigator.of(context).pop());
                        } else {
                          Navigator.of(context).pop();
                        }
                      });
                      print('儲存完畢');
                    }
                  },
                ));
      } else {
        print(null);
      }
    });
  }
}
