import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyThemeData {
  static ThemeData brightness = ThemeData(
    brightness: Brightness.light,
    scaffoldBackgroundColor: const Color(0xFFFFFFFF),
    backgroundColor: const Color(0xFFE1F5FE),
    primaryColor: const Color(0xff5BC0BE),
    textTheme: GoogleFonts.latoTextTheme(),
    colorScheme:
        ColorScheme.fromSwatch().copyWith(secondary: const Color(0xFF0277BD)),
  );

  static ThemeData darkness = ThemeData(
    brightness: Brightness.dark,
    scaffoldBackgroundColor: const Color(0xFF37474F),
    backgroundColor: const Color(0xFF37474F),
    primaryColor: const Color(0xff5BC0BE), //Color(0xFFFF8C00)
    textTheme: GoogleFonts.latoTextTheme(),
    colorScheme:
        ColorScheme.fromSwatch().copyWith(secondary: const Color(0xFF0277BD)),
  );
}

const TextStyle myBoldStyle =
    TextStyle(color: Color(0xff5BC0BE), fontSize: 24.0);
