import 'dart:async';

import 'counter_event.dart';

class CounterBloc {
  //為了記數(上數/下數)事件所使用的 Bloc(商業邏輯) pattern
  int _counter = 0;

  final _counterEventController =
      StreamController<CounterEvent>(); //"事件控制者"，型別為自訂義的抽象物件"CounterEvent"
  // For events, exposing only a sink which is an input
  Sink<CounterEvent> get counterEventSink => _counterEventController.sink;

  final _counterStateController = StreamController<int>(); //"狀態控制者"，型別為integer
  // For state, exposing only a sink which is an input
  StreamSink<int> get _inCounter =>
      _counterStateController.sink; //stream sink接收input狀態
  // For state, exposing only a stream which outputs data
  Stream<int> get counter =>
      _counterStateController.stream; //產生一個型別為integer的Stream作為輸出

  CounterBloc() {
    // Whenever there is a new event, we want to map it to a new state
    _counterEventController.stream
        .listen(_mapEventToState); //觸發的事件會輸入函式"_mapEventToState"
  }

  void _mapEventToState(CounterEvent event) {
    //根據輸入的事件產生對應的狀態
    if (event is IncrementEvent) {
      _counter++;
      _inCounter.add(_counter); //將新的變數"_counter"輸入狀態控制器，告知其發生狀態變化
    } else if (event is DecrementEvent) {
      _counter--;
      _inCounter.add(_counter);
    } else if (event is UpdateEvent) {
      _counter = event.newValue;
      _inCounter.add(_counter);
    }
  }

  void dispose() {
    _counterStateController.close(); //關閉狀態控制器
    _counterEventController.close(); //關閉事件控制器
  }
}
