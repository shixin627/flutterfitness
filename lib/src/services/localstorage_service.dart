import 'dart:convert';

import 'package:flutter/material.dart';

import '/src/data/constant.dart';
import '/src/models/body_size.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/user_model.dart';
import '../models/permission.dart';

const String UserKey = 'user';
const String MuscleKey = 'muscle';
const String PermissionKey = 'permission';
const String BodyKey = 'body';
const String BindedKey = 'binded';

class LocalStorageService {
  // 儲存於本地的shared preferences服務
  static LocalStorageService? _instance;
  static SharedPreferences? _preferences;

  static Future<LocalStorageService?> getInstance() async {
    //調動內部儲存空間時通常耗時較久，會使用async與await函式等待其餘作業完成後再執行
    _instance ??= LocalStorageService();
    _preferences ??= await SharedPreferences.getInstance();
    return _instance;
  }

  // 以下根據對應的"字串Key"
  UserModel? get user {
    var userJson = _getFromDisk(UserKey);
    if (userJson == null) {
      return null;
    }

    return UserModel.fromJson(json.decode(userJson)); //解碼
  }

  set user(UserModel? userToSave) {
    if (userToSave != null) {
      saveStringToDisk(UserKey, json.encode(userToSave.toJson())); //編碼後儲存
    }
  }

  List<int> get strength {
    var strengthJson = _getFromDisk(MuscleKey);
    if (strengthJson != null) {
      return List<int>.from(json.decode(strengthJson));
    } else {
      final list = List<int>.filled(itemLen, 0);
      strength = list;
      return list;
    }
  }

  set strength(List<int> strenghtToSave) {
    saveStringToDisk(MuscleKey, json.encode(strenghtToSave));
  }

  Permission? get permission {
    var permissionJson = _getFromDisk(PermissionKey);
    if (permissionJson == null) {
      return null;
    }
    return Permission.fromJson(json.decode(permissionJson));
  }

  set permission(Permission? permissionToSave) {
    saveStringToDisk(PermissionKey, json.encode(permissionToSave));
  }

  Body get body {
    final bodyJson = _getFromDisk(BodyKey);
    if (bodyJson != null) {
      return Body.fromJson(json.decode(bodyJson));
    } else {
      return Body(foreArm: 29.0, upperArm: 28.0);
    }
  }

  void saveBody(Body bodyToSave) {
    saveStringToDisk(BodyKey, json.encode(bodyToSave));
  }

  String get bindedAddress {
    final address = _getFromDisk(BindedKey);
    return address as String;
  }

  set bindedAddress(String bindedAddress) {
    saveStringToDisk(BindedKey, bindedAddress);
  }

  //調動及儲存的函式
  dynamic _getFromDisk(String key) {
    //依據key從固定記憶體位址獲取資料
    final value = _preferences!.get(key);
    debugPrint(
        '(TRACE) LocalStorageService:_getFromDisk. key: $key value: $value');
    return value;
  }

  void saveStringToDisk(String key, String content) {
    //依據key儲存到固定記憶體位址
    debugPrint(
        '(TRACE) LocalStorageService:_saveStringToDisk. key: $key value: $content');
    _preferences!.setString(key, content);
  }
}
