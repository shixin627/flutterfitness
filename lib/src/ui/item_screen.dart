import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '/src/data/constant.dart';
import '../models/ExerciseData_model.dart';
import '../models/history_line.dart';
import '../models/date_model.dart';
import '../services/navigation_service.dart';
import '../widgets/material.dart';
import '../widgets/theme.dart';
import '../service_locator.dart';

class TheItemScreen extends StatefulWidget {
  final int itemId;
  final List<DataSet> dataSetList;
  const TheItemScreen(this.itemId, this.dataSetList, {Key? key})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return TheItemScreenState(itemId, dataSetList);
  }
}

class TheItemScreenState extends State<TheItemScreen> {
  final int itemId;
  final List<DataSet> dataSetList;
  TheItemScreenState(this.itemId, this.dataSetList);
  final NavigationService _navigationService = locator<NavigationService>();

  final HistoryData _history = HistoryData();
  int _actualIndex = 0;
  late String _item;
  final List<String> _lineDropdownItems = [
    '總做功量',
    '總運動期間',
    '平均功率',
    '總訓練量',
    '肌耐力',
    '爆發力',
  ];

  prepareLineList(List<DataSet> _list) {
    _history.initial(_history.baseIndex);
    if (_list.isNotEmpty) {
      for (var _dataset in _list) {
        _history.addNextList(Date.dateFormat.parse(_dataset.datetime), _dataset,
            _history.baseIndex); //加入至歷史曲線圖中的一天
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _item = ITEM_NAME[itemId];
    _history.index = _lineDropdownItems[_actualIndex];
    _history.baseIndex = 'on_Item';
    prepareLineList(dataSetList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        title: Text(
          _item,
          style: const TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w700,
            fontSize: 30.0,
          ),
        ),
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: colorPrimary),
          onPressed: () => _navigationService.goBack(),
        ),
      ),
      body: ScreenBackground(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: StaggeredGrid.count(
            crossAxisCount: 2,
            crossAxisSpacing: 12.0,
            mainAxisSpacing: 12.0,
            children: <Widget>[
              StaggeredGridTile.count(
                crossAxisCellCount: 2,
                mainAxisCellCount: 4,
                child: DashboardElement(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  HistoryData.title[_actualIndex],
                                  style: const TextStyle(color: Colors.green),
                                ),
                              ],
                            ),
                            DropdownButton(
                              isDense: true,
                              value: _history.index,
                              onChanged: (value) {
                                var txt = value as String;
                                setState(() {
                                  _history.index = txt;
                                  _actualIndex = _lineDropdownItems
                                      .indexOf(txt); // Refresh the chart
                                });
                              },
                              items: _lineDropdownItems.map((String title) {
                                return DropdownMenuItem(
                                  value: title,
                                  child: Text(title,
                                      style: const TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.0)),
                                );
                              }).toList(),
                            ),
                          ],
                        ),
                        const Padding(padding: EdgeInsets.only(bottom: 20.0)),
                        Expanded(
                          child: TimeSeriesChart(_history),
                        ),
                      ],
                    ),
                  ),
                  onTap: () =>
                      debugPrint(_history.maptoLine[_actualIndex].toString()),
                ),
              ),
            ],
            // staggeredTiles: [
            //   StaggeredTile.extent(2, 450.0),
            // ],
          ),
        ),
      ),
    );
  }
}
