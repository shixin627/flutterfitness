import 'dart:convert';

class InBody {
  int id;
  int bodyID;
  String datetime;
  BodyQuality body;

  InBody(this.id, this.bodyID, this.datetime, this.body);

  Map<String, dynamic> toJson() {
    Map _body = body.toJson();
    return {'body_id': bodyID, 'datetime': datetime, 'body': _body};
  }

  factory InBody.fromJson(Map<String, dynamic> json) {
    var _jsonBody = jsonDecode(json['body']);
    return InBody(
      json['id'] as int,
      json['body_id'] as int,
      json['datetime'] as String,
      BodyQuality.fromJson(_jsonBody),
    );
  }

  @override
  String toString() {
    return '{ $id, $bodyID, $datetime, $body }';
  }
}

class BodyQuality {
  double weight;
  double smm;
  double fat;
  int score;

  BodyQuality(
      {required this.weight,
      required this.smm,
      required this.fat,
      required this.score});

  Map<String, dynamic> toJson() {
    return {
      'weight': weight,
      'smm': smm,
      'fat': fat,
      'score': score,
    };
  }

  factory BodyQuality.fromJson(Map<String, dynamic> json) {
    return BodyQuality(
      weight: json['weight'] as double,
      smm: json['smm'] as double,
      fat: json['fat'] as double,
      score: json['score'] as int,
    );
  }

  @override
  String toString() {
    return '{ $weight, $smm, $fat, $score }';
  }
}
