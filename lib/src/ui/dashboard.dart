import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'dart:convert';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import '/src/data/constant.dart';
import '/src/data/global.dart';
import '/src/helpers/decoder.dart';
import '/src/helpers/function.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import '../data/constant.dart';
import '../models/ExerciseData_model.dart';
import '../models/fitness_item.dart';
import '../models/date_model.dart';
import '../services/cloud_service.dart';
import '../services/local_database_service.dart';
import '../services/localstorage_service.dart';
import '../widgets/material.dart';
import '../service_locator.dart';
import '../widgets/theme.dart';
import '../widgets/CircleProgress.dart';
import '../blocs/exercise_bloc.dart';
import '../blocs/exercise_event.dart';

class DashboardPage extends StatefulWidget {
  final int itemIndex;
  const DashboardPage({Key? key, required this.itemIndex}) : super(key: key);
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  int actualweight = BellWeights[3]; //設定初始值為表單的第一項
  int actualBellIndex = 0; //實際重量

  final _bloc = ExerciseBloc();
// Bluetooth
  bool isReady = false;
  Stream<List<int>>? stream;
  StreamSubscription<List<int>>? clientListener;
  FlutterBlue flutterBlue = FlutterBlue.instance;
  BluetoothDevice? targetDevice;
  BluetoothCharacteristic? targetCharacteristic;
  // User's information
  late int _calories;
  late List<int> _strength;
  late int _muscleStrength;
  late int _ratio;
  // Targets of the item
  static int targetSet = 2;
  static int targetRepi = 8;

  // Current dataset
  late int _itemIndex;

  // dataset of position
  bool switchDataSet = false;
  List<double> baseData = <double>[0.0, 0.0];
  List<double> dataSetA = <double>[];
  List<double> dataSetB = <double>[];

  final int sizeOfArray = 10;

  // 藍芽連線
  Future<void> connectToDevice() async {
    if (targetDevice == null) {
      _pop();
      return;
    }
    Timer(const Duration(seconds: 15), () {
      if (isReady == false) {
        disconnectFromDevice();
        _pop();
      }
    });

    await targetDevice!.connect();
    discoverServices();
  }

  Future<void> disconnectFromDevice() async {
    if (targetDevice == null) {
      _pop();
      return;
    }
    debugPrint("disconnect from device!");
    targetDevice!.disconnect();
  }

  Future<void> discoverServices() async {
    if (targetDevice == null) {
      _pop();
      return;
    }

    List<BluetoothService> services = await targetDevice!.discoverServices();
    for (var service in services) {
      if (service.uuid.toString() == M_SERVICE_UUID) {
        for (var characteristic in service.characteristics) {
          if (characteristic.uuid.toString() == M_CHARACTERISTIC_UUID) {
            characteristic.setNotifyValue(!characteristic.isNotifying);
            targetCharacteristic = characteristic;
            stream = targetCharacteristic!.value;
            // 監聽藍芽特徵值
            clientListener = stream!.listen((value) async {
              final strings = await Decoder.decodeUTF8(value);
              dataHandler(strings);
            });

            setState(() {
              isReady = true;
            });
          }
        }
      }
    }

    if (!isReady) {
      _pop();
    }
  }

  _pop() {
    Navigator.of(context).pop(true);
  }

  // 藍芽客戶端寫入資料給服務端
  // writeData(String data) async {
  //   if (targetCharacteristic == null) return;
  //   List<int> bytes = utf8.encode(data);
  //   await targetCharacteristic.write(bytes);
  // }

  /// 更新資料集(位置)
  void _setNewDataSet(double position) {
    if (switchDataSet) {
      baseData = setDataSet(dataSetB, dataSetA, position);
    } else {
      baseData = setDataSet(dataSetA, dataSetB, position);
    }
    switchDataSet = !switchDataSet;
  }

  /// 輸入解碼後的封包[dataList]至資料處理函式
  Future<void> dataHandler(List<String> dataList) async {
    // print(dataList);
    if (dataList.isNotEmpty) {
      if (dataList.length == 1) {
        /// "[position]"
        debugPrint("運動中...更新位置");
        final position = double.parse(dataList[0]);
        _bloc.exerciseEventSink.add(MovementEvent(position));
      } else if (dataList.length > 1) {
        final headKey = dataList[0];
        switch (headKey) {
          case 'B':
            {
              /// "B,[sets]" or "B,[sets],[restBetweenSets]"
              debugPrint("開始運動，目前負重為 $actualweight KG");
              final sets = int.parse(dataList[1]);
              if (dataList.length == 3) {
                final restBetweenSets = int.parse(dataList[2]);
                _bloc.exerciseEventSink.add(StartEvent(sets, actualweight,
                    restBetweenSets: restBetweenSets));
              } else {
                _bloc.exerciseEventSink.add(StartEvent(sets, actualweight));
              }
            }
            break;

          case 'R':
            {
              /// "R,[accVsLengthIntergration],[durationInSeconds],[repetitions]"
              debugPrint("完成一趟反覆");
              final accVsLengthIntergration = double.parse(dataList[1]);
              final durationInSeconds = int.parse(dataList[2]);
              final repetitions = int.parse(dataList[3]);
              _bloc.exerciseEventSink.add(RepeatEvent(
                  accVsLengthIntergration, durationInSeconds, repetitions));
            }
            break;

          default:
            debugPrint('nothing happened');
            break;
        }
      }
    }
  }

  //提醒視窗
  Future<bool> _onWillPop() async {
    final _context = context;
    return await showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: const Text('提醒'),
              content: const Text('結束訓練\n本次的運動數據將會被儲存至手機\n若有開啟網路也會同時上傳至雲端'),
              actions: <Widget>[
                TextButton(
                  child: const Text('不儲存'),
                  onPressed: () => Navigator.of(context).pop(true),
                ),
                TextButton(
                  child: const Text('繼續'),
                  onPressed: () => Navigator.of(context).pop(false),
                ),
                TextButton(
                  child: const Text('儲存'),
                  onPressed: () {
                    debugPrint(_bloc.workTotal.toString());
                    if (_bloc.workTotal != 0) {
                      final timestamp = Date.dateFormat.format(DateTime.now());

                      LocalDatabaseService.insertData({
                        'user_id': localUser.id,
                        'item_id': _itemIndex,
                        'datetime': timestamp,
                        'dataset': json.encode(_bloc.theSet),
                        'interRest': json.encode(_bloc.restBetweenSets),
                      });

                      if (localUser.id > 0) {
                        DataSet _dataSet = DataSet(
                            0, localUser.id.toString(), _itemIndex, timestamp,
                            dataset: _bloc.theSet,
                            interRest: _bloc.restBetweenSets);

                        CloudService.submitData(_context, _dataSet);
                      }
                    }
                    _pop();
                  },
                ),
              ],
            )) as bool;
  }

  /// 運動指南
  void _showInfoDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("運動指南"),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () => Navigator.of(context).pop(false),
              ),
            ],
            content: Column(
              children: <Widget>[
                Text(
                  '提升爆發力：建議重量${(_muscleStrength * 0.5).floor()}(KG) PS.最大肌力的50%',
                  style: const TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.w700,
                    fontSize: 20.0,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '提升肌耐力：建議重量${(_muscleStrength * 0.7).floor()}(KG) PS.最大肌力的70%',
                  style: const TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.w700,
                    fontSize: 20.0,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '提升肌力：建議重量${(_muscleStrength * 0.9).floor()}(KG) PS.你最大肌力的90%',
                  style: const TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.w700,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
          );
        });
  }

  void _strengthDialog() {
    showDialog(
        context: context,
        builder: (context) {
          bool selected = false;
          return AlertDialog(
            title: const Text("盡量選擇較大的負重，以正常速度持續反覆直到沒力為止"),
            actions: <Widget>[
              TextButton(
                child: const Text('不儲存'),
                onPressed: () => Navigator.of(context).pop(false),
              ),
              TextButton(
                child: const Text('儲存紀錄'),
                onPressed: () {
                  debugPrint("${_bloc.muscleStrength}");
                  _strength[_itemIndex] = _bloc.muscleStrength;
                  locator<LocalStorageService>().strength = _strength;
                  Navigator.of(context).pop(false);
                },
              ),
            ],
            content: StatefulBuilder(builder: (context, StateSetter setState) {
              return CheckboxListTile(
                  title: Visibility(
                    visible: selected,
                    child: const Icon(Icons.fitness_center,
                        color: Colors.blueGrey, size: 30.0),
                  ),
                  value: selected,
                  onChanged: (_) {
                    setState(() {
                      selected = !selected;
                    });
                  });
            }),
          );
        });
  }

  ValueChanged<int?>? _changeWeight(int? value) {
    if (value != null) {
      setState(() {
        actualweight = value;
        actualBellIndex = BellWeights.indexOf(value);
        if (actualweight < _muscleStrength) {
          _ratio = (actualweight / _muscleStrength * 100).floor();
        } else {
          _ratio = 100;
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
    targetDevice = Provider.of<FitnessItemModel>(context, listen: false).target;
    connectToDevice(); // 以藍芽連接設備
    LocalDatabaseService.open(); //打開 SQLite
    _calories = 10000;
    _itemIndex = widget.itemIndex;
    _strength = locator<LocalStorageService>().strength;
    _muscleStrength = _strength[_itemIndex];
    if (actualweight < _muscleStrength) {
      _ratio = (actualweight / _muscleStrength * 100).floor();
    } else {
      _ratio = 100;
    }
  }

  @override
  void dispose() {
    disconnectFromDevice();
    _bloc.dispose();
    clientListener?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          elevation: 2.0,
          backgroundColor: colorGrey,
          title: const Text(
            'Dashboard',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w700,
              fontSize: 30.0,
            ),
          ),
          actions: <Widget>[
            Container(
              margin: const EdgeInsets.only(right: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                    icon: const Icon(
                      Icons.info_outline,
                      color: Colors.orange,
                      size: 20.0,
                    ),
                    onPressed: _showInfoDialog,
                  ),
                ],
              ),
            ),
          ],
        ),
        body: StaggeredGrid.count(
          crossAxisCount: 2,
          crossAxisSpacing: 12.0,
          mainAxisSpacing: 12.0,
          // padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          // staggeredTiles: [
          //   StaggeredTile.extent(1, 220.0),
          //   StaggeredTile.extent(1, 220.0),
          //   StaggeredTile.extent(2, 230.0),
          //   StaggeredTile.extent(1, 210.0),
          //   StaggeredTile.extent(1, 210.0),
          // ],
          children: <Widget>[
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 2,
              child: DashboardElement(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Divider(height: 5),
                    TextButton(
                      onPressed: () async => targetSet =
                          await setTargetNum(context, 0, 10, targetSet),
                      child: Column(
                        children: <Widget>[
                          const Text(
                            '目標組數',
                            style: TextStyle(color: Colors.grey, fontSize: 17),
                          ),
                          Text(
                            '$targetSet 組',
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w700,
                              fontSize: 20.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Divider(height: 5),
                    TextButton(
                      onPressed: () async {
                        targetRepi =
                            await setTargetNum(context, 0, 30, targetRepi);
                        setState(() {});
                      },
                      child: Column(
                        children: <Widget>[
                          const Text('目標反覆次數',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 17)),
                          Text("$targetRepi 次",
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20.0))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 2,
              child: DashboardElement(
                child: Column(
                  children: <Widget>[
                    Text(
                      '${CATEGORIES[_itemIndex]}-${ITEM_NAME[_itemIndex]}',
                      style: const TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.w700,
                        fontSize: 20.0,
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Wrap(
                      children: <Widget>[
                        const Icon(
                          Icons.fitness_center,
                          color: Colors.orange,
                          size: 20.0,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        DropdownButton(
                          isDense: true,
                          value: actualweight,
                          onChanged: _changeWeight,
                          items: BellWeights.map((int weight) {
                            return DropdownMenuItem(
                              value: weight,
                              child: Text(
                                weight.toString(),
                                style: const TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20.0,
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                        const Text(
                          'KG',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                            fontSize: 20.0,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    CircularPercentIndicator(
                      radius: 140.0,
                      lineWidth: 13.0,
                      animation: true,
                      animationDuration: 600,
                      percent: (Global.bellWeight < _muscleStrength)
                          ? (Global.bellWeight / _muscleStrength)
                          : 1.0,
                      circularStrokeCap: CircularStrokeCap.round,
                      progressColor: Colors.indigo[400],
                      center: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          TextButton(
                            onPressed: _strengthDialog,
                            child: Text(
                              '1RM(${_muscleStrength}KG)',
                              style: const TextStyle(
                                  color: Colors.grey, fontSize: 20),
                            ),
                          ),
                          Text(
                            '$_ratio%',
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 26.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 2,
              mainAxisCellCount: 2,
              child: DashboardElement(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: StreamBuilder<int>(
                    stream: _bloc.repeat,
                    initialData: 0,
                    builder: (context, snapshot) {
                      return SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: DataTable(
                            columns: const [
                              DataColumn(
                                label: Text('重量'),
                              ),
                              DataColumn(
                                label: Text('次數'),
                              ),
                              DataColumn(
                                label: Text('訓練量'),
                              ),
                              DataColumn(
                                label: Text('做功量'),
                              ),
                              DataColumn(
                                label: Text('平均功率'),
                              ),
                              DataColumn(
                                label: Text('最大爆發力'),
                              ),
                            ],
                            rows: _bloc.theSet
                                .map(
                                  (theSet) => DataRow(cells: [
                                    DataCell(
                                      Text('${theSet.weight}KG'),
                                      onTap: () {},
                                    ),
                                    DataCell(
                                      Text(
                                        '${theSet.repetitions}次',
                                      ),
                                      onTap: () {},
                                    ),
                                    DataCell(
                                      Text(
                                        '${theSet.trainingVolume}KG',
                                      ),
                                      onTap: () {},
                                    ),
                                    DataCell(
                                      Text(
                                        '${theSet.joule}焦耳',
                                      ),
                                      onTap: () {},
                                    ),
                                    DataCell(
                                      Text(
                                        '${theSet.powerAve}W',
                                      ),
                                      onTap: () {},
                                    ),
                                    DataCell(
                                      Text(
                                        '${theSet.maxPower}W',
                                      ),
                                      onTap: () {},
                                    ),
                                  ]),
                                )
                                .toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                onTap: () {},
              ),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 2,
              child: DashboardElement(
                child: Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: (isReady == false)
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : StreamBuilder<double>(
                          stream: _bloc.positionStream,
                          initialData: 0.0,
                          builder: (BuildContext context,
                              AsyncSnapshot<double> snapshot) {
                            if (snapshot.hasError) {
                              return Text('Error: ${snapshot.error}');
                            }
                            if (!snapshot.hasData) {
                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                            if (snapshot.connectionState ==
                                ConnectionState.active) {
                              final position = snapshot.data!;
                              _setNewDataSet(position);
                              return Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          const Text(
                                            '位置',
                                            style: TextStyle(
                                                color: Colors.green,
                                                fontSize: 17.0),
                                          ),
                                          Text(
                                              position.toStringAsFixed(2) +
                                                  '公分',
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 17.0)),
                                        ],
                                      ),
                                    ],
                                  ),
                                  const Padding(
                                      padding: EdgeInsets.only(bottom: 4.0)),
                                  Sparkline(
                                    data: baseData,
                                    lineGradient: LinearGradient(
                                      begin: Alignment.bottomLeft,
                                      end: Alignment.topRight,
                                      stops: const [0.1, 0.5, 0.7, 0.9],
                                      colors: [
                                        Colors.indigo[100]!,
                                        Colors.indigo[400]!,
                                        Colors.indigo[600]!,
                                        Colors.indigo[900]!
                                      ],
                                    ),
                                    lineWidth: 4,
                                    fillMode: FillMode.none,
                                    pointsMode: PointsMode.last,
                                    pointSize: 10.0,
                                    pointColor: Colors.red,
                                    sharpCorners: false,
                                  ),
                                ],
                              );
                            } else {
                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                          }),
                ),
              ),
            ),
            StaggeredGridTile.count(
              crossAxisCellCount: 1,
              mainAxisCellCount: 2,
              child: DashboardElement(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: StreamBuilder<int>(
                      stream: _bloc.repeat,
                      initialData: 0,
                      builder: (context, snapshot) {
                        final repetitions = snapshot.data!;
                        return Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                RichText(
                                  text: TextSpan(
                                    text: '第${_bloc.sets}組/$targetSet ',
                                    style: const TextStyle(
                                      color: Colors.green,
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: ' 第$repetitions/$targetRepi',
                                        style: TextStyle(
                                          color: colorAccent,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 17.0,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                if (repetitions >= targetRepi)
                                  const Icon(
                                    Icons.check_circle,
                                    color: Colors.green,
                                  )
                                else
                                  const Icon(
                                    Icons.trending_up,
                                    color: Colors.orange,
                                  ),
                              ],
                            ),
                            const Padding(
                                padding: EdgeInsets.only(bottom: 10.0)),
                            LinearProgressIndicator(
                              value: repetitions / targetRepi,
                            ),
                            Expanded(
                              child: CustomPaint(
                                foregroundPainter: CircleProgress(
                                    _bloc.workTotal.roundToDouble(),
                                    true,
                                    _calories),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      const Text(
                                        '做功量',
                                        style: TextStyle(
                                          color: Colors.black45,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      Text(
                                        '${_bloc.workTotal}',
                                        style: const TextStyle(
                                          color: Colors.black,
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      const Text(
                                        '焦耳',
                                        style: TextStyle(
                                          color: Colors.black45,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      }),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
