import 'package:get_it/get_it.dart';
import './services/localstorage_service.dart';
import './services/navigation_service.dart';

// 建立instance的locator物件(class)
GetIt locator = GetIt.instance;

// 建立Local storage service的locator
Future setupLocator() async {
  LocalStorageService? instance = await LocalStorageService.getInstance();
  locator.registerSingleton<LocalStorageService>(instance!);
}

void setupNavLocator() {  //建立 Navigation service的locator
  locator.registerLazySingleton(() => NavigationService());
}