import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import '../models/math.dart';

class DataBaseSQLite{
  Future<Database> database(String item) async => openDatabase(
        join(await getDatabasesPath(), '$item.db'),
        onCreate: (db, version) {
          return db.execute(
            "CREATE TABLE $item(timestamp INTEGER PRIMARY KEY, data TEXT, frame TEXT)",
          );
        },
        version: 1,
      );

  Future<bool> insertData(String item, Map<String, dynamic> map) async {
    if (map.isNotEmpty) {
      final Database db = await database(item);
      await db.insert(
        item,
        map,
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
      return true;
    } else {
      print('No valid data can be inserted');
      return false;
    }
  }

  Future<void> insertDataSet(String item, List<Arm> dataset) async {
    final Database db = await database(item);
    for (var i = 0; i < dataset.length; i++) {
      await db.insert(
        item,
        dataset[i].toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }
  }

  Future<List<Arm>> queryDataset(String item) async {
    final Database db = await database(item);

    final List<Map<String, dynamic>> maps = await db.query(item);
    print(maps.length);
    return List.generate(maps.length, (i) {
      // print(maps[i]);
      return Arm.fromMap(maps[i]);
    });
  }

  Future<void> updateData(String item, Map<String, dynamic> map) async {
    final db = await database(item);
    await db.update(
      item,
      map,
      where: "timestamp = ?",
      whereArgs: [map['timeStamp']],
    );
  }

  Future<void> deleteData(String item, int timestamp) async {
    final db = await database(item);

    await db.delete(
      item,
      where: "timestamp = ?",
      whereArgs: [timestamp],
    );
  }

  Future<void> deleteTable(String item) async {
    final db = await database(item);
    await db.delete(item);
  }
}
