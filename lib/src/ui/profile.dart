import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '/src/data/global.dart';
import '/src/data/constant.dart';
import '/src/widgets/custom_form.dart';
import 'binding_bluetooth.dart';
import 'package:image_picker/image_picker.dart';
import '../models/inbody_model.dart';
import '../models/user_model.dart';
import '../services/cloud_service.dart';
import '../services/localstorage_service.dart';
import '../service_locator.dart';
import '../widgets/theme.dart';
import 'package:percent_indicator/percent_indicator.dart';
import '../widgets/itemCard.dart';
import 'developer.dart';
import 'inbody.dart';

class ProfilePage extends StatefulWidget {
  final String? email;
  final String? uid;
  final String? displayName;
  final String? photoUrl;

  const ProfilePage({
    Key? key,
    this.email,
    this.uid,
    this.displayName,
    this.photoUrl,
  }) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  UserModel? _user;
  List<int> _strength = [];

  ///
  bool _editMode = false;
  TextEditingController? _displayNameController;
  String? _titleText;
  String? _displayName;
  String? _photoUrl;
  XFile? _image;
  List<InBody> _listInBody = [];

  Future<void> refreshList() async {
    _listInBody = await CloudService.fetchInBody(_user!.id);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _user = locator<LocalStorageService>().user ??
        UserModel(id: 0, email: 'null', name: 'null');
    _strength = locator<LocalStorageService>().strength;
    refreshList();
    _displayNameController = TextEditingController(
      text: _user!.name ?? ' ',
    );
    _titleText = '個人檔案';
    _displayName = _user!.name ?? ' ';
    _photoUrl = widget.photoUrl;
  }

  Widget _divider() => Divider(
        color: Colors.blueGrey[500],
        indent: 24.0,
        endIndent: 24.0,
        thickness: 1.0,
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: colorGrey,
        title: Text(
          _titleText!,
          style: const TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w700,
            fontSize: 30.0,
          ),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              // Details Card
              Container(
                width: double.infinity,
                child: Card(
                  color: Colors.blueGrey[200],
                  margin: const EdgeInsets.only(top: 60.0, bottom: 28.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 90.0, 2.0, 16.0),
                    child: SingleChildScrollView(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            // Display Name
                            Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    AnimatedSwitcher(
                                      duration:
                                          const Duration(milliseconds: 100),
                                      child: _editMode
                                          ? _editNameWidget()
                                          : _displayNameWidget(
                                              _displayName!,
                                            ),
                                    ),
                                  ],
                                ),
                              ],
                            ),

                            // Level
                            CircularPercentIndicator(
                              radius: 120.0,
                              lineWidth: 13.0,
                              animation: true,
                              animationDuration: 600,
                              percent: 0.7,
                              circularStrokeCap: CircularStrokeCap.round,
                              progressColor: Colors.indigo[400],
                              center: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const <Widget>[
                                  Text(
                                    "健康評估",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "100",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 26.0,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            // Bottom Options
                            _divider(),

                            MyCustomForm(
                              body: body,
                              callback: locator<LocalStorageService>().saveBody,
                            ),

                            Material(
                              elevation: 5.0,
                              borderRadius: BorderRadius.circular(30.0),
                              color: colorAccent,
                              child: MaterialButton(
                                minWidth: MediaQuery.of(context).size.width,
                                padding: const EdgeInsets.fromLTRB(
                                    20.0, 15.0, 20.0, 15.0),
                                onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          BindingBluetoothPage()),
                                ),
                                child: Text(
                                  "記憶新的藍芽健身設備",
                                  textAlign: TextAlign.center,
                                  style: textStyle.copyWith(
                                      fontSize: 21,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),

                            _divider(),

                            const Text(
                              '最大肌力(1RM)',
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 20.0,
                              ),
                            ),

                            Center(
                              child: Container(
                                height: 250,
                                child: Swiper(
                                  itemCount: itemLen,
                                  loop: false,
                                  viewportFraction: 0.8,
                                  scale: 0.7,
                                  outer: true,
                                  pagination: const SwiperPagination(
                                    alignment: Alignment.bottomCenter,
                                    margin: const EdgeInsets.all(8.0),
                                  ),
                                  onTap: (index) => null,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Stack(
                                      children: <Widget>[
                                        Center(
                                          child: Container(
                                            height: 250,
                                            child: Opacity(
                                              opacity: 0.3,
                                              child: ItemCard(
                                                item: ITEM_NAME[index],
                                                color: colorGrey,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Center(
                                          child: Container(
                                            child: Text(
                                              '${(_strength[index])}KG',
                                              style: TextStyle(
                                                  fontSize: 30,
                                                  fontWeight: FontWeight.bold,
                                                  color: colorAccent),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ),
                            ),

                            _divider(),

                            const SizedBox(
                              height: 20,
                            ),
                            Container(
                              child: (_listInBody.isNotEmpty)
                                  ? SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: DataTable(
                                          columns: const [
                                            DataColumn(
                                              label: Text('時間'),
                                            ),
                                            DataColumn(
                                              label: Text('體重'),
                                            ),
                                            DataColumn(
                                              label: Text('骨骼肌重量'),
                                            ),
                                            DataColumn(
                                              label: Text('脂肪重量'),
                                            ),
                                            DataColumn(
                                              label: Text('健康評估'),
                                            ),
                                          ],
                                          rows: _listInBody.map((inbody) {
                                            return DataRow(cells: [
                                              DataCell(
                                                Text(inbody.datetime),
                                                onTap: () {},
                                              ),
                                              DataCell(
                                                Text(
                                                    '${inbody.body.weight} KG'),
                                                onTap: () {},
                                              ),
                                              DataCell(
                                                Text('${inbody.body.smm} KG'),
                                                onTap: () {},
                                              ),
                                              DataCell(
                                                Text('${inbody.body.fat} KG'),
                                                onTap: () {},
                                              ),
                                              DataCell(
                                                Text(
                                                  '${inbody.body.score} 分',
                                                ),
                                                onTap: () {},
                                              ),
                                            ]);
                                          }).toList(),
                                        ),
                                      ),
                                    )
                                  : const Center(
                                      child: Text(
                                        '沒有讀取到InBody的歷史資料',
                                      ),
                                    ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Material(
                              elevation: 5.0,
                              borderRadius: BorderRadius.circular(30.0),
                              color: colorAccent,
                              child: MaterialButton(
                                minWidth: MediaQuery.of(context).size.width,
                                padding: const EdgeInsets.fromLTRB(
                                    20.0, 15.0, 20.0, 15.0),
                                onPressed: () => (_user!.id > 0)
                                    ? Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                UploadInBody(_user!.id)),
                                      )
                                    : null,
                                child: Text(
                                  "輸入 InBody 量測數據",
                                  textAlign: TextAlign.center,
                                  style: textStyle.copyWith(
                                      fontSize: 21,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),

              // Avatar
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(70.0),
                    border: Border.all(
                      color: Colors.blueGrey[100]!,
                      width: 6.0,
                    ),
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.black12,
                        blurRadius: 2.0,
                      ),
                    ]),
                child: Stack(
                  children: <Widget>[
                    Hero(
                      tag: 'profile',
                      child: CircleAvatar(
                        radius: 60,
                        backgroundImage: (_editMode != true)
                            ? (_photoUrl == null)
                                ? const AssetImage(
                                    'assets/images/profile-image.png',
                                  )
                                : const AssetImage(
                                    'assets/images/profile-image.png',
                                  )
                            : _image == null
                                ? const AssetImage(
                                    'assets/images/profile-image.png',
                                  )
                                : const AssetImage(
                                    'assets/images/profile-image.png',
                                  ),
                      ),
                    ),

                    // Edit Button
                    Positioned(
                      right: 0.0,
                      bottom: 0.0,
                      child: AnimatedContainer(
                        margin: EdgeInsets.fromLTRB(
                          0,
                          0,
                          _editMode ? 0 : 15,
                          _editMode ? 0 : 15,
                        ),
                        duration: const Duration(milliseconds: 100),
                        height: _editMode ? 32 : 0,
                        width: _editMode ? 32 : 0,
                        decoration: BoxDecoration(
                          color: Colors.blueGrey[100],
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: FloatingActionButton(
                          heroTag: null,
                          elevation: 0.0,
                          backgroundColor: Colors.blueGrey[100],
                          child: Icon(
                            Icons.edit,
                            color: Colors.blueGrey[700],
                            size: _editMode ? 19 : 0,
                          ),
                          onPressed: (){},
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      // Edit Floating Action Button
      floatingActionButton: FloatingActionButton(
        onPressed: () => _editMode
            ? _onConfirmUpdate(
                displayName: _displayNameController!.text,
                photo: _image!,
              )
            : _onEdit(context),
        child: Icon(
          _editMode ? Icons.done : Icons.edit,
          color: Colors.blueGrey[900],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget _displayNameWidget(String displayName) {
    return Center(
      child: Container(
        alignment: Alignment.center,
        width: 200,
        padding: const EdgeInsets.fromLTRB(0, 12.0, 3.0, 12.0),
        child: Text(
          displayName,
          style: const TextStyle(
            fontSize: 25.0,
          ),
        ),
      ),
    );
  }

  Widget _editNameWidget() {
    return Container(
      width: 200,
      child: TextField(
        autofocus: true,
        style: const TextStyle(
          fontSize: 25,
        ),
        controller: _displayNameController,
        textAlign: TextAlign.center,
        decoration: const InputDecoration(),
      ),
    );
  }

  Future<void> _onEdit(BuildContext context) async {
    setState(() {
      _editMode = !_editMode;
      _titleText = '更新檔案';
    });
  }

  void _onConfirmUpdate({
    String? displayName,
    XFile? photo,
  }) async {
    _user!.name = displayName;
    locator<LocalStorageService>().user = _user;

    setState(() {
      if (!mounted) {
        return;
      }
      _editMode = !_editMode;
      _displayName = displayName;
      _titleText = '個人檔案';
    });
  }

  // Future _getImage() async {
  //   XFile? image = await ImagePicker.pickImage(
  //     source: ImageSource.gallery,
  //     maxHeight: 256,
  //     maxWidth: 256,
  //   );
  //   debugPrint(image.toString());

  //   setState(() {
  //     if (!mounted) {
  //       return;
  //     }
  //     _image = image!;
  //   });
  // }
}
