import 'package:flutter/material.dart';

class ListHeader extends StatelessWidget {
  final String headerText;
  final VoidCallback? seeAllCallback;
  final Widget widget;

  const ListHeader(this.headerText, {Key? key, this.seeAllCallback, required this.widget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0, left: 20.0, right: 20.0),
      child: Row(
        textBaseline: TextBaseline.alphabetic,
        crossAxisAlignment: CrossAxisAlignment.baseline,
        children: <Widget>[
          Expanded(
            child: Text(
              headerText,
              style: const TextStyle(
                fontSize: 36.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          widget,
          // Offstage(
          //   offstage: (this.seeAllCallback == null),
          //   child: InkResponse(
          //     onTap: this.seeAllCallback,
          //     child: Text(
          //       '搜尋',
          //       style: TextStyle(
          //         color: Theme.of(context).accentColor,
          //         fontWeight: FontWeight.bold,
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
