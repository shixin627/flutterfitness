import 'package:flutter/material.dart';

class Project {
  final String name;
  final String port;
  final int hex;
  Project({required this.name, required this.port, required this.hex});
}

final List<Color> availableColors = [
    Colors.purpleAccent,
    Colors.yellow,
    Colors.lightBlue,
    Colors.orange,
    Colors.pink,
    Colors.redAccent,
    const Color(0xFFEB1555),
    const Color(0xFF1D1E33),
    const Color(0xFF111328),
  ];

// These colors (including there names) were taken from https://flatuicolors.com
class Projects {
  static List<Project> projects = [
    //1
    Project(
      name: "arm curl",
      port: "Hand",
      hex: 0xff1abc9c,
    ),
    //2
    Project(
      name: "chest press",
      port: "Hand",
      hex: 0xff16a085,
    ),
    //3
    Project(
      name: "lateral raise",
      port: "Hand",
      hex: 0xff19a031,
    ),
    //4
    Project(
      name: "converging shoulder press",
      port: "Hand",
      hex: 0xff24ff90,
    ),
    //5
    Project(
      name: "multi-position press",
      port: "Hand",
      hex: 0xff2ecc71,
    ),
    //6
    Project(
      name: "diverging seated row",
      port: "Hand",
      hex: 0xff27ae60,
    ),
    //7
    Project(
      name: "adjustable cable crossover",
      port: "Hand",
      hex: 0xff30aa00,
    ),
    //8
    Project(
      name: "seated leg curl",
      port: "Feet",
      hex: 0xfff28000,
    ),
    //9
    Project(
      name: "leg extension",
      port: "Feet",
      hex: 0xfff30000,
    ),
    //10
    Project(
      name: "hip adduction",
      port: "Feet",
      hex: 0xfff29a2f,
    ),
    //11
    Project(
      name: "prone leg curl",
      port: "Feet",
      hex: 0xfff1c40f,
    ),
    //12
    Project(
      name: "leg press",
      port: "Feet",
      hex: 0xfff39c12,
    ),
    //13
    Project(
      name: "calf press",
      port: "Feet",
      hex: 0xffe67e22,
    ),
    //14
    Project(
      name: "leg extension curl",
      port: "Feet",
      hex: 0xffd35400,
    ),
    //15
    Project(
      name: "rotary hip",
      port: "Feet",
      hex: 0xffe74c3c,
    ),
    //16
    Project(
      name: "rotary torso",
      port: "Body",
      hex: 0xffecf0a9,
    ),
    //17
    Project(
      name: "diverging lat pulldown",
      port: "Body",
      hex: 0xffecf0f1,
    ),
    //18
    Project(
      name: "lat pulldown-low row",
      port: "Body",
      hex: 0xffbdc3c7,
    ),
    //19
    Project(
      name: "Concrete",
      port: "Body",
      hex: 0xff95a5a6,
    ),
    //20
    Project(
      name: "Asbestos",
      port: "Body",
      hex: 0xff7f8c8d,
    ),
  ];
}
