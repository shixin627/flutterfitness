import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'models/navigating_arguments.dart';
import 'models/route_paths.dart' as routes;
import 'ui/item_files_screen.dart';
import 'ui/judge_auth.dart';
import 'ui/cloud.dart';
import 'ui/dashboard.dart';
import 'ui/day_screen.dart';
import 'ui/history_screen.dart';
import 'ui/item_screen.dart';
import 'ui/login_server.dart';
import 'ui/profile.dart';
import 'ui/register.dart';
import 'ui/session_screen.dart';
import 'ui/judge_bt_state.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  //產生應用程式的路由
  Widget child;
  switch (settings.name) {
    //由名字選擇產生的路由
    case routes.HomeRoute:
      child = JudgeBTStateScreen();
      break;
    case routes.DBManagement:
      child = DatabaseManagement();
      break;
    case routes.StatisticRoute:
      child = HistoryStatistic();
      break;
    case routes.ProfileRoute:
      child = ProfilePage();
      break;
    case routes.FreeWeightRoute:
      child = JudgeAuth();
      break;
    case '/files':
      final NavigatingArguments args =
          settings.arguments as NavigatingArguments;
      return PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) {
          return DirectoryScreen(animation,
              label: args.label, message: args.message, files: args.files);
        },
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return FadeTransition(
            opacity: animation,
            child: ScaleTransition(
              scale: animation.drive(
                Tween(begin: 1.3, end: 1.0).chain(
                  CurveTween(curve: Curves.easeOutCubic),
                ),
              ),
              child: child,
            ),
          );
        },
      );
    case routes.DashboardRoute:
      final DashboardPage args = settings.arguments
          as DashboardPage; //定義class(DashboardPage)的arguments
      child = DashboardPage(
          itemIndex: args.itemIndex); //頁面跳轉時，同步傳送當前所使用之class的arguments
      break;
    case routes.RegisterRoute:
      child = RegisterUser();
      break;
    case routes.LoginRoute:
      child = LoginPage();
      break;
    case routes.CloudRoute:
      child = CloudViewer();
      break;
    case routes.ItemRoute:
      final TheItemScreen args =
          settings.arguments as TheItemScreen; //定義class的arguments
      child = TheItemScreen(args.itemId, args.dataSetList);
      break;
    case routes.DayRoute:
      final TheDayScreen args =
          settings.arguments as TheDayScreen; //定義class的arguments
      child = TheDayScreen(args.theDate, args.dataSetList);
      break;
    case routes.SessionRoute:
      final SessionScreen args = settings.arguments as SessionScreen;
      child = SessionScreen(
        dataSet: args.dataSet,
      );
      break;
    default:
      child = Scaffold(
        body: Center(
          child: Text('No path for ${settings.name}'),
        ),
      );
      break;
  }
  return MaterialPageRoute(builder: (context) => child);
}
