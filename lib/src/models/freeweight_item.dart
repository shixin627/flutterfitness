// 自由重量訓練項目
class Item {
  final String title;
  final double lowerThreshold; // 下臨界值
  final double upperThreshold; // 上臨界值
  const Item(
      {required this.title,
      this.lowerThreshold = 0.0,
      this.upperThreshold = 0.0});
}
