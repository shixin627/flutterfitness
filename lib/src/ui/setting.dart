import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/prefs.dart';
// import '../data/global.dart';
// import '../services/auth.dart';

class SettingsPage extends StatelessWidget {
  final Animation<double> initialAnimation;
  const SettingsPage(this.initialAnimation, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.blue[100]!, Colors.blue[400]!],
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text('Theme Mode'),
                  Switch(
                    value: context.select<PrefsNotifier, bool>(
                        (notifier) => notifier.userDarkMode),
                    onChanged: (bool newValue) {
                      Provider.of<PrefsNotifier>(context, listen: false)
                          .userDarkMode = newValue;
                    },
                  ),
                ],
              ),
              // CircleAvatar(
              //   backgroundImage: NetworkImage(
              //     Global.user.photoURL,
              //   ),
              //   radius: 60,
              //   backgroundColor: Colors.transparent,
              // ),
              // SizedBox(height: 40),
              // Text(
              //   'NAME',
              //   style: TextStyle(
              //       fontSize: 15,
              //       fontWeight: FontWeight.bold,
              //       color: Colors.black54),
              // ),
              // Text(
              //   Global.user.displayName,
              //   style: TextStyle(
              //       fontSize: 25,
              //       color: Colors.deepPurple,
              //       fontWeight: FontWeight.bold),
              // ),
              // SizedBox(height: 20),
              // Text(
              //   'EMAIL',
              //   style: TextStyle(
              //       fontSize: 15,
              //       fontWeight: FontWeight.bold,
              //       color: Colors.black54),
              // ),
              // Text(
              //   Global.user.email,
              //   style: TextStyle(
              //       fontSize: 25,
              //       color: Colors.deepPurple,
              //       fontWeight: FontWeight.bold),
              // ),
              // SizedBox(height: 40),
              // ElevatedButton(
              //   onPressed: () {
              //     signOutGoogle().then((_) => Navigator.of(context).pop());
              //   },
              //   child: Padding(
              //     padding: const EdgeInsets.all(8.0),
              //     child: Text(
              //       'Sign Out',
              //       style: TextStyle(fontSize: 25, color: Colors.white),
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
