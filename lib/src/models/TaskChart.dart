import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import 'ExerciseData_model.dart';
import 'Task.dart';

///用於展示某一次運動的歷史資料
class TaskData {
  late List<Task> workTask;
  late List<Task> powerTask;
  late List<Task> trainTask;

  Map<String, List<Task>> get maptoTask {
    return {
      '總做功量': workTask,
      '平均功率': powerTask,
      '總訓練量': trainTask,
    };
  }

  late String index;

  static const Map<String, String> unit = {
    '總做功量': '焦耳',
    '平均功率': '瓦特',
    '總訓練量': '公斤'
  };

  static const List<String> title = ['總做功量', '平均功率', '總訓練量'];
}

class SessionBar extends StatelessWidget {
  final TaskData data;
  final bool animate;

  const SessionBar(this.data, {Key? key, this.animate = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return charts.BarChart(
      _createTaskData(data),
      animate: animate,
      behaviors: [
        charts.ChartTitle('組',
            behaviorPosition: charts.BehaviorPosition.bottom,
            titleOutsideJustification:
                charts.OutsideJustification.middleDrawArea),
        charts.ChartTitle(TaskData.unit[data.index] ?? '',
            behaviorPosition: charts.BehaviorPosition.start,
            titleOutsideJustification:
                charts.OutsideJustification.middleDrawArea),
        charts.ChartTitle(data.index,
            behaviorPosition: charts.BehaviorPosition.top,
            titleOutsideJustification:
                charts.OutsideJustification.middleDrawArea)
      ],
    );
  }

  List<charts.Series<Task, String>> _createTaskData(TaskData _data) {
    return [
      charts.Series<Task, String>(
        id: _data.index,
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (Task task, _) => task.number.toString(),
        measureFn: (Task task, _) => task.value,
        data: _data.maptoTask[_data.index] ?? [],
      )
    ];
  }

  static void fetchSession(DataSet session, TaskData task) {
    task.workTask = <Task>[];
    task.powerTask = <Task>[];
    task.trainTask = <Task>[];
    for (var dataset in session.dataset) {
      var index = session.dataset.indexOf(dataset);
      task.workTask.add(Task(index + 1, dataset.joule));
      task.powerTask.add(Task(index + 1, dataset.maxPower));
      task.trainTask.add(Task(index + 1, dataset.trainingVolume));
    }
  }
}
