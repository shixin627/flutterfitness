import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '/src/data/global.dart';
import 'google_sign_in_screen.dart';
// import 'package:flutter_unity_widget_example/src/ui/login_server.dart';
import 'pageview.dart';

class JudgeAuth extends StatelessWidget {
  const JudgeAuth({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<User>(
      builder: (context, user, child) {
        if (user == null) {
          return GoogleSignInScreen(); // LoginPage()
        } else {
          Global.user = user;
          print("Welcome " + user.displayName!);
          return MyPageView();
        }
      },
    );
  }
}
