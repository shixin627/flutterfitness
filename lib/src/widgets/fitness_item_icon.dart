import 'package:flutter/material.dart';

class FreeWeightItem extends StatelessWidget {
  final String item;
  final Color? color;
  final VoidCallback? onTap;
  final VoidCallback? onLongPress;
  final Widget child;
  const FreeWeightItem(this.item,
      {Key? key, this.color, this.onTap, this.onLongPress, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Material(
            type: MaterialType.circle,
            color: color ?? Colors.white,
            child: InkWell(
              onTap: onTap,
              onLongPress: onLongPress,
              child: AspectRatio(
                aspectRatio: 1.0,
                child: Container(
                  padding: const EdgeInsets.all(28.0),
                  child: child,
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 8.0),
            alignment: Alignment.center,
            child: Text(
              item,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
        ],
      ),
    );
  }
}

class MachineItem extends StatelessWidget {
  final String item;
  final VoidCallback? onTap;
  final Widget child;

  const MachineItem(this.item, {Key? key, this.onTap, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Material(
            type: MaterialType.circle,
            color: Colors.white,
            child: InkWell(
              onTap: onTap,
              child: AspectRatio(
                aspectRatio: 1.0,
                child: Container(
                  padding: const EdgeInsets.all(28.0),
                  child: child,
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 8.0),
            alignment: Alignment.center,
            child: Text(
              item,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
        ],
      ),
    );
  }
}
