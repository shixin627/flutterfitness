import 'package:intl/intl.dart';

class Date {  //日期與時間的型別轉換物件
  static DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
  static DateFormat ymdFormat = DateFormat("yyyy-MM-dd");
}
