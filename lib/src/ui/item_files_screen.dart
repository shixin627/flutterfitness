import 'dart:io';
import 'package:flutter/material.dart';
import '/src/data/constant.dart';
import 'package:provider/provider.dart';
import 'package:path/path.dart' as Path;
import 'package:flutter_sparkline/flutter_sparkline.dart';
import '../services/firebase.dart';
import '../data/global.dart';
import '../helpers/data_manager.dart';
import '../providers/artificial_intelligent.dart';

class DirectoryScreen extends StatefulWidget {
  final Animation<double> initialAnimation;
  final int label;
  final String message;
  final List<File> files;
  const DirectoryScreen(this.initialAnimation,
      {Key? key, required this.label, required this.message, required this.files})
      : super(key: key);

  @override
  _DirectoryScreenState createState() =>
      _DirectoryScreenState(label, message, files);
}

class _DirectoryScreenState extends State<DirectoryScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final int label;
  final String message;
  List<File> files;
  _DirectoryScreenState(this.label, this.message, this.files);
  @override
  Widget build(BuildContext context) {
    final title = DUMBBELL_ITEMs[label].title;
    final lowerThreshold = DUMBBELL_ITEMs[label].lowerThreshold;
    final upperThreshold = DUMBBELL_ITEMs[label].upperThreshold;
    return Scaffold(
      key: _scaffoldKey,
      body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                expandedHeight: 200.0,
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text(title.replaceAll('_', ' ').toUpperCase(),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  background: Image.asset(
                    'assets/images/Exercise_Workout.jfif',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ];
          },
          body: ListView.builder(
              itemCount: files.length,
              itemBuilder: (context, i) {
                final file = files[i];
                final basePath = Path.basename(file.path);
                final pathOnCloud = 'free_weight/$title/$userId/$basePath'; //資料夾路徑
                return Dismissible(
                    key: Key(basePath),
                    background: Container(
                      color: Colors.green,
                      child: Icon(Icons.check),
                    ),
                    secondaryBackground: Container(
                      color: Colors.red,
                      child: Icon(Icons.delete_forever),
                    ),
                    onDismissed: (direction) {
                      if (direction == DismissDirection.endToStart) {
                        context
                            .read<FirebaseService>()
                            .deleteFile(pathOnCloud)
                            .then((_) {
                          DataManager.deleteFile(file.path);
                        });
                        setState(() {
                          files.removeAt(i);
                        });
                        _showToast(context, "$file dismissed");
                      } else if (direction == DismissDirection.startToEnd) {}
                    },
                    child: ListTile(
                      title: Text(basePath),
                      trailing: IconButton(
                          icon: Icon(Icons.upload_file),
                          onPressed: () {
                            context
                                .read<FirebaseService>()
                                .uploadFile(pathOnCloud, file)
                                .then((successful) {
                              if (successful) {
                                _showToast(context, "Uploaded Successfully");
                              } else {
                                _showToast(context, "Failed");
                              }
                            });
                          }),
                      onTap: () {
                        file.readAsLines().then((lines) {
                          lines.removeRange(0, 2);
                          final dataset = lines.map((line) {
                            final data = line.split(",");
                            final z =
                                double.parse(data[16]) + double.parse(data[19]);
                            return z;
                          }).toList();
                          // print(dataset.reduce(max));
                          // print(dataset.reduce(min));

                          showLine(
                              context, dataset, lowerThreshold, upperThreshold);
                        });
                      },
                    ));
              })),
    );
  }

  Future<void> showLine(BuildContext context, List<double> data,
      double lowerThreshold, double upperThreshold) async {
    return context
        .read<ArtificialIntelligent>()
        .detectPeaks(label, data)
        .then((model) {
      if (model != null) {
        return showDialog<void>(
          context: context,
          barrierDismissible: true,
          builder: (BuildContext context) {
            return SimpleDialog(
              children: [
                Container(
                  width: 700,
                  child: data == null
                      ? null
                      : Sparkline(
                          data: data,
                          lineWidth: 3.0,
                        ),
                ),
                Center(
                    child: Text("峰谷的數量: " + model.valleys.length.toString())),
                Center(child: Text("反覆次數: " + model.repetition.toString())),
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
            );
          },
        );
      }
    });
  }

  void _showToast(BuildContext context, String content) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(content),
        action: SnackBarAction(
            label: 'UNDO',
            onPressed: ScaffoldMessenger.of(context).hideCurrentSnackBar),
      ),
    );
  }
}
