import 'package:firebase_auth/firebase_auth.dart';
import '/src/models/body_size.dart';
import '/src/models/user_model.dart';
import '/src/services/localstorage_service.dart';
import '/src/service_locator.dart';
import 'constant.dart';

Body get body => locator<LocalStorageService>().body;

double get upperArm => body.upperArm! / 100; //unit: cm->m
double get foreArm => body.foreArm! / 100; //unit: cm->m

/// 使用前臂與上臂的垂直分量計算3D垂直座標的位置(單位: 公尺)
double getRealPositionFrom(double verticalComponentOfUnitUpperArm,
    double verticalComponentOfUnitLowerArm) {
  return upperArm * verticalComponentOfUnitUpperArm +
      foreArm * verticalComponentOfUnitLowerArm;
}

UserModel get localUser =>
    locator<LocalStorageService>().user ??
    UserModel(
        id: 0, email: 'null', name: 'null'); //不存在id為0的使用者

String get bindedAddress => locator<LocalStorageService>().bindedAddress;
// DD:A8:6B:A1:CA:60, E4:DC:E2:FB:7F:1B, E8:EF:47:18:7C:06, D8:7D:75:DC:93:7E

/*
List<String> devicesAddress = [
  // 自由重量訓練
  bindedAddress, //Biceps Curl
  bindedAddress, //Lateral Raise
  bindedAddress, //Shoulder Press
  // 手部
  '3C:71:BF:88:D6:96', //胸部推舉
  '24:6F:28:17:13:96', //臂部彎舉
  '', //側平舉
  '', //分動式肩部推舉
  '', //Multi-position Press
  '', //分動式坐姿划船
  //腳部
  '', //坐姿腿部彎舉
  '', //腿部伸屈
  '', //髖關節內收
  '', //俯臥腿彎舉
  '', //腿部推舉
  '', //小腿推舉
  '', //腿部伸屈彎舉
  '', //髖關節旋轉
  //身體
  '', //軀幹旋轉
  '', //分動式背部下拉
  '', //Lat Pulldown-low Row
  '', //Adjustable Cable Crossover
];
*/

String get userId =>
    (Global.user != null) ? Global.user!.uid : localUser.id.toString();

class Global {
  static User? user;
  static int bellWeightIndex = 3;
  static int get bellWeight => BellWeights[bellWeightIndex];
  static bool isDoingFreeWeight = false;
  static int currentSelectedItemID = 0;
}
