import 'dart:convert';
import 'set_model.dart';

class DataSet {
  int id;
  String userID;
  int itemID;
  String datetime;
  List<TheSet> dataset;
  List<int> interRest;

  DataSet(this.id, this.userID, this.itemID, this.datetime,
      {this.dataset = const [], this.interRest = const []});

  static List<DataSet> list = [];

  Analysis get analysis {
    Analysis _analysis = Analysis(
        totalWork: 0,
        totalDuration: 0,
        totalTraining: 0,
        averagePower: 0,
        maxEnduration: 0,
        maxPower: 0);

    for (var theSet in dataset) {
      _analysis.totalWork += theSet.joule;
      _analysis.totalDuration += theSet.duration;
      _analysis.totalTraining += theSet.trainingVolume;
      if (theSet.joule > _analysis.maxEnduration) {
        _analysis.maxEnduration = theSet.joule;
      }
      if (theSet.maxPower > _analysis.maxPower) {
        _analysis.maxPower = theSet.maxPower;
      }
    }
    _analysis.averagePower =
        (_analysis.totalWork / _analysis.totalDuration).floor();

    return _analysis;
  }

  Map<String, dynamic> toJson() {
    List<Map> maps = [];
    maps = dataset.map((i) => i.toJson()).toList();
    return {
      'user_id': userID,
      'item_id': itemID,
      'datetime': datetime,
      'dataset': maps,
      'interRest': interRest
    };
  }

  factory DataSet.fromJson(Map<String, dynamic> json) {
    if (json['dataset'] != null) {
      var _string = jsonDecode(json['dataset']);
      var dataObjsJson = _string as List;
      List<TheSet> _dataset = dataObjsJson
          .map((datasetJson) => TheSet.fromJson(datasetJson))
          .toList();
      var _interRestJson = jsonDecode(json['interRest']);
      List<int> _interRest =
          _interRestJson != null ? List.from(_interRestJson) : [];
      return DataSet(json['id'] as int, json['user_id'] as String,
          json['item_id'] as int, json['datetime'] as String,
          dataset: _dataset, interRest: _interRest);
    } else {
      return DataSet(json['id'] as int, json['user_id'] as String,
          json['item_id'] as int, json['datetime'] as String);
    }
  }

  @override
  String toString() {
    return '{ $id, $userID, $itemID, $datetime, $dataset, $interRest }';
  }
}

class Analysis {
  int totalWork;
  int totalDuration;
  int totalTraining;
  int averagePower;
  int maxEnduration;
  int maxPower;
  Analysis(
      {required this.totalWork,
      required this.totalDuration,
      required this.totalTraining,
      required this.averagePower,
      required this.maxEnduration,
      required this.maxPower});
}
