import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/user_model.dart';
import '../services/localstorage_service.dart';
import '../services/navigation_service.dart';
import '../widgets/theme.dart';
import '../service_locator.dart';

class RegisterUser extends StatefulWidget {
  RegisterUserState createState() => RegisterUserState();
}

class RegisterUserState extends State<RegisterUser> {
  final formKey = GlobalKey<FormState>();
  final mainKey = GlobalKey<ScaffoldState>();
  LocalStorageService storageService = locator<LocalStorageService>();
  final NavigationService _navigationService = locator<NavigationService>();
  bool visible = false;
  String? name, email, password;

  Future userRegistration() async {
    var form = formKey.currentState;

    setState(() {
      visible = true;
    });

    if (form!.validate()) {
      form.save();

      // SERVER API URL
      var url = 'http://120.105.133.147/project/filter/register_user.php';
      // Store all data with Param Name.
      var data = {'name': name, 'email': email, 'password': password};
      // Starting Web API Call.
      var response = await http.post(Uri.parse(url), body: json.encode(data));
      // If Web call Success than Hide the CircularProgressIndicator.
      if (response.statusCode == 200) {
        setState(() {
          visible = false;
        });
      }

      storageService.user =
          UserModel(email: email!, name: name, id: 0); //儲存登錄之email,password以及姓名
      _navigationService.goBack();
      // Navigator.of(context).pop(true);

      var snackbar = SnackBar(
        content: Text('歡迎 $name !'),
        duration: const Duration(milliseconds: 5000),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackbar);
    }
  }

  Widget build(BuildContext context) {
    final registerButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: const Color(0xff0211C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: userRegistration,
        child: Text("Register",
            textAlign: TextAlign.center,
            style: textStyle.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
      key: mainKey,
      body: Center(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Text('User Registration Form', style: textStyle),
                  ),
                  const Divider(),
                  const SizedBox(height: 45.0),
                  TextFormField(
                    obscureText: false,
                    style: textStyle,
                    autocorrect: false,
                    decoration: InputDecoration(
                      contentPadding:
                          const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Name",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    ),
                    onSaved: (str) => name = str,
                  ),
                  const SizedBox(height: 25.0),
                  TextFormField(
                    style: textStyle,
                    autocorrect: false,
                    decoration: InputDecoration(
                      contentPadding:
                          const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Email",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    ),
                    validator: (str) => (str!.contains('@')) == false
                        ? "Not a Valid Email!"
                        : null,
                    onSaved: (str) => email = str,
                  ),
                  const SizedBox(height: 25.0),
                  TextFormField(
                    obscureText: true,
                    style: textStyle,
                    autocorrect: false,
                    decoration: InputDecoration(
                      contentPadding:
                          const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Password",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    ),
                    validator: (str) => str!.length <= 7 ? "長度至少7個字元!" : null,
                    onSaved: (str) => password = str,
                  ),
                  const SizedBox(
                    height: 35.0,
                  ),
                  registerButon,
                  const SizedBox(
                    height: 15.0,
                  ),
                  Visibility(
                    visible: visible,
                    child: Container(
                      margin: const EdgeInsets.only(bottom: 30),
                      child: const CircularProgressIndicator(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
