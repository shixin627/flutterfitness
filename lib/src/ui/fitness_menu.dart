import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import '/src/data/constant.dart';
import '/src/providers/bluetooth_notifier.dart';
import '/src/providers/menu_provider.dart';
import '/src/ui/dashboard2.dart';
import '/src/widgets/fitness_item_icon.dart';
import 'package:provider/provider.dart';
import '../models/fitness_item.dart';
import 'dashboard.dart';
import '../services/navigation_service.dart';
import '../widgets/material.dart';
import '../service_locator.dart';
import '../models/route_paths.dart' as routes;
import '../widgets/ListHeader.dart';

class FitnessMenuScreen extends StatefulWidget {
  const FitnessMenuScreen({Key? key}) : super(key: key);

  @override
  _FitnessMenuScreen createState() => _FitnessMenuScreen();
}

class _FitnessMenuScreen extends State<FitnessMenuScreen>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    locator<BTNotifier>().stopScan();
    super.dispose();
  }

  void _navigate(int index) {
    if (Provider.of<FitnessItemModel>(context, listen: false).target != null) {
      locator<NavigationService>().navigateTo(
        routes.DashboardRoute,
        arguments: DashboardPage(
          itemIndex: index,
        ),
      );
    }
  }

  Widget _buildItems(BuildContext context, int index) {
    final itemTitle = ITEM_NAME[index];
    bool isAvailable =
        Provider.of<FitnessItemModel>(context, listen: false).isAvailable;
    Widget widget;
    if (index < 3) {
      // 如果是自由重量訓練的項目
      widget = Selector<BTNotifier, bool>(
        selector: (_, notifier) => notifier.isAvailable,
        builder: (context, isAvailable, child) {
          if (isAvailable) {
            return Selector<BTNotifier, BluetoothDeviceState?>(
              selector: (_, notifier) => notifier.targetStatus,
              builder: (context, targetStatus, child) {
                switch (targetStatus) {
                  case BluetoothDeviceState.disconnected:
                    debugPrint('BluetoothDeviceState.disconnected');
                    return FreeWeightItem(
                      itemTitle,
                      child: Image.asset(
                        ImageAssets[itemTitle] ??
                            'assets/images/free-weight/biceps_curl.png',
                        fit: BoxFit.contain,
                      ),
                      color: Colors.blue[100]!,
                      onLongPress: () {
                        locator<BTNotifier>().connectToDevice();
                      },
                    );
                  case BluetoothDeviceState.connecting:
                    debugPrint('BluetoothDeviceState.connecting');
                    return FreeWeightItem(
                      itemTitle,
                      child: Image.asset(
                        ImageAssets[itemTitle] ?? defaultImageAsset,
                        fit: BoxFit.contain,
                      ),
                      color: Colors.blue[300]!,
                    );
                  case BluetoothDeviceState.connected:
                    debugPrint('BluetoothDeviceState.connected');
                    return FreeWeightItem(
                      itemTitle,
                      child: Image.asset(
                        ImageAssets[itemTitle] ?? defaultImageAsset,
                        fit: BoxFit.contain,
                      ),
                      color: Colors.yellow[100]!,
                      onTap: () =>
                          Provider.of<MenuProvider>(context, listen: false)
                              .doIt(index),
                      onLongPress: () =>
                          locator<BTNotifier>().disconnectFromDevice(),
                    );
                  case BluetoothDeviceState.disconnecting:
                    debugPrint('BluetoothDeviceState.disconnecting');
                    return FreeWeightItem(
                      itemTitle,
                      child: Image.asset(
                        ImageAssets[itemTitle] ?? defaultImageAsset,
                        fit: BoxFit.contain,
                      ),
                      color: Colors.pinkAccent,
                    );
                  default:
                    return Container();
                }
              },
            );
          } else {
            return FreeWeightItem(
              itemTitle,
              child: Image.asset(
                ImageAssets[itemTitle] ?? defaultImageAsset,
                fit: BoxFit.contain,
              ),
              color: Colors.grey,
            );
          }
        },
      );
    } else {
      widget = MachineItem(
        itemTitle,
        child: Image.asset(
          ImageAssets[itemTitle] ?? defaultImageAsset,
          fit: BoxFit.contain,
        ),
        onTap: isAvailable ? () => _navigate(index) : () {},
      );
    }
    return widget;
  }

  Widget bleScanBtn() => Selector<BTNotifier, bool>(
        selector: (_, notifier) => notifier.isBLEon,
        builder: (context, isBLEon, child) {
          if (isBLEon) {
            return Selector<BTNotifier, bool>(
                builder: (_, isScanning, __) {
                  if (isScanning) {
                    return IconButton(
                        icon: const Icon(Icons.stop_circle_rounded,
                            color: Colors.red),
                        onPressed: locator<BTNotifier>().stopScan);
                  } else {
                    return IconButton(
                        icon: const Icon(Icons.bluetooth_audio,
                            color: Colors.cyan),
                        onPressed: locator<BTNotifier>().startScan);
                  }
                },
                selector: (_, notifier) => notifier.isScanning);
          } else {
            return const Opacity(
              opacity: 1,
              child: Icon(
                Icons.bluetooth_disabled,
                color: Colors.red,
              ),
            );
          }
        },
      );

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Selector<MenuProvider, bool>(
      selector: (_, provider) => provider.isDoingFreeWeight,
      builder: (context, isDoingFreeWeight, child) {
        final itemIndex =
            Provider.of<MenuProvider>(context, listen: false).selectedIndex;
        if (isDoingFreeWeight) {
          return DashBoardFreeWeight(itemIndex);
        } else {
          return ScreenBackground(
            child: RefreshIndicator(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: MediaQuery.of(context).padding.top),
                    ListHeader(
                      '運動菜單',
                      seeAllCallback: null,
                      widget: bleScanBtn(),
                    ),
                    Consumer<FitnessItemModel>(
                        builder: (context, model, child) {
                      return GridFeaturedItems(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 16.0),
                        itemCount: itemLen,
                        itemBuilder: _buildItems,
                      );
                    }),
                    const SizedBox(height: 36.0),
                  ],
                ),
              ),
              onRefresh: () => locator<BTNotifier>().refreshTargetBLE(),
            ),
          );
        }
      },
    );
  }
}

class GridFeaturedItems extends StatelessWidget {
  final int crossAxisCount;
  final EdgeInsetsGeometry? padding;
  final IndexedWidgetBuilder itemBuilder;
  final int itemCount;

  const GridFeaturedItems({
    Key? key,
    this.crossAxisCount = 2,
    this.padding,
    required this.itemBuilder,
    required this.itemCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var rows = <TableRow>[];
    var cells = <Widget>[];
    for (int i = 0; i < itemCount; i++) {
      if (i > 0 && (i % crossAxisCount) == 0) {
        rows.add(TableRow(children: List.from(cells)));
        cells.clear();
      }
      cells.add(itemBuilder(context, i));
    }
    if (cells.isNotEmpty) {
      if (crossAxisCount - cells.length > 0) {
        cells.addAll(
            List.generate(crossAxisCount - cells.length, (i) => Container()));
      }
      rows.add(TableRow(children: List.from(cells)));
    }
    Widget child = Table(children: rows);
    if (padding != null) {
      child = Padding(padding: padding!, child: child);
    }
    return child;
  }
}
