import 'package:flutter/material.dart';
import 'package:path/path.dart';
import '../models/ExerciseData_model.dart';
import 'package:sqflite/sqflite.dart';

class LocalDatabaseService {
  static Database? db;
  static const String table_name = 'sessions';
  static const String user_id_tag = 'user_id';    //使用者識別碼
  static const String item_id_tag = 'item_id';    //項目識別碼
  static const String datetime_tag = 'datetime';  //儲存時間
  static const String dataset_tag = 'dataset';    //運動處方變項資料集
  static const String interRest_tag = 'interRest';//組間休息時間
  //打開 SQLite Database
  static Future open() async {
    db = await openDatabase(join(await getDatabasesPath(), 'SmartFitness.db'),
        version: 1, onCreate: (Database db, int version) async {
      db.execute('''
          CREATE TABLE $table_name (
            id integer primary key,
            $user_id_tag text,
            $item_id_tag unsign integer,
            $datetime_tag text,
            $dataset_tag text,
            $interRest_tag text
          );
        ''');
    });
  }

  static Future<List<Map<String, dynamic>>> getDataList() async {
    if (db == null) {
      await open();
    }
    return await db!.query(table_name);
  }

  static Future insertData(Map<String, dynamic> data) async {
    await db!.insert(table_name, data);
  }

  static Future updateData(Map<String, dynamic> data) async {
    await db!.update(table_name, data, where: 'id = ?', whereArgs: [data['id']]);
  }

  static Future deleteData(int id) async {
    await db!.delete(table_name, where: 'id = ?', whereArgs: [id]);
  }

  static Future<List<DataSet>> fetchSessions() async {
    final items = await getDataList();
    debugPrint(items.toString());
    List<DataSet> datasetList = items.map<DataSet>((json) {
      var dataset = DataSet.fromJson(json);
      return dataset;
    }).toList();

    return datasetList;
  }
}
