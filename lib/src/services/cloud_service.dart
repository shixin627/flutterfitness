import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../models/ExerciseData_model.dart';
import '../models/inbody_model.dart';

class CloudService {
  
  static Future submitData(BuildContext _context, DataSet _dataset) async { // 呼叫網站伺服器，傳送運動資料到資料庫
    var url = 'http://120.105.133.146/project/filter/SubmitData.php'; // API URL
    String jsonDataset = jsonEncode(_dataset);
    var response = await http.post(Uri.parse(url), body: jsonDataset);
    var message = jsonDecode(response.body);
    showDialog(
      context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(message),
          actions: <Widget>[
            TextButton(
              child: Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
    print("Data-set Inserted to MySQL database.");
  }

  static Future updateBody(BuildContext _context, InBody _body) async {
    var url = 'http://120.105.133.146/project/filter/SubmitBody.php'; // API URL
    String _jsonString = jsonEncode(_body);
    var response = await http.post(Uri.parse(url), body: _jsonString);
    var message = jsonDecode(response.body);
    showDialog(
      context: _context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(message),
          actions: <Widget>[
            TextButton(
              child: Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
    print("Body Composition Inserted to MySQL database.");
  }

  static Future<List<InBody>> fetchInBody(int _id) async {
    const apiURL = 'http://120.105.133.146/project/filter/GetBody.php';
    var _key = {'body_id': _id};  //製作有使用者編號的鑰匙
    var response = await http.post(Uri.parse(apiURL), body: json.encode(_key));
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      List<InBody> _inBodyList = data.map<InBody>((json) {
        var _inBody = InBody.fromJson(json);
        return _inBody;
      }).toList();
      return _inBodyList;
    } else {
      throw Exception('Failed to load data from Server.');
    }
  }

  static Future<List<DataSet>> fetchSessions(int _id) async {
    // var response = await http.get(apiURL);
    const apiURL = 'http://120.105.133.146/project/filter/GetUser.php';
    var _key = {'user_id': _id};
    var response = await http.post(Uri.parse(apiURL), body: json.encode(_key));
    if (response.statusCode == 200) {
      var items = jsonDecode(response.body);
      List<DataSet> datasetList = items.map<DataSet>((json) {
        var dataset = DataSet.fromJson(json);
        return dataset;
      }).toList();
      return datasetList;
    } else {
      throw Exception('Failed to load data from Server.');
    }
  }
}
