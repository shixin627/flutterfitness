import 'package:flutter/material.dart';
import '/src/data/constant.dart';

class ItemCard extends StatelessWidget {
  final String item;
  final Color color;

  const ItemCard({required this.item, required this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      padding: EdgeInsets.all(18.0),
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(12.0),
        boxShadow: const <BoxShadow>[
          BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0,
            offset: Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            height: 120,
            padding: const EdgeInsets.all(8.0),
            child: Image.asset(
              ImageAssets[item] ?? defaultImageAsset,
              fit: BoxFit.contain,
            ),
          ),
          Text(
            item,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
