import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'models/navigating_arguments.dart';
import 'providers/prefs.dart';
import 'service_locator.dart';
import 'services/navigation_service.dart';
import 'ui/dashboard.dart';
import 'ui/day_screen.dart';
import 'ui/history_screen.dart';
import 'ui/item_files_screen.dart';
import 'ui/item_screen.dart';
import 'ui/judge_auth.dart';
import 'ui/profile.dart';
import 'ui/register.dart';
import 'ui/session_screen.dart';
import 'ui/setting.dart';
import 'ui/simple_screen.dart';

class MotionTrackingApp extends StatelessWidget {
  const MotionTrackingApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Free-Weight Tracking App',
      theme: Provider.of<PrefsNotifier>(context).userDarkMode
          ? ThemeData.dark().copyWith(
              textTheme: GoogleFonts.rubikTextTheme(
              Theme.of(context).textTheme.apply(
                    bodyColor: Colors.white,
                    displayColor: Colors.white,
                  ),
            ))
          : ThemeData.light().copyWith(
              primaryColor: const Color(0xFF00C8F4),
              textTheme: GoogleFonts.rubikTextTheme(
                Theme.of(context).textTheme,
              )),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      navigatorKey: locator<NavigationService>().navigatorKey,
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case '/':
            return MaterialPageRoute(
              builder: (context) => const JudgeAuth(), // MyPageView()
            );
          case '/simple':
            return  MaterialPageRoute(
              builder: (context) => const SimpleScreen(),
            );
          case '/settings':
            return PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) {
                return SettingsPage(animation);
              },
              transitionsBuilder:
                  (context, animation, secondaryAnimation, child) {
                return FadeTransition(
                  opacity: animation,
                  child: ScaleTransition(
                    scale: animation.drive(
                      Tween(begin: 1.3, end: 1.0).chain(
                        CurveTween(curve: Curves.easeOutCubic),
                      ),
                    ),
                    child: child,
                  ),
                );
              },
            );
          case '/files':
            final args = settings.arguments as NavigatingArguments;
            return PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) {
                return DirectoryScreen(animation,
                    label: args.label,
                    message: args.message,
                    files: args.files);
              },
              transitionsBuilder:
                  (context, animation, secondaryAnimation, child) {
                return FadeTransition(
                  opacity: animation,
                  child: ScaleTransition(
                    scale: animation.drive(
                      Tween(begin: 1.3, end: 1.0).chain(
                        CurveTween(curve: Curves.easeOutCubic),
                      ),
                    ),
                    child: child,
                  ),
                );
              },
            );
          case '/item':
            final TheItemScreen args = settings.arguments as TheItemScreen;
            return MaterialPageRoute(
              builder: (context) =>
                  TheItemScreen(args.itemId, args.dataSetList),
            );
          case '/day':
            final TheDayScreen args = settings.arguments as TheDayScreen;
            return MaterialPageRoute(
              builder: (context) =>
                  TheDayScreen(args.theDate, args.dataSetList),
            );
          case '/session':
            final SessionScreen args = settings.arguments as SessionScreen;
            return MaterialPageRoute(
              builder: (context) => SessionScreen(
                dataSet: args.dataSet,
              ),
            );
          case '/profile': //ProfilePage
            // final ProfilePage args = settings.arguments;
            return MaterialPageRoute(
              builder: (context) => const ProfilePage(),
            );
          case '/register':
            return MaterialPageRoute(builder: (context) => RegisterUser());
          case '/history':
            return MaterialPageRoute(builder: (context) => HistoryStatistic());
          case '/dashboard':
            return MaterialPageRoute(builder: (context) {
              final DashboardPage args = settings.arguments as DashboardPage;
              return DashboardPage(itemIndex: args.itemIndex);
            });
          default:
            throw UnimplementedError('no route for $settings');
        }
      },
    );
  }
}
