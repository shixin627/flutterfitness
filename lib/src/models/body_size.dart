class Body {
  double? foreArm;
  double? upperArm;

  Body({this.foreArm, this.upperArm});

  Map<String, dynamic> toJson() {
    return {
      'foreArm': foreArm,
      'upperArm': upperArm,
    };
  }

  factory Body.fromJson(Map<String, dynamic> json) {
    return Body(
      foreArm: json['foreArm'] as double,
      upperArm: json['upperArm'] as double,
    );
  }
}
