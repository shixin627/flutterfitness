using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class CubeInteraction: MonoBehaviour, IEventSystemHandler
{
    [SerializeField]
    Vector3 RotateAmount;

    void Start()
    {
        RotateAmount = new Vector3(1, 1, 1);
    }

    void Update()
    {
        gameObject.transform.Rotate(RotateAmount * Time.deltaTime * 10);

        for (int i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase.Equals(TouchPhase.Began))
            {
                var hit = new RaycastHit();

                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);

                if (Physics.Raycast(ray, out hit))
                {
                    UnityMessageManager.Instance.SendMessageToFlutter("方块被触碰");
                }
            }
        }
    }

    // （Flutter 调用）设置旋转速度
    public void SetRotationSpeed(String message)
    {
        float value = float.Parse(message);
        RotateAmount = new Vector3(value, value, value);
    }
}